<?php

use Illuminate\Http\Request;
use App\Models\PageDataset;
use App\Models\Pages;
use App\Models\Templates;
use App\Models\StaticStubs;
use App\Http\Controllers\Libs\CURL;
use Illuminate\Support\Facades\Log;
use Dejurin\GoogleTranslateForFree;
/*Route::get('/backup', function () {
    $file = file_get_contents(public_path() . "/" . 'backup.json');
    $file = json_decode($file, true);

    foreach ($file as $model => $data) {
        $initModel = new $model;
        $initModel->insert($data);
    }
});*/

/*Route::get('/pereezd', function () {
    $models = [
        "App\Models\UserRole",
        "App\Models\Users",
        "App\Models\UsersAuthLog",
        "App\Models\Templates",
        "App\Models\Pages",
        "App\Models\PageDataset",
        "App\Models\Questions",
        "App\Models\StaticStubs",
        "App\Models\Teachers",
        "App\Models\TeacherInformation",
        "App\Models\QuestionAnswer",
        "App\Models\News",
        "App\Models\Lecturers",
        "App\Models\LectureHall",
        "App\Models\FAQ",
        "App\Models\HigherEducation",
        "App\Models\HigherEducationProfile",
        "App\Models\Faculty",
        "App\Models\Cathedra",
        "App\Models\AdditionalEducation",
        "App\Models\AdditionalEducationSections",
    ];

    $response = [];

    foreach ($models as $model) {
        $initModel = new $model;
        /*$columns = $initModel->getFillable();
        array_unshift($columns, 'id');*/
     /*   $response += [$model => $initModel->orderBy('id', 'ASC')->get()];
    }
    return $response;
//    $columns = $initModel->getFillable();
});*/

Route::get("/insert-smi", function () {

    $file = file_get_contents(public_path() . "/" . 'smi.json');
    $modelData = json_decode($file, true);

    for ($i = 0; $i < count($modelData); $i++) {

        if (preg_match("/[[~[\d]*]]/", $modelData[$i]['link'])) {
            unset($modelData[$i]);
        } else {
            if ($modelData[$i]['link'][0] == "/") {
                $modelData[$i]['link'] = "http://inpsycho.ru" . $modelData[$i]['link'];
            }
            if (mb_stristr($modelData[$i]['link'], "files_new") !== FALSE) {
                $modelData[$i]['link'] = "http://inpsycho.ru/" . $modelData[$i]['link'];
            }
        }

    }

    $modelData = array_values($modelData);

    DB::table('smi_about_us')->insert($modelData);

});

Route::get("/smi", function () {
    set_time_limit(0);
    $str = '<table border="0" cellpadding="10" cellspacing="5" style="width:100%;">
	<tbody><!--tr class="hiden-row" style="display:none;"-->
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://www.psychologies.ru/events/kak-vliyayut-na-nas-tsveta/" target="_blank">Статья Поповой «Как цвета влияют на нас?» в журнале «Psychologies»</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>04.08.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="files_new/2020/08/статья А.Воиновой.pdf" target="_blank">«Индивидуальный ключ: концепция Школы практической психологии Т.Ю.Базарова» статья Анны Войновой в журнале «Управление персоналом» №27(583)/2020</a></strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://psy.su/feed/8326/" target="_blank">Норма и патология личности: доклад Наталии Белопольской</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>19.06.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.bestfacts.ru/deti-s-problemami-razvitiya-v-sovremennom-mire/" target="_blank">Статья Усановой "Дети с проблемами развития в современном мире" в " Best Facts"</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>16.06.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://moscow.psiheya-market.ru/stati/zdorove/obshchie-principy-zaboty-o-psihicheskom-zdorove-v-usloviyah-karantina-i-samoizolyacii" target="_blank">Статья Плужникова "Общие принципы заботы о психическом здоровье в условиях карантина и самоизоляции" в Психея-Маркет</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>16.06.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://moscow.psiheya-market.ru/stati/psihologiya/kak-spravitsya-s-oshchushcheniem-odinochestva-i-nenuzhnosti" target="_blank">Статья Тихоновой "Как справиться с ощущением одиночества и ненужности" в Психея-Маркет</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>04.06.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://www.bestfacts.ru/odinochestvo-v-period-samoizolyaczii/" target="_blank">«Одиночество в период самоизоляции» Дрокина О.В.</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>26.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://radiosputnik.ria.ru/20200611/1572769950.html" target="_blank">Наталия Белопольская и Светлана Штукарева. С удаленки в офис. Как мы будем возвращаться к привычной жизни</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>11.06.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://www.bestfacts.ru/odinochestvo-v-period-samoizolyaczii/" target="_blank">«Одиночество в период самоизоляции» Дрокина О.В.</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>26.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.psychologies.ru/events/koronavirus-kak-perejit-neizbejnyie-peremenyi/" target="_blank">Авторская статья Минасян И.Р. Журнал «Psychologies»</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>22.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://psy.su/feed/8265/" target="_blank">Национальный конкурс «Золотая Психея» и премия «Рыцарь культуры достоинства»</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>20.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://psy.su/feed/8241/" target="_blank">Советы Галины Галимовой в «Психологической газете»</a>&nbsp;</strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>20.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://psy.su/feed/8261/" target="_blank">«О том, как найти опору, когда все меняется» Авторская статья Курдюковой Н.А. в «Психологической газете»</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>19.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://thediplomaticsociety.co.za/home/16-home/3005-140-characters-diplomacy" target="_blank">Винченцо Лигорио для журнала The Diplomatic</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>07.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://player.vimeo.com/video/418760661?fbclid=IwAR2e--M1vPWdZ-5LI1NBUFAUk0TvkyXRVPQHqnUyJDTPmdsYJTcCRYwae-E" target="_blank">Один невероятный день с Владимиром Майковым</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://youtu.be/M_jjMcBMbxE" target="_blank">Л.И. Сурат интервью итоговому выпуску новостей на Первом канале</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>15.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://xn--80aesfpebagmfblc0a.xn--p1ai/news/20200422-1530.html" target="_blank">Л.И. Сурат для Стопкоронавирус.рф</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>22.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://tass.ru/obschestvo/8316921" target="_blank">Илья Плужников для ТАСС</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>23.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://xn--80aesfpebagmfblc0a.xn--p1ai/news/20200515-1230.html" target="_blank">Наталья Курдюкова «Как реагировать на перемены и правильно оценивать свои ресурсы: мнение психолога» для сайта Стопкоронавирус.рф</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>15.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.psychologies.ru/events/karantin-vozmojnost-pochuvstvovat-sebya-nujnyim/" target="_blank">Журнал «Псайколоджи». Авторская статья Визель Т.Г.</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>14.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://www.pravda.ru/health/1497525-suitsid/" target="_blank">Правда.ру. Комментарий В.П. Файнзильберга</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>12.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://xn--80aesfpebagmfblc0a.xn--p1ai/news/20200511-1230.html" target="_blank">Дмитрий Занин «Психолог рассказал, как управлять стрессом в период изоляции» для сайта Стопкоронавирус.рф</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>11.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://msk.postupi.online/journal/novosti-vuzov/studenty-podelilis-svoimi-vpechatleniyami-o-distantsionnom-obuchenii/" target="_blank">Поступи.Онлайн. Студенты поделились своими впечатлениями о дистанционном обучении</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>08.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://xn--80aesfpebagmfblc0a.xn--p1ai/news/20200508-1230.html" target="_blank">Татьяна Попова «Битва подушками и арт-терапия: психолог рассказала, как определить эмоции ребенка и поддержать его» для сайта Стопкоронавирус.рф</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>08.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://xn--80aesfpebagmfblc0a.xn--p1ai/news/20200506-1230.html" target="_blank">Галина Галимова «Окружите себя устойчивыми людьми»: психолог рассказала, как контролировать тревогу» для сайта Стопкоронавирус.рф</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>06.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://msk.postupi.online/journal/novosti-vuzov/kak-rabotayut-psikhologi-  moskovskogo-instituta-psikhoanaliza-v-epokhu-karantina/" target="_blank">Как работают психологи Московского института психоанализа в «эпоху карантина»</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>01.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.bestfacts.ru/psihologicheskaya-pomoshh-v-usloviyah-pandemii/" target="_blank">Лучшие мировые новости. «Домашняя» психологическая помощь родителям и детям в условиях пандемии. Авторская статья Иванова Михаила</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>01.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://xn--80aesfpebagmfblc0a.xn--p1ai/news/20200501-1230.html" target="_blank">Наталья Ошемкова «Психолог: танцы под любимую музыку и генеральная уборка пойдут на пользу организму» для сайта Стопкоронавирус.рф</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>01.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://msk.postupi.online/journal/novosti-vuzov/kak-rabotayut-psikhologi-moskovskogo-instituta-psikhoanaliza-v-epokhu-karantina/" target="_blank">Поступи.Онлайн. «Как работают психологи Московского института психоанализа в «эпоху карантина» </a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>01.05.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://www.psychologies.ru/events/pandemiya-proverka-na-prochnost-i-vremya-dlya-tvorchestva/" target="_blank">Журнал «Псайколоджис». Реклама бесплатных вебинаров</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>30.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://moscow.psiheya-market.ru/stati/psihologiya/rabota-psihologov-v-epohu-karantina" target="_blank">Психея-Маркет. Статья «Работа психологов в эпоху карантина»</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>30.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://msk.postupi.online/journal/novosti-vuzov/studenty-podelilis-svoimi-vpechatleniyami-o-distantsionnom-obuchenii/" target="_blank">Поступи.Онлайн. Статья-отзыв от студентов Московского института психоанализа о дистанционном обучении</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>29.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://psychojournal.ru/article/2270-art-terapija-vo-vremja-samoizoljacii-podderzhka-i-iscelenie.html" target="_blank">Психологический журнал. Авторская статья Т.А.Поповой</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>28.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://xn--80aesfpebagmfblc0a.xn--p1ai/news/20200427-1530.html" target="_blank">Виктория Шиманская. Пять минут на творчество: как справляться со стрессом в условиях самоизоляции – видео для сайта Стопкоронавирус.рф</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>27.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.psychologies.ru/events/kak-sohranit-mir-s-soboy-vo-vremya-samoizolyatsii/?token=18c29edccfbb08b58cc19902a3b967cb" target="_blank">Журнал «Псайколоджис». Авторская статья Файнзильберга В.П.</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>27.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://www.ridus.ru/news/322967" target="_blank">Евгений Головинов для Ридус</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>24.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://xn--80aesfpebagmfblc0a.xn--p1ai/news/20200424-1530.html" target="_blank">Светлана Штукарева. Как преодолеть страх и апатию для Стопкоронавирус.рф</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>24.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://psychojournal.ru/article/2266-kak-rukovoditelju-organizovat-udalennuju-rabotu.html" target="_blank">Психологический журнал. Авторская статья Горюшкина Ольга Сергеевна</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>24.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://moscow.psiheya-market.ru/novosti/khoroshie_novosti/kak-byt-horoshimi-roditelyami-v-period-srednego-karantina" target="_blank">Психея Маркет. Авторская статья Григорович Л.А.</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>24.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://xn--80aesfpebagmfblc0a.xn--p1ai/news/20200429-1130.html" target="_blank">Елена Круглякова. Психологический «антивирус»: паническая атака. для Стопкоронавирус.рф</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>24.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.psychologies.ru/standpoint/samoizolyatsiya-sozdaem-usloviya-dlya-izmeneniy-k-luchshemu/" target="_blank">Владимир Шляпников рассказывает, как лучше адаптироваться к непростому периоду самоизоляции</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>23.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://xn--80aesfpebagmfblc0a.xn--p1ai/news/20200423-1500.html" target="_blank">Илья Плужников. Личное пространство, распорядок дня и ведение дневника. Рекомендации для комфортной самоизоляции – видео для сайта Стопкоронавирус.рф</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>23.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://moscow.psiheya-market.ru/stati/psihologiya/chemu-nas-uchat-dni-samoizolyacii" target="_blank">Психея Маркет. Авторская статья Курдюковой Н.А.</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>23.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://msk.academica.ru/university/novosti/19970-Moskovskij-institut-psihoanaliza/" target="_blank">Академика. Серия статей о вебинарах с 17.03.2020 по 21.04.2020</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>21.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://xn--80aesfpebagmfblc0a.xn--p1ai/news/20200420-1530.html" target="_blank">Татьяна Пуйда для Стопкоронавирус.рф</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>20.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.youtube.com/watch?v=0Miw10Np3Hg" target="_blank">Л.И. Сурат для Россия-24</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>17.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://regnum.ru/news/society/2920212.html" target="_blank">Владимир Шляпников для Regnum</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>17.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.ridus.ru/news/325149?preview=true" target="_blank">Владимир Шляпников для Ридус</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>17.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://moeobrazovanie.ru/publikacii/anons/marafon_besplatnyh_vebinarov_ot_mip_239287.html" target="_blank">Мое образование Марафон бесплатных вебинаров</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>15.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.psychologies.ru/events/obschenie-v-seti-kak-protivostoyat-kiberagressii-i-kiberbullingu/" target="_blank">Общение в Сети: как противостоять киберагрессии и кибербуллингу? Чигарькова С.В.</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>14.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.pravda.ru/health/1488786-koronavirus_Fainzilberg/" target="_blank">Владимир Файнзильберг Pravda</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>14.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://zen.yandex.ru/media/psikin/psihologiia-hamstva-v-filme-vokzal-dlia-dvoih-vymescenie-i-vytesnenie-5e914d1e378f695792304c2c" target="_blank">Канал на Яндекс.Дзен. Бесплатная реклама вебинаров Московского института психоанализа</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>13.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://msk.postupi.online/journal/novosti-vuzov/moskovskiy-institut-psikhoanaliza-zapuskaet-vebinary-dlya-pedagogov/" target="_blank">ПоступиОнлайн. Новость о бесплатных вебинарах Московского института психоанализа</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>13.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://moslenta.ru/city/sovety-psikhologov-goryachei-linii-po-koronavirusu.htm" target="_blank">Светлана Штукарева для Мослента</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>13.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="http://www.psychologies.ru/events/obschenie-v-seti-kak-protivostoyat-kiberagressii-i-kiberbullingu/" target="_blank">Г.В. Соллдатова для Psychologies</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>10.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://www.ridus.ru/news/324573" target="_blank">Михаил Иванов для Ридус</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>10.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://smartafisha.ru/blogs/prostor-dlya-tvorchestva-v-usloviyah-samoizolyacii" target="_blank">СмартАфиша Лейбин Валерий Моисеевич Авторская статья</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>09.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://msk.postupi.online/journal/novosti-vuzov/rekomendatsii-dlya-starsheklassnikov-kak-organizovat-svoyo-obuchenie-na-domu/" target="_blank">Поступи Онлайн Группа преподавателей Московского института психоанализа. Статью подготовили: Шляпников Владимир Николаевич, Григорович Любовь Алексеевна, Соловьева Екатерина Валерьевна и Демидов Александр Александрович</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>09.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.youtube.com/watch?v=twxJywIlu1c" target="_blank">Светлана Штукарева для Стопкоронавирус.рф</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>08.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://msk.postupi.online/journal/novosti-vuzov/metodicheskie-rekomendatsii-po-preodoleniyu-paniki-v-bolshikh-sotsialnykh-gruppakh/" target="_blank">Поступи Онлайн Красило Александр Иванович Авторская статья</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>07.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://msk.postupi.online/journal/novosti-vuzov/moskovskiy-institut-psikhoanaliza-zapustil-marafon-besplatnykh-vebinarov/" target="_blank">Поступи Онлайн Новость «Марафон бесплатных вебинаров»</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>06.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="https://www.psy.su/feed/8109/" target="_blank">В.Н. Шляпников для "Психологическая газета"</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top;"><strong>05.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; background-color: rgb(236, 236, 236);"><strong><a class="point" href="http://childpsy.ru/conf/36761/" target="_blank">Детская психология Пресс-релиз «Марафон бесплатных вебинаров»</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; background-color: rgb(236, 236, 236);"><strong>05.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="http://psypress.ru/events/28582.shtml" target="_blank">ПСИ-ПРЕСС Пресс-релиз «Марафон бесплатных вебинаров»</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top;"><strong>05.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://msk.citifox.ru/event/marafon-besplatnykh-vebinarov-proekt/" target="_blank">МСК.СИТИ.ФОКС. Марафон бесплатных вебинаров</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; background-color: rgb(236, 236, 236);"><strong>03.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong><a class="point" href="https://www.vesti.ru/videos/show/vid/832985/" target="_blank">А.И.Аверьянов для Россия-24</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%;"><strong>01.04.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.1tv.ru/news/2020-03-31/382931-karantin_daet_vozmozhnost_provesti_vremya_s_detmi_polezno_i_interesno" target="_blank">Михаил Иванов Первому каналу</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; background-color: rgb(236, 236, 236);"><strong>31.03.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="https://mel.fm/afisha/2501798-marafon-besplatnykh-vebinarov-v-moskovskom-institute-psikhoanaliza" target="_blank">МЕЛ.ФМ</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top;"><strong>31.03.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.ntv.ru/novosti/2311321/" target="_blank">Светлана Штукарева для НТВ</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>26.03.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="https://life.ru/p/1314854" target="_blank">Л.И. Сурат для LIFE</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top;"><strong>25.03.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.vesti.ru/videos/show/vid/831890/#" target="_blank">Олеся Васильева для Россия-24</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>24.03.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="https://ria.ru/20200324/1569069059.html" target="_blank">Михаил Иванов для РИА Новости</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top;"><strong>24.03.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.vesti.ru/videos/show/vid/831846/#" target="_blank">Виктория Шиманская для Россия-24</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>24.03.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="https://www.youtube.com/watch?v=TjRAHZmDmuo&amp;feature=youtu.be" target="_blank">Владимир Шляпников в передаче "Вечер с В. Соловьевым"</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top;"><strong>23.03.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.gazeta.ru/social/news/2020/03/23/n_14196619.shtml" target="_blank">Л.И. Сурат для Газета.ru</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong>23.03.2020</strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="2" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="https://www.psychologies.ru/events/pandemiya-ugroza-ili-novyie-vozmojnosti/" target="_blank">Журнал «Псайколоджис». Статья «Пандемия: угроза или новые возможности?» (реклама вебинаров)</a></strong></td>
			<td rowspan="1" style="text-align: left; vertical-align: top;"><strong>19.03.2020</strong></td>
		</tr>
	</tbody>
</table>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;">&nbsp;</p>

<table border="0" cellpadding="10" cellspacing="5" style="width:100%;">
	<tbody><!--tr class="hiden-row" style="display:none;"-->
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.gazeta.ru/social/2019/07/02/12471391.shtml" target="_blank">Владимир Файнзильберг в разговоре с Газетой.Ru "Убийства после психушки: кто виноват в этих преступлениях"</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="http://www.psychologies.ru/self-knowledge/6-uprajneniy-dlya-razvitiya-emotsionalnogo-intellekta-u-detey/" target="_blank">Преподаватель программы по EQ В. Шиманская для Psychologies</a></strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="http://online-zhurnaly.ru/zhurnaly/8106-psychologies-7-iyul-2019.html" target="_blank">Онлайн журнал Psychologies №7, июль 2019 - о нас стр. 116-117</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="http://www.psychologies.ru/standpoint/perestante-kontrolirovat-sebya-i-dobetes-uspeha/" target="_blank">Статья В. Шляпникова для Psychologies - в СМИ о нас</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.1tv.ru/news/issue/2018-10-18/21:00#8" target="_blank">Насколько на практике в России трудно купить оружие? Выпуск программы "Время" в 21:00, 18 октября 2018 года</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="http://www.forbes.ru/biznes/368169-travmaticheskiy-shok-chto-izvestno-o-massovom-ubiystve-v-kerchi" target="_blank">Травматический шок. Что известно о массовом убийстве в Керчи</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="http://rapsinews.ru/human_rights_protection_news/20180907/287255273.html" target="_blank">В ООН, СЕ и ОБСЕ стало больше политики и меньше прав человека – член СПЧ</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="https://www.the-village.ru/village/children/kids-news/317527-lektsiya-o-bezopasnosti" target="_blank">В Еврейском музее проведут бесплатную лекцию о безопасности детей</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="[[~6499]]" target="_blank">Психолог сборной России по скалолазанию, преподаватель Московского института психоанализа и руководитель лаборатории "Психология спорта. Базовый курс" Константин Бочавер рассказал православному порталу всю правду о большом спорте</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="[[~6498]]" target="_blank">Проект psypublic.com взял интервью у Наталии Белопольской, заведующей кафедрой клинической психологии Московского Института Психоанализа</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://otr-online.ru/news/lektorii-territoriya-rebenka-101768.html" target="_blank">Общественное телевидение России о запуске лектория "Территория ребенка"</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="https://www.mos.ru/calendar/event/44741088/" target="_blank">Официальный сайт мэра Москвы «Территория ребенка» в Еврейском музее</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="news.sputnik.ru/sport/66a567885be45677f0ff4a4ac1551e86d6581523" target="_blank">Спутник-Новости о запуске лектория "Территория ребенка"</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="https://www.culture.ru/events/294641/cikl-lekcii-territoriya-rebenka-aprelskie-vstrechi" target="_blank">Культура.рф о цикле лекций «Территория ребенка. Апрельские встречи»</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="http://prodod.moscow/archives/5431" target="_blank">К. Бочавер: «Даже чемпионы мира начали свой путь со школьного кружка, институтской секции, увлечения»</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="http://prodod.moscow/archives/5431" target="_blank">К. Бочавер: «Даже чемпионы мира начали свой путь со школьного кружка, институтской секции, увлечения»</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.culture.ru/events/275988/lekciya-zagadki-genetiki" target="_blank">Культура.рф о лекции «Загадки генетики»</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="https://www.m24.ru/news/obrazovanie/28022018/26159" target="_blank">Москва24 "Московский институт психоанализа объявляет набор на магистерскую программу"</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="http://eduhh.ru/moscow/seminar/psychology/155678" target="_blank">HH.ru_Образование о лекции «Загадки генетики»</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="https://neva.today/news/moskovskiy-institut-psihoanaliza-obyavlyaet-nabor-na-magisterskuyu-programmu-liderstvo-v-upravlenii-147324/" target="_blank">NevaToday «Лидерство в управлении гражданскими и общественными инициативами»</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="http://www.mk.ru/social/2018/02/10/moskovskiy-institut-psikhoanaliza-obyavlyaet-nabor-na-magisterskuyu-programmu-liderstvo-v-upravlenii-grazhdanskimi-i-obshhestvennymi-iniciativami.html" target="_blank">МК про новый поток магистерской программы «Лидерство в управлении гражданскими и общественными инициативами».</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="https://www.jewish-museum.ru/tolerance-center/events/navyki-konstruktivnogo-obshcheniya-mezhdu-roditelyami-i-detmi-lektsiya-eleny-asensio-martines-/" target="_blank">Еврейский музей и центр толерантности о лекции "Навыки конструктивного общения между родителями и детьми" Е. Мартинес</a></strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.asi.org.ru/event/2018/02/09/moskva-lektorij-roditeli-obuchenie-deti-territoriya/" target="_blank">Агентство социальной информации о лектори для родителей «Территория ребенка»</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="https://afisha.yandex.ru/moscow/free/kompliment-pokhvala-podderzhka" target="_blank">Яндекс.афиша о лекции "Комплимент, похвала, поддержка" Е. Мартинес </a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.kommersant.ru/doc/3557375" target="_blank">"Коммерсант" о программе "Лидерство в управлении гражданскими и общественными инициативами". Набор!</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="http://www.psychologies.ru/articles/razgadat-son-i-predotvratit-bolezn/" target="_blank">Статья Н.К.Асановой для Psychologies</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="[[~6138]]" target="_blank">Интервью С. Пьехи к конференции "Перед зависимостью все равны"</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="https://www.e-xecutive.ru/management/market/1988033-magisterskaya-programma-po-podgotovke-liderov-novogo-pokoleniya" target="_blank">"Магистерская программа по подготовке лидеров нового поколения" E-xecutive.ru</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="http://fulledu.ru/news/vuzi/news/4573_obyavlen-nabor-na-magisterskuu-programmu-liderstvo.html" target="_blank">"Магистерская программа по подготовке лидеров нового поколения" Навигатор образования</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="http://tass.ru/novosti-partnerov/4918422" target="_blank">"Магистерская программа по подготовке лидеров нового поколения" ТАСС</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="/news/v_rossijskix_vuzax_poyavitsya_sistema_raspoznavaniya_licz_dlya_sdachi_ekzamenov_onlajn._pro_mip_portal_Mel.fm
" target="_blank">В российских вузах появится система распознавания лиц для сдачи экзаменов онлайн. Про МИП портал "Mel.fm"</a></strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="/news/nauchnyij_zhurnal_mip_metodologiya_i_istoriya_psixologii" target="_blank">Научный журнал МИП «Методология и история психологии»</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="/news/kommentarij_izvestiyam_ot_prepodavatelya_mip" target="_blank">Комментарий "Известиям" от преподавателя МИП</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="/news/portal_7ya_o_lekczii_proekta_rebenok" target="_blank">Портал "7я" о лекции проекта РЕБЕНОК</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="/news/zhurnal_schastlivyie_roditeli_o_lekczii_proekta_rebenok" target="_blank">Журнал «Счастливые родители» о лекции проекта РЕБЕНОК</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="/news/minobr_nauki_rf_ob_effektivnosti_vuzov" target="_blank">Минобр науки РФ об эффективности вузов</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="/news/ria_novosti_mip_na_9_meste_v_top-vuzov_rf" target="_blank">"РИА новости": МИП на 9 месте в Топ-вузов РФ</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="/news/nashi_magistrantyi_i_prezident_rossii_v.v._putin" target="_blank">Наши магистранты в инстаграме президента РФ</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="/news/yandeks-afisha_o_proekte_rebenok_korrekcziya_rechevogo_razvitiya_u_detej" target="_blank">Яндекс-афиша о проекте РЕБЕНОК «Коррекция речевого развития у детей»</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="/news/master-klass_logopeda_t._laninoj_korrekcziya_rechevogo_razvitiya_u_detej_na_portale_7ya" target="_blank">Мастер-класс логопеда Т. Ланиной «Коррекция речевого развития у детей» на портале "7я"</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="/news/interfaks_soobshhaet_o_nabore_na_magisterskuyu_programmu_mip" target="_blank">Интерфакс сообщает о наборе на магистерскую программу МИП</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="/news/prezentacziya_knigi_e.zhurek_na_vyistavke_ekotex" target="_blank">Презентация книги Е.Журек на выставке "Экотех"</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="/news/sozdanie_obrazovatelnogo_czentra_v_udmurtii" rel="nofollow" target="_blank">Создание образовательного центра в Удмуртии</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="http://www.kommersant.ru/doc/2748325" rel="nofollow" target="_blank">МИП на 1 месте в рейтинге зарплат выпускников московских вузов 2013-го года</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="http://www.kommersant.ru/doc/2917206" rel="nofollow" style="line-height: 1.5em;" target="_blank">В Московском Институте Психоанализа проведено тестирование участников федерального проекта «Трезвая Россия»</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="news/reportazh_iz_zakryitoj_shkolyi_dlya_podrostkov">Репортаж из закрытой школы для подростков, нарушивших закон, с которыми работают психоаналитики МИП.</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="news/mama_ne_ubivaj_intervyu_asanovoi">Мама, не убивай! Интервью Асановой Н.К. для portal-kultura.ru</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="http://ug.ru/news/18299" rel="nofollow" target="_blank">В Москве прошел четвертый форум «Аутизм. Вызовы и решения»</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="news/4276">Сюжет телеканала "Просвещение ТВ" о мероприятии "Траектории развития негосударственных вузов"</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="news/interview_rektora_russia_today">Интервью ректора МИП Сурата Л.И. для интернет-издания Russia today</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="http://www.the-village.ru/village/people/experience/234129-therapist" rel="nofollow" target="_blank">Как я попробовал на себе четыре вида психотерапии (о логотерапии в МИП)</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="https://www.youtube.com/watch?v=pRJaBa76mZg" rel="nofollow" style="line-height: 1.5em;" target="_blank">Фильм телеканала ОТР &nbsp;к 160-ти летию со дня рождения З. Фрейда "Зигмунд Фрейд. По ту сторону сознания".</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="[[~4879]]" style="line-height: 1.5em;">Сойтись несходством</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="http://mel.fm/2017/02/19/lecture_abilities ">Лекция Ильи Захарова "Роль среды и генов" </a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="http://www.culture.ru/events/174979/-lektsiya-igra-v-zhizni-rebenka">Лекция Татьяны Пуйды "Игра в жизни ребенка" </a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="http://www.m24.ru/m/articles/134405">Заметки о лекции Галины Филипповой "Ребенок до рождения: от зачатия до родов"</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="http://vm.ru/news/369784.html">Лекция Марины Аромштам "Книжный старт"-Вечерняя Москва</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="http://philanthropy.ru/novosti-organizatsij/2017/04/25/49090/">Лекция Антона Захарова «Формирование пола у человека»</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="http://gorodskoyportal.ru/moskva/news/pr/34303402/?clear=1">Спектакль по пьесе В. Франкла «Синхронизация в Биркенвальде»-Городской портал </a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="http://msk24.net/cnews1945.html">Спектакль по пьесе В. Франкла «Синхронизация в Биркенвальде»-Москва24 </a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="http://www.m24.ru/articles/137683">Встреча с Павлом Лунгиным-Москва24</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="http://sovsekretno.tv/%D1%82%D0%B2%D0%BE%D1%80%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F-%D0%B2%D1%81%D1%82%D1%80%D0%B5%D1%87%D0%B0-%D1%81-%D0%BF%D0%B0%D0%B2%D0%BB%D0%BE%D0%BC-%D0%BB%D1%83%D0%BD%D0%B3%D0%B8%D0%BD%D1%8B%D0%BC/">Встреча с Павлом Лунгиным-Совершенно секретно</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="https://www.jewish-museum.ru/tolerance-center/events/priroda-nasiliya-lektsiya-sergeya-enikolopova/" target="_blank">Еврейский музей толерантности о лекции С. Ениколопова</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;"><strong><a class="point" href="http://www.psychologies.ru/events/lektsiya-priroda-nasiliya/" target="_blank">Psychologies о лекции С. Ениколопова</a> </strong></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: left; vertical-align: top; width: 2%;"><span style="color: #c90602;">➤</span></td>
			<td colspan="3" rowspan="1" style="text-align: left; vertical-align: top; width: 98%; background-color: rgb(236, 236, 236);"><strong><a class="point" href="http://www.examen.ru/news-and-articles/news/vosem-prichin-postupat-v-moskovskij-institut-psixoanaliza/">"Восемь причин поступать в Московский институт психоанализа" - Экзамен.ру </a> </strong></td>
		</tr>
	</tbody>
</table>
<style type="text/css">.point { cursor: pointer; }
.point:hover { color: #c90602; }
</style>
';

    preg_match_all("/(<a.*>(.*)<\/a>)|(<td[a-zA-Z\d\/\'\\\"\=\-\_ \;\:\(\)\,\%\.]*><strong>(.*)<\/strong><\/td>)/", $str, $output_array);
    $output_array = $output_array[4];

    $response = [];
    $id = -1;
    for ($i = 0; $i < count($output_array); $i++) {
        if (preg_match("/^\d*+\.+\d*+\.+\d*+$/", $output_array[$i])) {
            $response[$id]['date'] = date('Y-m-d', strtotime($output_array[$i]));
        } else {
            preg_match("/href=\"(.+?)\"/", $output_array[$i], $link);
            $link = $link[1] ?? null;
            preg_match("/>(.+?)</", $output_array[$i], $title);
            $title = $title[1] ?? null;

            $url = "https://www.webtran.ru/gtranslate/";

            $params = [
                "text" => $title,
                "gfrom" => "ru",
                "gto" => 'en',
            ];

            $headers = [
            ];

            $CURL = new CURL($url, "POST", $headers, $params);

            $responseCurl = $CURL->getResponse();

            $titleEng = $responseCurl;

            $response[] = [
                'title' => $title,
                'title_eng' => str_replace("\r\n", "", $titleEng),
                'link' => $link,
                'date' => null
            ];
            $id++;
        }
    }

    return $response;
});

Route::get("/testv", function () {
//    return 1;
    set_time_limit(0);
    $file = file_get_contents(public_path() . "/" . 'news.json');
    $modelData = json_decode($file, true);
    $tNews = [];
    foreach ($modelData as &$item) {
        $itemId = $item['id'];

        $columns = array_keys(json_decode(json_encode($item), true));
        $columns = array_diff($columns, ['id']);
        $columns = array_values($columns);
        $temp = [];
        foreach ($columns as $column) {
            $str = $item[$column];
            Log::info("Parse: $str");
            $str = str_replace(["\r\n", "\r", "\n"], ' ', $str);

            $v = preg_replace('/<.+?>/', ' -> $0 -> ', $str);
            $v = array_values(array_map('trim', array_diff(explode(' -> ', $v), [''])));

            foreach ($v as &$value) {
                if (!preg_match("/^<.+?>+$/ui", $value) && $value != "" && $value != "." && $value != "\r\n" && $value != "\r" && $value != "\n" && $value != "&nbsp;") {
                    $rows = preg_split('/(?<=[.?!;:])\s+(?=[a-zа-я<>\d])/ui', $value);
                    foreach ($rows as &$row) {
                        if (!preg_match("/^&[a-zA-Z\;&]*;+$/ui", $row)) {
                            $url = "https://www.webtran.ru/gtranslate/";

                            $params = [
                                "text" => $row,
                                "gfrom" => "ru",
                                "gto" => 'en',
                            ];

                            $headers = [
                            ];

                            $CURL = new CURL($url, "POST", $headers, $params);

                            $response = $CURL->getResponse();

                            $code = $CURL->getStatusCode();

                            Log::info("-----------------------------------------------------------------");
                            Log::info($code);
                            Log::info($response);
                            Log::info("-----------------------------------------------------------------");

                            $CURL = null;

                            if ($code == 200) {
                                $row = $response ?? $row;
//                                $engLang = "";
                                /*try {
                                    foreach ($response[0] as $rItem) {
                                        $engLang .= $rItem[0];
                                    }
                                    $value = $engLang;
                                } catch (Exception $e) {

                                }*/
                            } else {
                                $errors[] = $row;
                            }
                        }
                        usleep(100000);
                    }
                }
            }
            $v = implode($v);
            Log::info("New text for $column: $v");
            $temp += [$column => $v];
        }
        $temp += ['id' => $itemId];

        if (file_exists(public_path() . "/tempNews.json")) {
            $tempFile = file_get_contents(public_path() . "/tempNews.json");
            $tempFile = json_decode($tempFile, true);
            $tempFile[] = $temp;
            file_put_contents(public_path() . "/tempNews.json", json_encode($tempFile));
        } else {
            file_put_contents(public_path() . "/tempNews.json", json_encode($temp));
        }

        $tNews[] = $temp;
    }

    return $modelData;
});

Route::get("/test", function () {
//    return 1;
    set_time_limit(0);
    $file = file_get_contents(public_path() . "/" . 'news.json');
    $modelData = json_decode($file, true);
    $tNews = [];
    foreach ($modelData as &$item) {
        $itemId = $item['id'];

        $columns = array_keys(json_decode(json_encode($item), true));
        $columns = array_diff($columns, ['id']);
        $columns = array_values($columns);
        $temp = [];
        foreach ($columns as $column) {
            $str = $item[$column];
            Log::info("Parse: $str");
            $str = str_replace(["\r\n", "\r", "\n"], ' ', $str);

            $v = preg_replace('/<.+?>/', ' -> $0 -> ', $str);
            $v = array_values(array_map('trim', array_diff(explode(' -> ', $v), [''])));

            foreach ($v as &$value) {
                if (!preg_match("/^<.+?>+$/ui", $value) && $value != "" && $value != "." && $value != "\r\n" && $value != "\r" && $value != "\n" && $value != "&nbsp;") {
                    $rows = preg_split('/(?<=[.?!;:])\s+(?=[a-zа-я<>\d])/ui', $value);
                    foreach ($rows as &$row) {
                        if (!preg_match("/^&[a-zA-Z\;&]*;+$/ui", $row)) {
                            $url = "https://www.webtran.ru/gtranslate/";

                            $params = [
                                "text" => $row,
                                "gfrom" => "ru",
                                "gto" => 'en',
                            ];

                            $headers = [
                            ];

                            $CURL = new CURL($url, "POST", $headers, $params);

                            $response = $CURL->getResponse();

                            $code = $CURL->getStatusCode();

                            Log::info("-----------------------------------------------------------------");
                            Log::info($code);
                            Log::info($response);
                            Log::info("-----------------------------------------------------------------");

                            $CURL = null;

                            if ($code == 200) {
                                $row = $response ?? $row;
//                                $engLang = "";
                                /*try {
                                    foreach ($response[0] as $rItem) {
                                        $engLang .= $rItem[0];
                                    }
                                    $value = $engLang;
                                } catch (Exception $e) {

                                }*/
                            } else {
                                $errors[] = $row;
                            }
                        }
                        usleep(100000);
                    }
                }
            }
            $v = implode($v);
            Log::info("New text for $column: $v");
            $temp += [$column => $v];
        }
        $temp += ['id' => $itemId];

        if (file_exists(public_path() . "/tempNews.json")) {
            $tempFile = file_get_contents(public_path() . "/tempNews.json");
            $tempFile = json_decode($tempFile, true);
            $tempFile[] = $temp;
            file_put_contents(public_path() . "/tempNews.json", json_encode($tempFile));
        } else {
            file_put_contents(public_path() . "/tempNews.json", json_encode($temp));
        }

        $tNews[] = $temp;
    }

    return $modelData;
});

Route::get('/translate-file', function () {
    set_time_limit(0);
    $file = file_get_contents(public_path() . "/" . 'news.json');
    $modelData = json_decode($file, true);

    foreach ($modelData as &$item) {
        $itemId = $item['id'];

        $columns = array_keys(json_decode(json_encode($item), true));
        $columns = array_diff($columns, ['id']);
        $columns = array_values($columns);
        $temp = [];
        foreach ($columns as $column) {
            $str = $item[$column];
            $str = str_replace(["\r\n", "\r", "\n"], ' ', $str);

            $v = preg_replace('/<.+?>/', ' -> $0 -> ', $str);
            $v = array_values(array_map('trim', array_diff(explode(' -> ', $v), [''])));

            foreach ($v as &$value) {
                if (!preg_match("/^<.+?>+$/ui", $value) && $value != "" && $value != "." && $value != "\r\n" && $value != "\r" && $value != "\n" && $value != "&nbsp;") {
                    $rows = preg_split('/(?<=[.?!;:\-\(\)])\s+(?=[a-zа-я<>\d])/ui', $value);
                    foreach ($rows as &$row) {
                        if (!preg_match("/^&[a-zA-Z\;&]*;+$/ui", $row)) {
                            $url = "https://www.translate.ru/services/soap.asmx/GetTranslation";

                            $params = [
                                "dirCode" => "ru-en",
                                "template" => "General",
                                "text" => $row,
                                "lang" => "ru",
                                "limit" => 3000,
                                "useAutoDetect" => true,
                                "key" => 123,
                                "ts" => "MainSite",
                                "tid" => "",
                                "IsMobile" => true
                            ];

                            $headers = [
                                "Content-Type: application/json",
                            ];

                            $CURL = new CURL($url, "POST", $headers, json_encode($params));

                            $response = $CURL->getResponse();

                            $code = $CURL->getStatusCode();

                            Log::info("-----------------------------------------------------------------");
                            Log::info($code);
                            Log::info($response);
                            Log::info("-----------------------------------------------------------------");

                            $CURL = null;
                            return $response;
                            if ($code == 200) {
                                $row = $response['d']['result'] ?? $row;
//                                $engLang = "";
                                /*try {
                                    foreach ($response[0] as $rItem) {
                                        $engLang .= $rItem[0];
                                    }
                                    $value = $engLang;
                                } catch (Exception $e) {

                                }*/
                            } else {
                                $errors[] = $row;
                            }
                        }
                        usleep(500000);
                    }
                }
            }
            $v = implode($v);
            $item[$column] = $v;
        }
        return $item;
    }

    return $modelData;

});

Route::get('/translate', function () {
    set_time_limit(0);
    $models = [
        'App\Models\News' => "*"
    ];

    $errors = [];

    foreach ($models as $model => $fillable) {
        $initModel = new $model;

        if ($fillable == "*") {
            $columns = $initModel->getFillable();
            foreach ($columns as &$column) {
                if (mb_stristr($column, "_eng") === FALSE || mb_stristr($column, "Eng") === FALSE) {
                    $column = null;
                }
            }
            $columns = array_diff($columns, ['', null]);
            array_unshift($columns, 'id');
        } else {
            $columns = $fillable;
        }

        $modelData = $initModel->orderBy('id', 'DESC')->get($columns);
        return $modelData;
        foreach ($modelData as &$item) {
            $itemId = $item->id;

            $columns = array_keys(json_decode(json_encode($item), true));
            $columns = array_diff($columns, ['id']);
            $columns = array_values($columns);
            $temp = [];
            foreach ($columns as $column) {
                $str = $item->$column;
                $str = str_replace(["\r\n", "\r", "\n"], ' ', $str);

                $v = preg_replace('/<.+?>/', ' -> $0 -> ', $str);
                $v = array_values(array_map('trim', array_diff(explode(' -> ', $v), [''])));

                foreach ($v as &$value) {
                    if (!preg_match("/^<.+?>+$/ui", $value) && $value != "" && $value != "." && $value != "\r\n" && $value != "\r" && $value != "\n" && $value != "&nbsp;") {
                        $rows = preg_split('/(?<=[.?!;:\-\(\)])\s+(?=[a-zа-я<>\d])/ui', $value);
                        foreach ($rows as &$row) {
                            if (!preg_match("/^&[a-zA-Z\;&]*;+$/ui", $row)) {
                                $url = "https://translate.google.com/translate_a/single";

                                $params = [
                                    'client' => 'webapp',
                                    'sl' => 'ru',
                                    'tl' => 'en',
                                    'q' => $row,
                                    'tk' => '',
                                    'dt' => 't',
                                    'cl' => 1
                                ];

                                $headers = [
                                    "Content-Type: application/x-www-form-urlencoded"
                                ];

                                $CURL = new CURL($url, "GET", $headers, $params);

                                $response = $CURL->getResponse();

                                $code = $CURL->getStatusCode();

                                $CURL = null;

                                if ($code == 200) {
                                    $engLang = "";
                                    try {
                                        foreach ($response[0] as $rItem) {
                                            $engLang .= $rItem[0];
                                        }
                                        $value = $engLang;
                                    } catch (Exception $e) {

                                    }
                                } else {
                                    $errors[] = $row;
                                }
                            }
                        }
                    }
                }
                $v = implode($v);
                $temp += [$column => $v];
            }

            $initModel->where('id', '=', $itemId)->update($temp);
        }

        return $initModel->orderBy('id', 'DESC')->get();

    }
});

Route::get('/is_life', function () {

    $file = file_get_contents(public_path() . "/" . 'final.json');
    $file = json_decode($file, true);
    $templateId = 44;

    $templateModel = new Templates();
    $defaultTemplate = $templateModel->where('id', '=', $templateId)->get('fields');
    $defaultTemplate = json_decode($defaultTemplate[0]->fields, true);

    $pageModel = new Pages();

    $pageDatasetModel = new PageDataset();

    $dataset = $pageDatasetModel->where('page_id', '=', 29)->get('dataset');

    $dataset = json_decode($dataset[0]->dataset, true);

    foreach ($file as $item) {
        $dataset['sections']['cards']['data'][] = [
            'bgi' => $item['bgi'],
            'category' => $item['category'],
            'categoryEng' => $item['categoryEng'],
            'link' => $item['link'],
            'period' => $item['period'],
            'periodEng' => $item['periodEng'],
            'title' => $item['title'],
            'titleEng' => $item['titleEng']
        ];

        $pageId = $pageModel->orderBy('id', 'DESC')->limit(1)->get('id');
        $pageId = $pageId[0]->id + 1;

        $pageModel->insert([
            'id' => $pageId,
            'route' => $item['link'],
            'title' => $item['title'],
            'title_eng' => $item['titleEng'],
            'template_id' => $templateId,
            'user_id' => 1
        ]);

        $datasetId = $pageDatasetModel->orderBy('id', 'DESC')->limit(1)->get('id');
        $datasetId = $datasetId[0]->id + 1;

        $tempDataset = $defaultTemplate;

        $tempDataset['sections']['titles']['data'][] = [
            'title' => $item['title'],
            'title_eng' => $item['title']
        ];

        $tempDataset['sections']['texts']['data'][] = [
            'text' => $item['content'],
            'text_eng' => $item['content']
        ];

        $pageDatasetModel->insert([
            'id' => $datasetId,
            'page_id' => $pageId,
            'dataset' => json_encode($tempDataset)
        ]);

    }
    $pageDatasetModel->where('page_id', '=', 29)->update(['dataset' => json_encode($dataset)]);
    return $dataset;

    $pages = [];

    $cards = [];

    return $file;

});

Route::get('/st_life', function () {

    set_time_limit(0);

    $base = "http://inpsycho.ru/";

    $path = public_path('/files/StudentLife');
    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';

    $query = <<<SQL1
    SELECT
pagetitle, description, content, alias, createdon
FROM `modx_site_content`
WHERE parent = 53 AND published = 1
ORDER BY createdon ASC
SQL1;

    $stLife = \DB::select(\DB::raw($query));

    $response = [];

    foreach ($stLife as $item) {
        $content = str_replace(["\r\n", "\r", "\n"], ' ', $item->content);
        $preview = "";
        $v = preg_replace('/<.+?>/', ' -> $0 -> ', $content);
        $v = array_values(array_map('trim', array_diff(explode(' -> ', $v), [''])));

        foreach ($v as &$value) {
            preg_match('/src=\"(.*?)\"/ui', $value, $output_array);

            if (isset($output_array[1])) {
                try {
                    $output_array[1] = str_replace(" ", "%20", $output_array[1]);
                    if (!preg_match("/(http)|(https)/ui", $output_array[1])) {
                        $img = file_get_contents($base . $output_array[1]);
                    } else {
                        $img = file_get_contents($output_array[1]);
                    }
                    $type = explode('.', $output_array[1]);
                    $name = substr(str_shuffle($permitted_chars), 0, 10);
                    file_put_contents($path . "/$name." . $type[count($type) - 1], $img);
                    if ($preview == "") {
                        $preview = "http://77.222.59.67:996/files/StudentLife/$name." . $type[count($type) - 1];
                    }
                    $value = str_replace("src=\"" . urldecode($output_array[1]) . "\"", "src=\"http://77.222.59.67:996/files/StudentLife/$name." . $type[count($type) - 1] . '"', $value);

                } catch (Exception $e) {
//                    return $e->getMessage();
                    $value = str_replace("src=\"" . urldecode($output_array[1]) . "\"", "src=\"http://77.222.59.67:996/files/StudentLife/$name." . $type[count($type) - 1] . '"', $value);
                }
            }

        }
        $content = implode($v);

        $response[] = [
            'bgi' => $preview,
            'category' => '',
            'categoryEng' => '',
            'link' => "/$item->alias",
            'period' => date('d.m.Y', $item->createdon),
            'periodEng' => date('d.m.Y', $item->createdon),
            'title' => $item->pagetitle,
            'titleEng' => $item->pagetitle,
            'content' => $content
        ];

//        return date('d.m.Y', $item->createdon);
    }

    return $response;
});

/*Route::get('/static-stubs', function () {
    $model = new StaticStubs();

    $data = $model->where('template', '=', 'HigherEducationDetail')->firstOrFail('stub');

    $data = json_decode($data->stub, true);

    $data['sections']['specialties']['fields']['type']['options'][] = [
                            "name" => "Аспирантура",
                            "value" => "4"
                        ];

    $model->where('template', '=', 'HigherEducationDetail')->update(['stub' => json_encode($data)]);

    $data = $model->where('template', '=', 'HigherEducationDetail')->firstOrFail('stub');

    return json_decode($data->stub, true);
});*/
/*Route::get('/page-info', function () {

    $model = new Pages;

    $templateId = $model->where('route', '=', '/our_partners')->firstOrFail('template_id');
    $pageId = $model->where('route', '=', '/our_partners')->firstOrFail('id');

    $templateModel = new Templates();
    $pageModel = new PageDataset();

    /*$fields = $templateModel->where('id', '=', $templateId->template_id)->firstOrFail('fields');
    $fields = json_decode($fields->fields, true);
    $fields['sections']['BlockWithPolls']['fields']['description']['type'] = "textarea";
    $fields['sections']['BlockWithPolls']['fields']['descriptionEng']['type'] = "textarea";

    $templateModel->where('id', '=', $templateId->template_id)->update(['fields' => json_encode($fields)]);*/

    /*$dataset = $pageModel->where('page_id', '=', $pageId->id)->firstOrFail('dataset');
    $dataset = json_decode($dataset->dataset, true);

    $dataset['sections']['BlockWithPolls']['fields']['description']['type'] = "textarea";
    $dataset['sections']['BlockWithPolls']['fields']['descriptionEng']['type'] = "textarea";


    $pageModel->where('page_id', '=', $pageId->id)->update(['dataset' => json_encode($dataset)]);
    $fields = $templateModel->where('id', '=', $templateId->template_id)->firstOrFail('fields');
    $dataset = $pageModel->where('page_id', '=', $pageId->id)->firstOrFail('dataset');
    return [
        'template_dataset' => json_decode($fields->fields),
        'page_dataset' => json_decode($dataset->dataset)
    ];*/
//});

/*Route::get('/dpu_upd', function () {
    $file = file_get_contents(public_path() . "/" . 'dpoFinal.json');
    $file = json_decode($file, true);

    $modelAdditionalEducation = new \App\Models\AdditionalEducationSections();

    $modelAdditionalEducationDetail = new \App\Models\AdditionalEducation();

    $mainid = $modelAdditionalEducation->orderBy('id', 'DESC')->limit(1)->get('id');
    $mainid = $mainid[0]->id + 1;

    $smainid = $modelAdditionalEducationDetail->orderBy('id', 'DESC')->limit(1)->get('id');
    $smainid = $smainid[0]->id + 1;

    foreach ($file as $item) {
        $categoryId = $modelAdditionalEducation->where('title', '=', $item['category'])->get('id');

        if (count($categoryId) == 0) {
            $modelAdditionalEducation->insert([
                [
                    'id' => $mainid,
                    'title' => $item['category'],
                    'title_eng' => $item['category']
                ]
            ]);

            $categoryId = $modelAdditionalEducation->where('title', '=', $item['category'])->get('id');
        }

        $categoryId = $categoryId[0]->id;

        foreach ($item['data'] as &$cItem) {

            foreach ($cItem as &$ii) {
                $ii = str_replace('http://77.222.59.67:996/files/news', 'http://77.222.59.67:996/files/AdditionalEducation', $ii);
            }

            $metId = $modelAdditionalEducationDetail->where('title', '=', $cItem['title'])->get('id');

            if (count($metId) == 0) {
                $modelAdditionalEducationDetail->insert([
                    [
                        'id' => $smainid,
                        'title' => $cItem['title'],
                        'title_eng' => $cItem['title']
                    ]
                ]);
                $metId = $modelAdditionalEducationDetail->where('title', '=', $cItem['title'])->get('id');
            }

            $metId = $metId[0]->id;

            $cItem += ['section_id' => $categoryId];

            $modelAdditionalEducationDetail->where('id', '=', $metId)->update(
                $cItem
            );
            $smainid++;
        }
        $mainid++;
    }
});*/

//Route::get('/dpo_up', function () {
//    set_time_limit(0);
//    $file = file_get_contents(public_path() . "/" . 'dopData.json');
//    $file = json_decode($file, true);
//
//    $modelAdditionalEducation = new \App\Models\AdditionalEducationSections();
//
//    $modelAdditionalEducationDetail = new \App\Models\AdditionalEducation();
//
//    $base = "http://inpsycho.ru/";
//
//    $path = public_path('/files/AdditionalEducation');
//    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
//
//    $response = [];
//
//    foreach ($file as $item) {
//        /*$categoryId = $modelAdditionalEducation->where('title', '=', $item['category'])->get('id');
//
//        if (count($categoryId) == 0) {
//            $modelAdditionalEducation->insert([
//                [
//                    'title' => $item['category'],
//                    'title_eng' => $item['category']
//                ]
//            ]);
//            $categoryId = $modelAdditionalEducation->where('title', '=', $item['category'])->get('id');
//        }
//
//        $categoryId = $categoryId[0]->id;*/
//        $tempT = [];
//        foreach ($item['data'] as $dataItem) {
//            $temp = [
//                'about_program_text' => "",
//                'about_program_text_eng' => "",
//                'structure_text' => "",
//                'structure_text_eng' => "",
//                'text_admission' => "",
//                'text_admission_eng' => "",
//                'text_price' => "",
//                'text_price_eng' => "",
//                'text_reviews' => "",
//                'text_reviews_eng' => ""
//            ];
//
//            foreach ($dataItem['data'] as $key => &$metData) {
//                $content = str_replace(["\r\n", "\r", "\n"], ' ', $metData);
/*                $v = preg_replace('/<.+?>/', ' -> $0 -> ', $content);*/
//                $v = array_values(array_map('trim', array_diff(explode(' -> ', $v), [''])));
//
//                foreach ($v as &$value) {
//                    preg_match('/src=\"(.*?)\"/ui', $value, $output_array);
//
//                    if (isset($output_array[1])) {
//                        try {
//                            $output_array[1] = str_replace(" ", "%20", $output_array[1]);
//                            $img = file_get_contents($base . $output_array[1]);
//                            $type = explode('.', $output_array[1]);
//                            $name = substr(str_shuffle($permitted_chars), 0, 10);
//                            file_put_contents($path . "/$name." . $type[count($type) - 1], $img);
//                            $value = str_replace("src=\"" . urldecode($output_array[1]) . "\"", "src=\"http://77.222.59.67:996/files/news/$name." . $type[count($type) - 1] . '"', $value);
//
//                        } catch (Exception $e) {
//                            $value = $value;
//                        }
//                    }
//
//                }
//                $content = implode($v);
//                $metData = $content;
//
//                if ($key == "how_to_proceed") {
//                    $temp['text_admission'] = $metData;
//                    $temp['text_admission_eng'] = $metData;
//                }
//
//                if ($key == "cost") {
//                    $temp['text_price'] = $metData;
//                    $temp['text_price_eng'] = $metData;
//                }
//
//                if ($key == "about_program") {
//                    $temp['about_program_text'] = $metData;
//                    $temp['about_program_text_eng'] = $metData;
//                }
//
//                if ($key == "structure_program") {
//                    $temp['structure_text'] = $metData;
//                    $temp['structure_text_eng'] = $metData;
//                }
//
//                if ($key == "reviews") {
//                    $temp['text_reviews'] = $metData;
//                    $temp['text_reviews_eng'] = $metData;
//                }
//            }
//
//            $temp = array_diff($temp, [""]);
//            $temp += ['title' => $dataItem['dpo_name'], 'title_eng' => $dataItem['dpo_name']];
//            $tempT[] = $temp;
//           /* $metId = $modelAdditionalEducationDetail->where('title', '=', $dataItem['dpo_name'])->get('id');
//            if (count($metId) == 0) {
//                $modelAdditionalEducationDetail->insert([
//                    [
//                        'title' => $dataItem['dpo_name'],
//                        'title_eng' => $dataItem['dpo_name']
//                    ]
//                ]);
//                $metId = $modelAdditionalEducationDetail->where('title', '=', $dataItem['dpo_name'])->get('id');
//            }
//            $metId = $metId[0]->id;
//            return $dataItem;
//            $response[] = [
//
//            ];*/
//        }
//
//        $response[] = [
//            'category' => $item['category'],
//            'data' => $tempT
//        ];
//    }
//
//    return $response;
//});

/*Route::get('/parse-dpo', function () {
    $list = [
        'Психоанализ, психоаналитическое консультирование',
        'Психологическое консультирование',
        'Клиническая психология',
        'Психология и психотерапия детей и подростков',
        'Дефектология',
        'Логотерапия',
        'Коучинг. Международные программы',
        'Программы Московской школы практической психологии',
    ];

    $idList = [
        5441,
        5442,
        5443,
        5444,
        5445,
        5446,
        5447,
        7702
    ];

    $response = [];

    foreach ($list as $categoryId => $category) {
        $query = <<<SQL1
    SELECT
    id, pagetitle
FROM `modx_site_content`
WHERE parent = '$idList[$categoryId]'
ORDER BY pagetitle ASC
SQL1;

        $listDpo = \DB::select(\DB::raw($query));

        $temp = [];

        foreach ($listDpo as $dpoItem) {

            $query = <<<SQL1
    SELECT
pagetitle, content, menutitle
FROM `modx_site_content`
WHERE parent = '$dpoItem->id' AND published = 1 AND (menutitle = 'О программе' OR menutitle = 'Структура программы' oR menutitle = 'Содержание программы' OR menutitle = 'Требования к слушателям' OR menutitle = 'Преподаватели' OR menutitle = 'Стоимость' OR menutitle = 'Видео' OR menutitle = 'Как поступить' OR menutitle = 'Отзывы')
ORDER BY pagetitle ASC
SQL1;

            $dpoData = \DB::select(\DB::raw($query));

            $tempData = [];

            foreach ($dpoData as $dataItem) {
                switch ($dataItem->menutitle) {
                    case 'Преподаватели':
                            $tempData += ['teachers' => $dataItem->content];
                        break;
                    case 'Как поступить':
                            $tempData += ['how_to_proceed' => $dataItem->content];
                        break;
                    case 'Стоимость':
                            $tempData += ['cost' => $dataItem->content];
                        break;
                    case 'О программе':
                            $tempData += ['about_program' => $dataItem->content];
                        break;
                    case 'Содержание программы':
                            $tempData += ['content_program' => $dataItem->content];
                        break;
                    case 'Структура программы':
                            $tempData += ['structure_program' => $dataItem->content];
                        break;
                    case 'Требования к слушателям':
                            $tempData += ['requirements_for_listeners' => $dataItem->content];
                        break;
                    case 'Отзывы':
                            $tempData += ['reviews' => $dataItem->content];
                        break;
                }
            }

            if (!empty($tempData)) {
                $temp[] = [
                    'dpo_name' => $dpoItem->pagetitle,
                    'data' => $tempData
                ];
            }

        }

        $response[] = [
            'category' => $category,
            'data' => $temp
        ];

    }

    return $response;

});*/

//Route::get('/upd', function () {
//    $model = new \App\Models\News();
//    $data = $model->get();
//
//    foreach ($data as &$item) {
//        /*$item->preview = str_replace('..png', '.png', $item->preview);
//        $item->preview = str_replace('..PNG', '.PNG', $item->preview);
//        $item->preview = str_replace('..jpg', '.jpg', $item->preview);
//        $item->preview = str_replace('..JPG', '.JPG', $item->preview);
//        $item->preview = str_replace('..jpeg', '.jpeg', $item->preview);
//        $item->preview = str_replace('..JPEG', '.JPEG', $item->preview);*/
//
//        if (!preg_match('/http:\/\/77\.222\.59\.67:996\/files\/news\/[a-zA-Z\dа-яА-Я]*\.[a-zA-Z]*/ui', $item->preview)) {
//            $item->preview = "";
//            $model->where('id', '=', $item->id)->update([
//                'preview' => $item->preview
//            ]);
//        }
//
//        /*$model->where('id', '=', $item->id)->update([
//            'preview' => $item->preview
//        ]);*/
//        /*$item->content = str_replace('77.222.59.67', 'http://77.222.59.67', $item->content);
//        $item->content_eng = str_replace('77.222.59.67', 'http://77.222.59.67', $item->content_eng);
//        $model->where('id', '=', $item->id)->update([
//            'content' => $item->content,
//            'content_eng' => $item->content_eng
//        ]);*/
//    }
//
//    $model = new \App\Models\News();
//    $data = $model->get();
//    return $data;
//});

/*Route::get('/vitaly-pidaras', function () {
    $query = <<<SQL1
    SELECT
id, type, contentType, pagetitle, longtitle, description, alias, link_attributes, published, pub_date, unpub_date, parent, isfolder, introtext, content, richtext, template, menuindex, searchable, cacheable, createdby, createdon, editedby, editedon, deleted, deletedon, deletedby, publishedon, publishedby, menutitle, donthit, privateweb, privatemgr, content_dispo, hidemenu, class_key, context_key, content_type, uri, uri_override, hide_children_in_tree, show_in_tree, properties, alias_visible
FROM `modx_site_content`
WHERE uri LIKE 'edu_all/edu_add%' AND published = 1 AND alias LIKE 'struktura%'
SQL1;

    return \DB::select(\DB::raw($query));

});*/

/*Route::get('/iput', function () {
    $model = new \App\Models\News();

    $file = file_get_contents(public_path() . "/" . 'todb.json');
    $file = json_decode($file, true);
    $id = $model->orderBy('id', 'DESC')->limit(1)->get('id');
    $id = $id[0]->id + 1;

    foreach ($file as &$item) {
        $item['id'] = $id;
        $id += 1;
    }

    $model->insert($file);
//    $model->save();
});*/

/*Route::get('/set', function () {
    set_time_limit(0);
    $file = file_get_contents(public_path() . "/" . 'response.json');
    $file = json_decode($file, true);

    $base = "http://inpsycho.ru/";

    $path = public_path('/files/news');
    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';

    $response = [];

    $cat = [
        "Жизнь института",
        "СМИ",
        "Обучение",
        "Проекты",
        "Ректор"
    ];

    foreach ($file as $item) {
        $title = $item['pagetitle'];
        $content = $item['content'];
        $createdAt = date('Y-m-d', $item['createdon']);
        $content = str_replace(["\r\n", "\r", "\n"], ' ', $content);
        $preview = "";
        $v = preg_replace('/<.+?>/', ' -> $0 -> ', $content);
        $v = array_values(array_map('trim', array_diff(explode(' -> ', $v), [''])));

        foreach ($v as &$value) {
            preg_match('/src=\"(.*?)\"/ui', $value, $output_array);

            if (isset($output_array[1])) {
                try {
                    $output_array[1] = str_replace(" ", "%20", $output_array[1]);
                    $img = file_get_contents($base . $output_array[1]);
                    $type = explode('.', $output_array[1]);
                    $name = substr(str_shuffle($permitted_chars), 0, 10);
                    file_put_contents($path . "/$name." . $type[1], $img);
                    $preview = "http://77.222.59.67:996/files/news/$name" . $type[1];
                    $value = str_replace("src=\"" . urldecode($output_array[1]) . "\"", "src=\"77.222.59.67:996/files/news/$name." . $type[1] . '"', $value);

                } catch (Exception $e) {
                    $value = $value;
                }
            }

        }
        $content = implode($v);
        $response[] = [
            'title' => $title,
            'title_eng' => $title,
            'preview' => $preview,
            'content' => $content,
            'content_eng' => $content,
            'public_date' => $createdAt,
            'category' => $cat[rand(0, 4)],
        ];
    }

    return $response;
});*/

/*Route::get('/all-news', function () {
    $model = new \App\Models\News();
    $data = $model->orderBy('id', 'ASC')->get('title');
    $r = [];
    foreach ($data as $item) {
        $r[] = "pagetitle != '$item->title'";
    }

    $qu = implode($r, " AND ");

    return $qu;
});*/

/*Route::get('/update-page', function () {
    $page = Pages::where('route', '=', '/international_associations')->get(['id', 'template_id']);
    $pageId = $page[0]->id;

    $template = PageDataset::where('page_id', '=', $pageId)->get('dataset');
    $template = json_decode($template[0]->dataset, true);
    $template['sections']['BlockWithPolls']['fields']['description']['type'] = "textarea";
    $template['sections']['BlockWithPolls']['fields']['descriptionEng']['type'] = "textarea";

    PageDataset::where('page_id', '=', $pageId)->update([
        'dataset' => json_encode($template)
    ]);

});

Route::get('/page_dataset', function () {
    $pages = PageDataset::orderBy('id', 'DESC')->get();
    foreach ($pages as $page) {
        $page->dataset = json_decode($page->dataset, true);
    }
    return $pages;
});

Route::get('/all_pages', function () {
    $pages = Pages::orderBy('id', 'DESC')->get();
    return $pages;
});

Route::get('/all_templates', function () {
    $pages = Templates::orderBy('id', 'DESC')->get();
    foreach ($pages as $page) {
        $page->fields = json_decode($page->fields, true);
    }
    return $pages;
});*/

/*Route::get('/all', function () {
    $baseUrl = "App\Models\\";
    $models = [
        'Templates',
        'Pages',
        'PageDataset',
        'Questions',
        'QuestionAnswer',
        'News',
        'Teachers',
        'Lecturers',
        'LectureHall',
        'StaticStubs',
        'HigherEducation',
        'HigherEducationProfile'
    ];

    $response = [];

    foreach ($models as $model) {
        $urlModel = $baseUrl . $model;
        $initModel = new $urlModel;
        $response += [$model => $initModel->orderBy('id', 'ASC')->get()];
    }

    return $response;
});

Route::get('/upd-all', function () {

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "91.223.123.199:996/api/all",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $response = json_decode($response, true);

    $baseUrl = "App\Models\\";

    foreach ($response as $model => $rows) {
        $urlModel = $baseUrl . $model;
        $initModel = new $urlModel;
        $initModel->truncate();
        $upd = [];
        foreach ($rows as $row) {
            $temp = [];
            foreach ($row as $column => $item) {
                $temp += [$column => (($column != "id") && ($column != "user_id") && ($column != "template_id")) ? str_replace('91.223.123.199', '77.222.59.67', $item) : $item];
            }
            $upd[] = $temp;
        }
        $initModel->insert($upd);
    }

});
*/

Route::get('/student_life_update', function () {
    $pageId = Pages::where('route', '=', '/student_life')->get('id');
    $pageId = $pageId[0]->id;

    $dataset = PageDataset::where('page_id', '=', $pageId)->get('dataset');
    $dataset = json_decode($dataset[0]->dataset, true);

    return $dataset['sections']['cards']['data'];

    foreach ($dataset['sections']['cards']['data'] as $item) {
        $tempPageId = Pages::where('route', '=', $item['link'])->get('id');
        $tempPageId = $tempPageId[0]->id;

        $tempDataset = PageDataset::where('page_id', '=', $tempPageId)->get('dataset');
        $tempDataset = json_decode($tempDataset[0]->dataset, true);

        $add = true;

        foreach ($tempDataset['sections']['flags']['data'] as $dataItem) {
            if (mb_strtolower($dataItem['name']) == "форма") {
                $add = false;
            }
        }

        if ($add) {
            $tempDataset['sections']['flags']['data'][] = [
                'name' => 'Форма',
                'value' => "0"
            ];
        }

        PageDataset::where('page_id', '=', $tempPageId)->update([
            'dataset' => json_encode($tempDataset)
        ]);

    }


});

Route::get('/page_dataset', function () {
     $pages = PageDataset::all();
    foreach ($pages as $page) {
        $page->dataset = json_decode($page->dataset, true);
    }
    return $pages;
});

Route::get('/all_pages', function () {
    $pages = Pages::orderBy('id', 'DESC')->get();
    return $pages;
});

Route::get('/static_stub', function () {
    $pages = StaticStubs::get('stub');
    foreach ($pages as &$page) {
        $page->stub = json_decode($page->stub, true);
    }
    return $pages;
    $pages = StaticStubs::where('template', '=', 'AdditionalEducationalLecturers')->get('stub');
    $pages = json_decode($pages[0]->stub, true);

//    $pages['sections']['lecturers']['fields']['program_dpo']['title'] = 'Программа доп. образования';
//    $pages['sections']['lecturers']['fields']['program_dpo_eng']['title'] = 'Программа доп. образования (Англ)';
    $pages['sections']['lecturers']['fields'] += ['direction_id' => [
        'title' => "ID направления",
        'type' => 'text'
    ]];

    StaticStubs::where('template', '=', 'AdditionalEducationalLecturers')->update([
        'stub' => json_encode($pages)
    ]);

    return $pages;
});

Route::get('/all_templates', function () {
    $pages = Templates::orderBy('id', 'DESC')->get();
    foreach ($pages as $page) {
        $page->fields = json_decode($page->fields, true);
    }
    return $pages;
});

Route::get('/qwez', function () {

    $page = PageDataset::where('page_id', '=', 47)->get('dataset');

    $page = json_decode($page[0]->dataset, true);

    $page['sections']['cards']['fields'] += [
        "image" => [
            "title" => "Изображение",
            "type" => "text",
        ],
    ];

    PageDataset::where('page_id', '=', 47)->update([
        'dataset' => json_encode($page)
    ]);

    $page = PageDataset::where('page_id', '=', 47)->get('dataset');

    $page = json_decode($page[0]->dataset, true);

    return $page;

});


Route::get('/insert-new-stub', function () {
    $templateId = Templates::orderBy('id', 'DESC')->limit(1)->value('id');
    $templateId++;
    $pageId = Pages::orderBy('id', 'DESC')->limit(1)->value('id');
    $pageId++;
    $datasetId = PageDataset::orderBy('id', 'DESC')->limit(1)->value('id');
    $datasetId++;

    Templates::insert([
        'id' => $templateId,
        'name' => 'ContactsBlock404',
        'title' => 'Блок контактов',
        'fields' => json_encode([
            "title" => "Блок контактов",
            "titleEng" => "Contacts block",
            "template" => "404",
            "sections" => [
                "text_block" => [
                    "title" => "Блок текста",
                    "fields" => [
                        "text" => [
                            "title" => "Текст",
                            "type" => "textarea",
                        ],
                        "text_eng" => [
                            "title" => "Текст (Англ.)",
                            "type" => "textarea",
                        ],
                    ],
                    "data" => [],
                ],
                "phones" => [
                    "title" => "Телефоны",
                    "fields" => [
                        "phone" => [
                            "title" => "Телефон",
                            "type" => "text",
                        ],
                    ],
                    "data" => [],
                ],
                "emails" => [
                    "title" => "Эл. почты",
                    "fields" => [
                        "email" => [
                            "title" => "Почта",
                            "type" => "text",
                        ],
                    ],
                    "data" => [],
                ],
            ]
        ])
    ]);

    Pages::insert([
        'id' => $pageId,
        'route' => '/block_contacts',
        'title' => 'Блок контактов',
        'title_eng' => 'Contacts block',
        'template_id' => $templateId,
        'user_id' => 1
    ]);

    PageDataset::insert([
        'id' => $datasetId,
        'page_id' => $pageId,
        'dataset' => json_encode([
            "title" => "Блок контактов",
            "titleEng" => "Contacts block",
            "template" => "404",
            "sections" => [
                "text_block" => [
                    "title" => "Блок текста",
                    "fields" => [
                        "text" => [
                            "title" => "Текст",
                            "type" => "textarea",
                        ],
                        "text_eng" => [
                            "title" => "Текст (Англ.)",
                            "type" => "textarea",
                        ],
                    ],
                    "data" => [],
                ],
                "phones" => [
                    "title" => "Телефоны",
                    "fields" => [
                        "phone" => [
                            "title" => "Телефон",
                            "type" => "text",
                        ],
                    ],
                    "data" => [],
                ],
                "emails" => [
                    "title" => "Эл. почты",
                    "fields" => [
                        "email" => [
                            "title" => "Почта",
                            "type" => "text",
                        ],
                    ],
                    "data" => [],
                ],
            ]
        ])
    ]);
});

Route::get('/insert-to-stub', function () {
    $page = Pages::where('route', '=', '/cathedra_info')->value('id');
    $data = json_decode(PageDataset::where('page_id', '=', $page)->value('dataset'), true);
    $data['sections'] += ["events" => [
        "title" => "События",
        "fields" => [
            "cathedra_id" => [
                "title" => "ID кафедры",
                "type" => "number",
            ],
            "title" => [
                "title" => "Заголовок",
                "type" => "text",
            ],
            "title_eng" => [
                "title" => "Заголовок (Англ.)",
                "type" => "text",
            ],
            "description" => [
                "title" => "Описание",
                "type" => "text",
            ],
            "description_eng" => [
                "title" => "Описание (Англ.)",
                "type" => "text",
            ],
            "place_name" => [
                "title" => "Название места",
                "type" => "text",
            ],
            "place_name_eng" => [
                "title" => "Название места (Англ.)",
                "type" => "text",
            ],
            "place_address" => [
                "title" => "Адрес места",
                "type" => "text",
            ],
            "place_address_eng" => [
                "title" => "Адрес места (Англ.)",
                "type" => "text",
            ],
            "date_start" => [
                "title" => "Дата начала",
                "type" => "datetime-local",
            ],
            "date_end" => [
                "title" => "Дата окончания",
                "type" => "datetime-local",
            ],
            "link" => [
                "title" => "Ссылка",
                "type" => "text",
            ],
        ],
        "data" => [],
    ]];

    PageDataset::where('page_id', '=', $page)->update([
        'dataset' => json_encode($data)
    ]);

    $data = json_decode(PageDataset::where('page_id', '=', $page)->value('dataset'), true);

    return $data;
});

Route::get('/copy-stub', function () {
    $sub = json_decode(StaticStubs::where('template', '=', 'SubDepartment')->value('stub'), true);
    $temp = [
        'department_id' => [
            'title' => 'ID департамента',
            'type' => 'number'
        ]
    ];
    $result = array_merge($temp, $sub['sections']['sub_department']['fields']);
    $sub['sections']['sub_department']['fields'] = $result;
    StaticStubs::where('template', '=', 'SubDepartment')->update([
        'stub' => json_encode($sub)
    ]);

});



Route::post('/send-mail', 'App\Http\Controllers\Service\MailController@mail');

Route::group(['middleware' => ['cors']], function () {

    Route::post('/get-content', 'App\Http\Controllers\Service\ContentController@index');

    Route::post('/answer-the-questionnaire', 'App\Http\Controllers\ContentController@answerQuestionnaire');

    Route::group(['middleware' => ['AuthenticateUser', 'AuthenticateAdmin']], function () {

        Route::post('/create-content', 'App\Http\Controllers\Service\ContentController@create');

        Route::post('/update-content', 'App\Http\Controllers\Service\ContentController@update');

        Route::post('/delete-content', 'App\Http\Controllers\Service\ContentController@delete');

        Route::post('/create-page', 'App\Http\Controllers\ContentController@createPage');

        Route::post('/delete-page', 'App\Http\Controllers\ContentController@deletePage');

    });

    Route::get('/get-page', 'App\Http\Controllers\ContentController@getPage');

    Route::get('/search', 'App\Http\Controllers\Service\ContentController@search');
});

Route::group([
    'prefix' => '/user',
    'middleware' => [
        'cors'
    ]],
    function () {

        Route::group(['prefix' => '/auth'], function () {

            Route::post('/login', 'App\Http\Controllers\Auth\AuthController@login');

            Route::post('/registration', 'App\Http\Controllers\Auth\AuthController@registration');

        });

        Route::group(['prefix' => '/admin', 'middleware' => ['AuthenticateUser', 'AuthenticateAdmin']], function () {

            Route::group(['prefix' => '/list'], function () {

                Route::get('/users', 'App\Http\Controllers\Service\Admin\ListController@usersList');

                Route::get('/teachers', 'App\Http\Controllers\Service\Admin\ListController@teachersList');

                Route::get('/pages', 'App\Http\Controllers\Service\Admin\ListController@pagesList');

                Route::get('/roles', 'App\Http\Controllers\Service\Admin\ListController@roleList');

            });

            Route::post('/create-group', 'App\Http\Controllers\Service\Admin\GroupController@createGroup');

            Route::post('/delete-group', 'App\Http\Controllers\Service\Admin\GroupController@deleteGroup');

            Route::post('/add-to-group', 'App\Http\Controllers\Service\Admin\GroupController@addToGroup');

            Route::post('/remove-page-on-group', 'App\Http\Controllers\Service\Admin\GroupController@removePageOnGroup');

            Route::get('/groups', 'App\Http\Controllers\Service\Admin\GroupController@getGroupList');

            Route::post('/change-user-role', 'App\Http\Controllers\Service\Admin\UserController@changeUserRole');

            Route::post('/remove-user', 'App\Http\Controllers\Service\Admin\UserController@removeUser');

            Route::group(['prefix' => '/file-storage'], function () {

                Route::get('/file-list', 'App\Http\Controllers\Service\Admin\FileController@getList');

                Route::post('/upload-files', 'App\Http\Controllers\Service\Admin\FileController@uploadFiles');

                Route::post('/remove-file', 'App\Http\Controllers\Service\Admin\FileController@removeFile');

                Route::post('/remove-dir', 'App\Http\Controllers\Service\Admin\FileController@removeDir');

                Route::post('/create-folder', 'App\Http\Controllers\Service\Admin\FileController@createFolder');

            });

        });

    });

Route::group([
    'prefix' => '/pages',
    'middleware' => [
        'cors' /*'AuthenticateUser'*/
    ]],
    function () {


    });
