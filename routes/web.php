<?php

use Illuminate\Support\Facades\Route;


Route::group(['namespace' => 'App\Http\Controllers'], function () {
    Route::get('/auth', 'GateController@admin');

    Route::get('/admin', 'GateController@admin');

    Route::get('/admin/{any}', 'GateController@admin')->where('any', '.*');

    Route::get('/{any}', 'GateController@index')->where('any', '.*');
});

