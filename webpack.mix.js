const mix = require('laravel-mix');

mix.webpackConfig({
    resolve: {
        extensions : [ '.js', '.vue', '.json' , '.svg'],
        alias: {
            '@': __dirname + '/resources/js'
        },
    },
});

mix.js('resources/js/main.js', 'public/js').vue();
