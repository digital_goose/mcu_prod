<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{

    protected $table = "faculty";

    protected $fillable = [
        'title',
        'title_eng',
        'background_image',
        'alert_text',
        'alert_text_eng',
        'alert_link_title',
        'alert_link_title_eng',
        'alert_link',
    ];

    public $timestamps = false;

}
