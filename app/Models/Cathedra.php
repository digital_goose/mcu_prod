<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Cathedra extends Model
{

    protected $table = "cathedra";

    protected $fillable = [
        'faculty_id',
        'title',
        'title_eng',
        'background_image',
        'alert_text',
        'alert_text_eng',
        'alert_link_title',
        'alert_link_title_eng',
        'alert_link'
    ];

    public $timestamps = false;

}
