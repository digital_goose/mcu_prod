<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PageDataset extends Model
{

    protected $table = 'page_dataset';

    protected $fillable = [
        'id',
        'page_id',
        'dataset'
    ];

    public $timestamps = false;

}
