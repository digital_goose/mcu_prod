<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class HigherEducationProfile extends Model
{

    protected $table = 'higher_education_profile';

    protected $fillable = [
        'background_image',
        'preview',
        'title',
        'title_eng',
        'description',
        'description_eng',
        'period',
        'period_eng',
        'link',
        'text',
        'text_eng',
        'specialty_id',
    ];

    public $timestamps = false;

}
