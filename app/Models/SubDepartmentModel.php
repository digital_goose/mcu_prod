<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class SubDepartmentModel extends Model
{
    protected $table = "sub_department";

    protected $fillable = [
        'department_id',
        'title',
        'title_eng',
        'background_image',
        'alert_text',
        'alert_text_eng',
        'alert_link_title',
        'alert_link_title_eng',
        'alert_link',
    ];

    public $timestamps = false;
}
