<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class HonoraryProfessors extends Model
{

    protected $table = "honorary_professors";

    protected $fillable = [
        'photo',
        'full_name',
        'full_name_eng',
        'about_lecturer',
        'about_lecturer_eng',
        'about_lecturer_full',
        'about_lecturer_full_eng'
    ];

    public $timestamps = false;

}
