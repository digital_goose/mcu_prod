<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class GroupsLink extends Model
{

    protected $table = "groups_link";

    protected $fillable = [
        'id',
        'group',
        'page'
    ];

    public $timestamps = false;

}
