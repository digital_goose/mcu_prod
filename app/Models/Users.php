<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Users extends Model
{

    protected $table = 'users';

    protected $fillable = [
        'id',
        'email',
        'password',
        'role_id'
    ];

    static function searchUser($email) {
        return DB::table('users')->where('email', $email)->get()->count();
    }

    static function checkUser($email, $password) {
        $password = sha1($password);

        return DB::table('users')->whereRaw("email = '$email' AND password = '$password'")->get('id');
    }

    static function authenticateUser($email, $password) {
        $id = self::checkUser($email, $password);

        if (!isset($id[0]->id)) {
            return 404;
        }

        $token = self::createToken();
        $role = self::getUserRole($id[0]->id);
        return [$id[0]->id, $token, $role];
    }

    static function createToken() {
        return bin2hex(random_bytes(15));
    }

    static function getUserRole($id) {
        $role = DB::table('users')->where('id', $id)->get();
        return $role[0]->role_id;
    }

    static function registration($email, $password, $roleId) {
        $lastId = DB::table('users')->orderBy('id', 'DESC')->limit(1)->get('id');
        $lastId = $lastId[0]->id;
        $lastId++;
        return DB::table('users')->insert([
            [
                'id' => $lastId,
                'email' => $email,
                'password' => sha1($password),
                'role_id' => $roleId
            ]
        ]);
    }

    static function findUser($userId) {
        $data = DB::table('users')
            ->where('id', $userId)
            ->count();

        return $data > 0;
    }
}
