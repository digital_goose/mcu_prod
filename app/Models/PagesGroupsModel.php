<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PagesGroupsModel extends Model
{

    protected $table = "pages_groups";

    protected $fillable = [
        'id',
        'name'
    ];

    public $timestamps = false;

}
