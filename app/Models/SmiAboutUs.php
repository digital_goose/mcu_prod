<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class SmiAboutUs extends Model
{

    protected $fillable = [
        'title',
        'title_eng',
        'link',
        'date',
        'preview'
    ];

    protected $table = "smi_about_us";

    public $timestamps = false;

}
