<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Templates extends Model
{
    protected $table = 'templates';

    protected $fillable = [
        'id',
        'name',
        'title',
        'fields'
    ];

    public $timestamps = false;
}
