<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Lecturers extends Model
{

    protected $table = 'lecturers';

    protected $fillable = [
        'photo',
        'full_name',
        'full_name_eng',
        'about_lecturer',
        'about_lecturer_eng'
    ];

    public $searchable = [
        'full_name',
        'full_name_eng',
        'about_lecturer',
        'about_lecturer_eng'
    ];

    public $timestamps = false;

}
