<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UsersAuthLog extends Model
{

    protected $table = 'users_auth_log';

    protected $fillable = [
        'id',
        'user_id',
        'token',
        'ip'
    ];

    static function checkEmptyCell($id) {
        return DB::table('users_auth_log')->whereRaw("user_id = '$id' AND token = ''")->get('id');
    }

    static function userAuthorization($id, $token, $ip) {
        $emptyCellId = self::checkEmptyCell($id);

        $userId = DB::table('users_auth_log')->orderBy('id', 'DESC')->limit(1)->get('id');
        $userId = $userId[0]->id;
        $userId++;
        if (isset($emptyCellId[0]->id)) {
            return DB::table('users_auth_log')->where('id', $emptyCellId[0]->id)->update([
                'token' => sha1($token),
                'ip' => $ip
            ]);
        }

        return DB::table('users_auth_log')->insert([
            [
                "id" => $userId,
                'user_id' => $id,
                'token' => sha1($token),
                'ip' => $ip
            ]
        ]);
    }

    static function checkAuthorization($token) {
        $token = sha1($token);
        return DB::table('users_auth_log')->where("token", $token)->count();
    }

    static function getUserId($token) {
        $token = sha1($token);
        return DB::table('users_auth_log')->where("token", $token)->get('user_id');
    }

}
