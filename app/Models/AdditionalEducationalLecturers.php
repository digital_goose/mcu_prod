<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AdditionalEducationalLecturers extends Model
{

    protected $table = 'additional_educational_lecturers';

    protected $fillable = [
        'photo',
        'full_name',
        'full_name_eng',
        'about_lecturer',
        'about_lecturer_eng',
        'about_lecturer_full',
        'about_lecturer_full_eng',
        'program_dpo',
        'program_dpo_eng',
        'direction_id'
    ];

    public $timestamps = false;
}
