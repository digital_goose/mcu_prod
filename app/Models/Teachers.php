<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Teachers extends Model
{

    protected $table = 'teachers';

    protected $fillable = [
        'fullName',
        'fullNameEng',
        'department',
        'departmentEng',
        'faculty',
        'facultyEng',
        'photo',
        'position',
        'positionEng',
        'rank',
        'rankEng',
        'workExperience',
        'workExperienceEng',
        'disciplines',
        'disciplinesEng',
        'email',
        'phone',
        'scientificInterests',
        'scientificInterestsEng',
        'professionalEducation',
        'professionalEducationEng',
        'scienceDirections',
        'scienceDirectionsEng',
        'rewards',
        'rewardsEng',
        'publications',
        'publicationsEng'
    ];

    public $searchable = [
        'fullName',
        'fullNameEng',
        'department',
        'departmentEng',
        'faculty',
        'facultyEng',
        'position',
        'positionEng',
        'rank',
        'rankEng',
        'workExperience',
        'workExperienceEng',
        'disciplines',
        'disciplinesEng',
        'scientificInterests',
        'scientificInterestsEng',
        'professionalEducation',
        'professionalEducationEng',
        'scienceDirections',
        'scienceDirectionsEng',
        'rewards',
        'rewardsEng',
        'publications',
        'publicationsEng'
    ];

    public $timestamps = false;

}
