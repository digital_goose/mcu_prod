<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AdditionalEducation extends Model
{

    protected $table = 'additional_education';

    protected $fillable = [
        'section_id',
        'title',
        'title_eng',
        'background_image',
        'full_time_price',
        'part_time_price',
        'remote_from_price',
        'term',
        'term_part_time',
        'term_remote_from',
        'certificate',
        'certificate_eng',
        'is_remote',
        'is_recruiting',
        'appeal_link',
        'about_program_text',
        'about_program_text_eng',
        'structure_text',
        'structure_text_eng',
        'text_admission',
        'text_admission_eng',
        'text_price',
        'text_price_eng',
        'text_video',
        'text_video_eng',
        'text_reviews',
        'text_reviews_eng',
        'is_visible',
        'certificate_image',
        'order'
    ];

    public $timestamps = false;

}
