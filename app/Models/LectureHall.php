<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class LectureHall extends Model
{
    protected $table = 'lecture_hall';

    protected $fillable = [
        'image',
        'title',
        'title_eng',
        'date_start',
        'date_end',
        'kind',
        'place_name',
        'place_name_eng',
        'place_address',
        'place_address_eng',
        'register_link',
        'price_1',
        'price_description_1',
        'price_description_1_eng',
        'price_2',
        'price_description_2',
        'price_description_2_eng',
        'description',
        'description_eng',
        'format'
    ];

    public $searchable = [
        'title',
        'title_eng',
        'kind',
        'place_name',
        'place_name_eng',
        'place_address',
        'place_address_eng',
        'price_description_1',
        'price_description_1_eng',
        'price_description_2',
        'price_description_2_eng',
        'description',
        'description_eng',
    ];

    public $timestamps = false;

}
