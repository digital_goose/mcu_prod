<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AdditionalEducationSections extends Model
{

    protected $table = 'additional_education_sections';

    protected $fillable = [
        'title',
        'title_eng',
        'order'
    ];

    public $timestamps = false;

}
