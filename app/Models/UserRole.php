<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserRole extends Model
{

    protected $table = 'user_role';

    protected $fillable = [
        'id',
        'name',
        'title'
    ];

    static function getRoleId($roleName) {
        return DB::table('user_role')
            ->where('name', $roleName)
            ->get('id');
    }

    static function findRole($roleId) {
        $data = DB::table('user_role')
            ->where('id', $roleId)
            ->count();

        return $data > 0;
    }

    static function getRoleName($id) {
        $role = DB::table('user_role')
            ->where('id', $id)
            ->get();
        $role = $role[0]->name;
        $response = [
            'is_admin' => false,
            'is_teacher' => false
        ];
        if (mb_strtolower($role) == "admin") $response['is_admin'] = true;
        if (mb_strtolower($role) == "teacher") $response['is_teacher'] = true;

        return $response;
    }

}
