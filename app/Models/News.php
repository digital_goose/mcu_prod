<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = [
        'title',
        'title_eng',
        'preview',
        'content',
        'content_eng',
        'public_date',
        'category',
        'slider',
        'link'
    ];

    public $searchable = [
        'title',
        'title_eng',
        'preview',
        'content',
        'content_eng',
        'public_date',
    ];
}
