<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeacherInformation extends Model
{

    protected $table = 'teacher_information';

    protected $fillable = [
        'id',
        'full_name',
        'position',
        'academic_degree',
        'work_experience',
        'taught_disciplines',
        'email',
        'phone',
        'user_id'
    ];

}
