<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class HigherEducation extends Model
{

    protected $table = 'higher_education';

    protected $fillable = [
        'type',
        'title',
        'title_eng',
        'description',
        'description_eng',
        'code',
        'appeal_link',
        'background_image',
        'preview',
        'content_1',
        'content_1_eng',
        'content_2',
        'content_2_eng',
        'practice_title',
        'practice_title_eng',
        'practice_text',
        'practice_text_eng',
        'full_time_description',
        'full_time_description_eng',
        'full_time_term',
        'full_time_price',
        'full_time_dates',
        'full_time_dates_eng',
        'part_time_description',
        'part_time_description_eng',
        'part_time_term',
        'part_time_price',
        'part_time_dates',
        'part_time_dates_eng',
        'correspondence_description',
        'correspondence_description_eng',
        'correspondence_term',
        'correspondence_price',
        'correspondence_dates',
        'correspondence_dates_eng',
        'day_off_correspondence_description',
        'day_off_correspondence_description_eng',
        'day_off_correspondence_term',
        'day_off_correspondence_price',
        'day_off_correspondence_dates',
        'day_off_correspondence_dates_eng',
        'correspondence_distance_description',
        'correspondence_distance_description_eng',
        'correspondence_distance_term',
        'correspondence_distance_price',
        'correspondence_distance_dates',
        'correspondence_distance_dates_eng',
    ];

    public $timestamps = false;

}
