<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    protected $table = 'questions';

    protected $fillable = [
        'question',
        'question_eng',
        'description',
        'description_eng',
        'answer_1',
        'answer_1_eng',
        'answer_2',
        'answer_2_eng',
        'answer_3',
        'answer_3_eng',
        'answer_4',
        'answer_4_eng'
    ];

    public $timestamps = false;
}
