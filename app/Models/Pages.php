<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{

    protected $table = 'pages';

    protected $fillable = [
        'id',
        'route',
        'title',
        'title_eng',
        'template_id',
        'user_id'
    ];

    public $timestamps = false;

}
