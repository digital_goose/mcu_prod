<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class StaticStubs extends Model
{
    protected $table = 'static_stubs';

    protected $fillable = [
        'id',
        'template',
        'stub'
    ];

    public $timestamps = false;
}
