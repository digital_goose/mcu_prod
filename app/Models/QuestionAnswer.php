<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionAnswer extends Model
{
    protected $table = 'question_answers';

    protected $fillable = [
        'id',
        'question_id',
        'answer_id',
        'fingerprint'
    ];

    public $timestamps = false;
}
