<?php


namespace App\Http\Controllers\Libs;


class MailSendler
{

    private $status = false;

    private $error = null;

    public function __construct(Array $to, String $subject, String $message, String $from = null)
    {

        if (empty($to))
        {
            $this->error = "Param To is empty";
            return;
        }

        if (is_null($from))
        {
            $from = env('MAIL_USERNAME') ?? "mcu@mcumailtest.fvds.ru";
        }

        if ($from == "")
        {
            $this->error = "From mail is not exist";
            return;
        }

        $message = str_replace("\n.", "\n..", $message);

        $headers = [
            'From' => $from,
        ];

        $this->status = mail(implode(",", $to), $subject, $message);

        if (!$this->status) $this->error = "Error post";
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusMessage()
    {
        if (!is_null($this->error)) return $this->error;

        return "Success";
    }
}
