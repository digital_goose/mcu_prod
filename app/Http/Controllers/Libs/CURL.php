<?php


namespace App\Http\Controllers\Libs;


class CURL
{

    public $format = null;

    protected $curl = null;

    protected $response = null;

    protected $status = null;

    protected $headers = null;

    protected $error = null;

    /**
     * CURL constructor.
     * @param String $url
     * @param string $method
     * @param array|null $headers
     * @param array|null $params
     */

    public function __construct(String $url, String $method = "GET", Array $headers = null, Array $params = null)
    {

        $this->curl = curl_init();

        if (!is_null($params) && !empty($params))
        {
            switch (mb_strtoupper($method))
            {
                case 'GET':
                    $url .= '?' . http_build_query($params);
                    break;
                case 'POST':
                    curl_setopt($this->curl, CURLOPT_POSTFIELDS, http_build_query($params));
                    break;
            }
        }

        curl_setopt_array($this->curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HEADER => true
        ));

        if (!is_null($headers) && !empty($headers))
        {
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);
        }

        $this->sendRequest();

    }

    /**
     * Send request
     */

    private function sendRequest()
    {

        $this->response = curl_exec($this->curl);

        if ($this->response === FALSE)
        {

            $this->error = "CURL error: " . (string) curl_error($this->curl);

            $this->response = null;

            return;

        }

        $this->status = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

        $this->headers = substr($this->response, 0, curl_getinfo($this->curl, CURLINFO_HEADER_SIZE));

        $this->response = substr($this->response, curl_getinfo($this->curl, CURLINFO_HEADER_SIZE));

        curl_close($this->curl);

        $this->responseValidator();

    }

    /**
     * Response validator
     */

    private function responseValidator()
    {

        switch ( mb_strtolower($this->format) )
        {
            case 'json':
                    if ( is_string($this->response) ) $this->response = json_decode( $this->response, TRUE );
                break;
        }

    }

    /**
     * @return null|value
     */

    public function getStatusCode()
    {

        return $this->status;

    }

    /**
     * Get response headers
     * @return null|value
     */

    public function getHeaders()
    {

        return $this->headers;

    }

    /**
     * Get response CURL
     * @return null|value
     */

    public function getResponse()
    {

        if (!is_null($this->error))
            return $this->error;

        return $this->response;

    }

}
