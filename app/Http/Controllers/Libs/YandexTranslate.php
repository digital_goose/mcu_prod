<?php


namespace App\Http\Controllers\Libs;

use App\Http\Controllers\Libs\CURL;

class YandexTranslate
{

    private $response = "";

    /**
     * YandexTranslate constructor.
     * @param String $str
     * @param string $lang
     */

    public function __construct(String $str, String $lang = "ru-en")
    {

        $str = str_replace(["\r\n", "\r", "\n"], ' ', $str);

        $v = preg_replace('/<.+?>/', ' -> $0 -> ', $str);
        $v = array_values(array_map('trim', array_diff(explode(' -> ', $v), [''])));

        foreach ($v as $value) {
            if (!preg_match("/<[a-zA-Z=\"\'\\/-\_\d ]*>/", $value) && $value != "\r\n" && $value != "\r" && $value != "\n" && $value != "&nbsp;") {
                $rows = preg_split('/(?<=[.?!;:\-\(\)])\s+(?=[a-zа-я<>\d])/ui', $str);
                foreach ($rows as &$row) {
//                    $row = trim(str_replace('&nbsp;','', $row));
                    $this->response .= $this->translate($row, $lang);
                }
            } else {
                $this->response .= $value;
            }
        }
/*
        return $this->response = $v;

        $rows = preg_split('/(?<=[.?!;:\-\(\)])\s+(?=[a-zа-я<>\d])/ui', $str);
        $this->response = $rows;
        return;
        foreach ($rows as &$row) {
            $row = trim(str_replace('&nbsp;','', $row));
            $this->translate($row, $lang);
        }*/

    }

    /**
     * @param $str
     * @param $lang
     */

    private function translate($str, $lang)
    {

        $url = "https://translate.yandex.net/api/v1/tr.json/translate?id=6f023fbd.5f5027a9.4da08bb3.74722d74657874-0-0&srv=tr-text&lang=$lang&reason=auto&format=text";

        $params = [
            'text' => $str
        ];

        $headers = [
            "Content-Type: application/x-www-form-urlencoded"
        ];

        $CURL = new CURL($url, "POST", $headers, $params);

        $response = $CURL->getResponse();

//        $this->response .= preg_replace('/< (\w*)>/', '<$1>', preg_replace('/< \/ (\w*)>/', '</$1>', $response['text'][0])) ?? "";
        return $response['text'][0] ?? "";
    }

    /**
     * @return string
     */

    public function getResponse() {

        return $this->response;

    }

}
