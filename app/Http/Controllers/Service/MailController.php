<?php


namespace App\Http\Controllers\Service;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Libs\MailSendler;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\FeedbackMail;

class MailController extends Controller
{

    public function mail(Request $request) {

        if (!$request->form) {
            return Response()->json([
                'status' => 'error',
                'message' => 'Param form not valid!'
            ], 400);
        }

        switch (mb_strtolower($request->form)) {

            case 'subscription_to_newsletter':
                    return $this->subscriptionToNewsletter($request);
                break;
            case 'questions_left':
                    return $this->questionsLeft($request);
                break;
        }

    }

    private function subscriptionToNewsletter($request) {
        if ($request->email && filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            $to_email = 'inpsycho.ru@gmail.com';

            $data = array("body" => "Новая заявка на рассылку: $request->email");

            Mail::send('emails', $data, function($message) use ($to_email) {
                $message->to($to_email)->subject('Новая заявка');
                $message->from('inpsycho.ru@gmail.com','inpsycho-api.ru');
            });

            return Response("", 200);
        } else {
            return Response()->json([
                'status' => 'error',
                'message' => 'Param email not valid!'
            ], 400);
        }
    }

    private function questionsLeft($request) {
        if (
            ($request->email && filter_var($request->email, FILTER_VALIDATE_EMAIL)) &&
            $request->phone && $request->full_name && $request->message
        ) {
            $to_email = 'inpsycho.ru@gmail.com';

            $data = array(
                "phone" => "Телефон: $request->phone",
                "fullName" => "ФИО: $request->full_name",
                "mailMessage" => "Сообщение: $request->message",
                "email" => "E-mail: $request->email"
            );

            Mail::send('questions_left', $data, function($message) use ($to_email) {
                $message->to($to_email)->subject('Остались вопросы?');
                $message->from('inpsycho.ru@gmail.com','inpsycho-api.ru');
            });

            return Response("", 200);
        } else {
            return Response()->json([
                'status' => 'error',
                'message' => 'Params not exist!'
            ], 400);
        }
    }

    public static function sendMail(Request $request)
    {
        if (!$request->to) return Response()->json([
            'status' => 'error',
            'message' => "Param to not found"
        ]);

        if (!$request->subject) return Response()->json([
            'status' => 'error',
            'message' => "Param subject not found"
        ]);

        if (!$request->message) return Response()->json([
            'status' => 'error',
            'message' => "Param message not found"
        ]);

        $request->to = array_values(array_diff(explode(',', $request->to), [""]));

        $mail = new MailSendler($request->to, (string) $request->subject, (string) $request->message, "From: inpdedi@inpsycho-api.ru");

        if (!$mail->getStatus()) return Response()->json([
            'status' => 'error',
            'message' => $mail->getStatusMessage()
        ], 400);

        return Response()->json([
            'status' => 'success',
            'message' => $mail->getStatus()
        ], 200);
    }

}
