<?php

namespace App\Http\Controllers\Service\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Service\Admin\ListRepository;

class ListController extends Controller
{

    public function usersList() {
        return response()->json(
            [
                'status' => 'success',
                'data' => ListRepository::usersList()
            ],
            200
        );
    }

    public function teachersList() {
        return response()->json(
            [
                'status' => 'success',
                'data' => ListRepository::teachersList()
            ],
            200
        );
    }

    public function pagesList() {
        return response()->json(
            [
                'status' => 'success',
                'data' => ListRepository::pagesList()
            ],
            200
        );
    }

    public function roleList() {
        return response()->json(
            [
                'status' => 'success',
                'data' => ListRepository::roleList()
            ],
            200
        );
    }

}
