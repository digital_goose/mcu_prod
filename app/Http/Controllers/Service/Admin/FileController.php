<?php


namespace App\Http\Controllers\Service\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FileController extends Controller
{

    public function getList(Request $request) {
        $folder = "files";
        return $this->readFolder($folder);
    }

    public function createFolder(Request $request) {
        $baseFolder = public_path("files");
        $link = $request->link;
        $folderName = $request->folder_name;

        if (file_exists($baseFolder . "/$link")) {
            if (file_exists($baseFolder . "/$link/$folderName")) {
                return Response()->json([
                    "status" => "error",
                    "message" => "This folder exist!"
                ], 400);
            }

            if (mkdir($baseFolder . "/$link/$folderName", 0775, true)) {
                return Response()->json([
                    'status' => "create"
                ], 201);
            } else {
                return Response()->json([
                    "status" => "error",
                    "message" => "Undefined error"
                ], 400);
            }
        } else {
            return Response()->json([
                "status" => "error",
                "message" => "This link not exist!"
            ], 400);
        }
    }

    private function readFolder($folder) {
        $dir = public_path();

        $dirData = array_values(array_diff(scandir($dir . "/$folder"), ['..', '.']));

        $files = [];

        $countFiles = count($dirData);

        if ($countFiles === 0) return [];

        for ($index = 0; $index < $countFiles; $index += 1) {

            if (is_dir("$dir/$folder/$dirData[$index]")) {
                $files[] = [
                    'dir' => $dirData[$index],
                    'last_edit' => filemtime("$dir/$folder/$dirData[$index]"),
                    'files' => $this->readFolder("$folder/$dirData[$index]")
                ];
                continue;
            }
            $shem = parse_url(request()->url(), PHP_URL_SCHEME);
            $address = parse_url(request()->url(), PHP_URL_HOST);
            $port = parse_url(request()->url(), PHP_URL_PORT);

            if (preg_match("/([0-9]{1,3}[\.]){3}[0-9]{1,3}/", $address)) {
                $address .= ":$port";
            }

            $info = pathinfo("$dir/$folder/$dirData[$index]");
            $files[] = [
                'name' => $info['filename'],
                'type' => $info['extension'],
                'src' => $shem . "://" . $address . "/$folder/" . $info['basename'],
                'last_edit' => filemtime("$dir/$folder/$dirData[$index]")
            ];
        }

        return $files;
    }

    public function uploadFiles(Request $request) {

        if (!isset($request->template)) {
            return response()->json('', 404);
        }

        $dir = public_path('files');

        if (!file_exists($dir . "/" . $request->template)) {
            return response()->json('', 404);
        }

        if (empty($_FILES['files']) || !isset($_FILES['files'])) {
            return response()->json('', 404);
        }

        $allowFileType = [
            'jpg', 'JPG', 'png', 'PNG', 'jpeg', 'JPEG', 'docx', 'doc', 'bmp', 'BMP', 'xls', 'xlsx', 'pdf', 'ods', 'ppt', 'pptx',
            'mp4', 'mp3', 'avi', 'wav', 'mov', 'psd', 'ai', 'eps', 'tif', 'svg', 'gif', 'webp'
        ];

        $errors = [];
        $valid = [];

        $countFiles = count($_FILES['files']['name']);

        for ($index = 0; $index < $countFiles; $index += 1) {
            if (!empty($_FILES['files']['error'][$index]) || empty($_FILES['files']['tmp_name'][$index])) {
                switch ($_FILES['files']['error'][$index]) {
                    case 1:
                    case 2:
                        $error = 'Превышен размер загружаемого файла.';
                        break;

                    case 3:
                        $error = 'Файл был получен только частично.';
                        break;

                    case 4:
                        $error = 'Файл не был загружен.';
                        break;

                    case 6:
                        $error = 'Файл не загружен - отсутствует временная директория.';
                        break;

                    case 7:
                        $error = 'Не удалось записать файл на диск.';
                        break;

                    case 8:
                        $error = 'Загрузка файла остановлена системой.';
                        break;

                    case 9:
                        $error = 'Файл не был загружен - директория не существует.';
                        break;

                    case 10:
                        $error = 'Превышен максимально допустимый размер файла.';
                        break;

                    case 11:
                        $error = 'Данный тип файла запрещен.';
                        break;

                    case 12:
                        $error = 'Ошибка при копировании файла.';
                        break;

                    default:
                        $error = 'Файл не был загружен - неизвестная ошибка.';
                        break;
                }
                $errors[] = [
                    'file_name' => $_FILES['files']['name'][$index],
                    'message' => $error
                ];
            }
            elseif ( $_FILES['files']['tmp_name'][$index] == 'none' || !is_uploaded_file($_FILES['files']['tmp_name'][$index]) ) {
                $errors[] = [
                    'file_name' => $_FILES['files']['name'][$index],
                    'message' => 'Не удалось загрузить файл.'
                ];
            }
            else {
                $pattern = "[^a-zа-яё0-9,~!@#%^-_\$\?\(\)\{\}\[\]\. ]";

                $name = mb_eregi_replace($pattern, '-', $_FILES['files']['name'][$index]);

                $name = mb_ereg_replace('[-]+', '-', $name);

                $converter = [
                    'а' => 'a',   'б' => 'b',   'в' => 'v',    'г' => 'g',   'д' => 'd',   'е' => 'e',

                    'ё' => 'e',   'ж' => 'zh',  'з' => 'z',    'и' => 'i',   'й' => 'y',   'к' => 'k',

                    'л' => 'l',   'м' => 'm',   'н' => 'n',    'о' => 'o',   'п' => 'p',   'р' => 'r',

                    'с' => 's',   'т' => 't',   'у' => 'u',    'ф' => 'f',   'х' => 'h',   'ц' => 'c',

                    'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',  'ь' => 'ti',    'ы' => 'y',   'ъ' => '',

                    'э' => 'e',   'ю' => 'yu',  'я' => 'ya',



                    'А' => 'A',   'Б' => 'B',   'В' => 'V',    'Г' => 'G',   'Д' => 'D',   'Е' => 'E',

                    'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',    'И' => 'I',   'Й' => 'Y',   'К' => 'K',

                    'Л' => 'L',   'М' => 'M',   'Н' => 'N',    'О' => 'O',   'П' => 'P',   'Р' => 'R',

                    'С' => 'S',   'Т' => 'T',   'У' => 'U',    'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',

                    'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',  'Ь' => 'TI',    'Ы' => 'Y',   'Ъ' => '',

                    'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
                ];

                $name = strtr($name, $converter);

                $parts = pathinfo($name);

                if (empty($name) || empty($parts['extension'])) {
                    $errors[] = [
                        'file_name' => $_FILES['files']['name'][$index],
                        'massage' => 'Недопустимый тип файла!'
                    ];
                } else if (!empty($allowFileType) && !in_array(strtolower($parts['extension']), $allowFileType)) {
                    $errors[] = [
                        'file_name' => $_FILES['files']['name'][$index],
                        'massage' => 'Недопустимый тип файла!'
                    ];
                } else {
                    $name = $parts['filename'] . '.' . $parts['extension'];
                    $name = str_replace(" ", "_", $name);
                    $number = "";

                    $maxRequr = 10;
                    $i = 0;
                    while (file_exists($dir . "/" . $request->template . "/" . $name . $number)) {
                        $name = $parts['filename'] . mt_rand(1, 100) . '.' . $parts['extension'];

                        if ($i == $maxRequr) break;

                        $i += 1;
                    }
                    if (file_exists($dir . "/" . $request->template . "/" . $name)) {
                        $name .= mt_rand();
                    }

                    if (move_uploaded_file($_FILES['files']['tmp_name'][$index], $dir . "/" . $request->template . "/" . $name)) {
                        $valid[] = $name;
                    } else {
                        $errors[] = [
                            'file_name' => $_FILES['files']['name'][$index],
                            'message' => 'Не удалось сохранить файл!'
                        ];
                    }
                }
            }
        }

        return response()->json([
            "valid" => $valid,
            "bad" => $errors
        ], 200);

    }

    public function removeFile(Request $request) {

        if (!isset($request->src)) return response()->json('', 404);

        $dir = public_path('files');

        if (!file_exists($dir . "/" . $request->src))  return response()->json('', 404);

        if (!is_file($dir . "/" . $request->src)) return response()->json('', 404);

        if (unlink($dir . "/" . $request->src))
            return response()->json("", 200);
        else
            return response()->json("", 404);

    }

    public function removeDir(Request $request) {
        if (!isset($request->src)) return response()->json('', 404);

        $dir = public_path('files');

        if (!file_exists($dir . "/" . $request->src))  return response()->json('', 404);

        if ($this->delDir($dir . "/" . $request->src)) {
            return Response()->json([
                'status' => 'success'
            ], 200);
        } else {
            return Response()->json([
                'status' => 'error'
            ], 500);
        }
    }

    private function delDir($dir) {
        $files = array_diff(scandir($dir), ['.','..']);
        foreach ($files as $file) {
            (is_dir($dir.'/'.$file)) ? $this->delDir($dir.'/'.$file) : unlink($dir.'/'.$file);
        }
        return rmdir($dir);
    }
}
