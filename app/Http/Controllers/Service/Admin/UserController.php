<?php


namespace App\Http\Controllers\Service\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Service\Admin\UserRepository;

class UserController extends Controller
{

    public function changeUserRole(Request $request) {

        if ( !isset($request->user_id) || empty($request->user_id) ) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Param user_id not found!'
                ],
                400
            );
        }

        if ( !isset($request->role_id) || empty($request->role_id) ) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Param role_id not found!'
                ],
                400
            );
        }

        if ( !preg_match('/^\d*+$/ui', $request->role_id) || !preg_match('/^\d*+$/ui', $request->user_id) ) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Param role_id and user_id must be integer!'
                ],
                400
            );
        }

        return UserRepository::changeUserRole((int)$request->user_id, (int)$request->role_id);

    }

    public static function removeUser(Request $request) {

        if ( !isset($request->user_id) || empty($request->user_id) ) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Param user_id not found!'
                ],
                400
            );
        }

        if ( !preg_match('/^\d*+$/ui', $request->user_id) ) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Param user_id must be integer!'
                ],
                400
            );
        }

        return UserRepository::removeUser((int)$request->user_id);

    }

}
