<?php


namespace App\Http\Controllers\Service\Admin;


use App\Http\Controllers\Controller;
use App\Models\GroupsLink;
use App\Models\Pages;
use App\Models\PagesGroupsModel;
use App\Models\Templates;
use Illuminate\Http\Request;

class GroupController extends Controller
{

    private $groupModel;
    private $groupLinksModel;
    private $pagesModel;
    private $templatesModel;

    public function __construct(GroupsLink $groupLinksModel, PagesGroupsModel $pagesGroupsModel, Pages $pages, Templates $templates)
    {

        $this->groupLinksModel = new $groupLinksModel;
        $this->groupModel = new $pagesGroupsModel;
        $this->pagesModel = new $pages;
        $this->templatesModel = new $templates;

    }

    public function createGroup(Request $request) {
        $request = (array) $request->all();
        if (!isset($request['title']) || mb_strlen($request['title']) == 0) {
            return Response()->json([
                'status' => 'error',
                'message' => 'Param title not exist'
            ], 400);
        }

        try {
            return $this->groupModel->create(['name' => $request['title']]);
        } catch (\Exception $e) {
            return Response()->json([
                'status' => 'error',
                'message' => 'Непредвиденная ошибка'
            ], 500);
        }

    }

    public function getGroupList() {
        $groups = $this->groupModel->orderBy('id', 'ASC')->get([
            'id',
            'name as title'
        ])->toArray();

        foreach ($groups as &$group) {
            $group['links'] = [];
        }

        $templateId = $this->templatesModel->where('name', '=', 'ContentPage')->get('id');
        $templateId = $templateId[0]->id;

        $pagesInGroup = $this->pagesModel
            ->where('template_id', '=', $templateId)
            ->leftJoin('groups_link', 'groups_link.page', '=', 'pages.id')
            ->get([
                'pages.id',
                'groups_link.group',
                'route',
                'title',
                'title_eng'
            ])->toArray();

        $groupsId = array_column($groups, 'id');

        $pages = [];

        foreach ($pagesInGroup as $page) {
            if (is_null($page['group'])) {
                $pages[] = $page;
                continue;
            }

            $id = array_search($page['group'], $groupsId);

            if ($id !== FALSE) {
                $groups[$id]['links'][] = $page;
            }

        }

        return [
            'groups' => $groups,
            'pages' => $pages
        ];
    }

    public function deleteGroup(Request $request) {
        $request = (array) $request->all();
        if (!isset($request['group_id']) || mb_strlen($request['group_id']) == 0 || !is_numeric($request['group_id'])) {
            return Response()->json([
                'status' => 'error',
                'message' => 'Param Group id not exist'
            ], 400);
        }

        try {
            $count = $this->groupModel->where('id', '=', $request['group_id'])->count();
            if ($count == 0) {
                return Response()->json([
                    'status' => 'error',
                    'message' => 'This group not found'
                ], 400);
            }

            $this->groupModel->where('id', '=', $request['group_id'])->delete();
            $this->groupLinksModel->where('group', '=', $request['group_id'])->delete();
            return Response()->json([
                'status' => 'success',
                'message' => 'This group deleted'
            ], 200);
        } catch (\Exception $e) {
            return Response()->json([
                'status' => 'error',
                'message' => 'Непредвиденная ошибка'
            ], 500);
        }
    }

    public function addToGroup(Request $request) {
        $request = (array) $request->all();
        if (!isset($request['group_id']) || mb_strlen($request['group_id']) == 0 || !is_numeric($request['group_id'])) {
            return Response()->json([
                'status' => 'error',
                'message' => 'Param Group id not exist'
            ], 400);
        }

        if (!isset($request['page_id']) || mb_strlen($request['page_id']) == 0 || !is_numeric($request['page_id'])) {
            return Response()->json([
                'status' => 'error',
                'message' => 'Param Page Id not exist'
            ], 400);
        }

        try {
            $templateId = $this->templatesModel->where('name', '=', 'ContentPage')->get('id');
            $templateId = $templateId[0]->id;

            $count = $this->groupModel->where('id', '=', $request['group_id'])->count();
            if ($count == 0) {
                return Response()->json([
                    'status' => 'error',
                    'message' => 'This group not found'
                ], 400);
            }

            $count = $this->pagesModel->where('id', '=', $request['page_id'])->where('template_id', '=', $templateId)->count();

            if ($count == 0) {
                return Response()->json([
                    'status' => 'error',
                    'message' => 'This page is not a content page'
                ], 400);
            }

            $count = $this->groupLinksModel->where('page', '=', $request['page_id'])->count();
            if ($count > 0) {
                return Response()->json([
                    'status' => 'error',
                    'message' => 'This page isset in group'
                ], 400);
            }

            return $this->groupLinksModel->create(['group' => $request['group_id'], 'page' => $request['page_id']]);
        } catch (\Exception $e) {
            return Response()->json([
                'status' => 'error',
                'message' => 'Непредвиденная ошибка'
            ], 500);
        }
    }

    public function removePageOnGroup(Request $request) {
        $request = (array) $request->all();
        if (!isset($request['group_id']) || mb_strlen($request['group_id']) == 0 || !is_numeric($request['group_id'])) {
            return Response()->json([
                'status' => 'error',
                'message' => 'Param Group id not exist'
            ], 400);
        }

        if (!isset($request['page_id']) || mb_strlen($request['page_id']) == 0 || !is_numeric($request['page_id'])) {
            return Response()->json([
                'status' => 'error',
                'message' => 'Param Page Id not exist'
            ], 400);
        }

        try {
            $templateId = $this->templatesModel->where('name', '=', 'ContentPage')->get('id');
            $templateId = $templateId[0]->id;

            $count = $this->groupModel->where('id', '=', $request['group_id'])->count();
            if ($count == 0) {
                return Response()->json([
                    'status' => 'error',
                    'message' => 'This group not found'
                ], 400);
            }

            $count = $this->pagesModel->where('id', '=', $request['page_id'])->where('template_id', '=', $templateId)->count();

            if ($count == 0) {
                return Response()->json([
                    'status' => 'error',
                    'message' => 'This page is not a content page'
                ], 400);
            }

            $count = $this->groupLinksModel->where('page', '=', $request['page_id'])->where('group', '=', $request['group_id'])->count();
            if ($count == 0) {
                return Response()->json([
                    'status' => 'error',
                    'message' => 'This page not isset in this group'
                ], 400);
            }

            $this->groupLinksModel->where('page', '=', $request['page_id'])->where('group', '=', $request['group_id'])->delete();
            return Response()->json([
                'status' => 'success',
                'message' => 'This page delete'
            ], 200);
        } catch (\Exception $e) {
            return Response()->json([
                'status' => 'error',
                'message' => 'Непредвиденная ошибка'
            ], 500);
        }
    }

}
