<?php


namespace App\Http\Controllers\Service;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Libs\YandexTranslate;
use App\Models\PageDataset;
use App\Models\Pages;
use App\Repositories\Service\ContentRepository;
use Illuminate\Http\Request;


class ContentController extends Controller
{

    private $baseRepository = "App\Repositories\Service\ContentRepository";
    private $baseModelPath  = "App\Models\\";
    private $repositoryList = ['templates', 'news', 'questionnaire', 'guest_lecturer', 'lecture_hall', 'lecturers', 'index', 'archive_activity', 'higher_education', 'higher_education_profile', 'additional_education_sections', 'additional_education', 'faculty', 'cathedra', 'smi_about_us', 'additional_educational_lecturers', 'honorary_professors', 'department', 'sub_department'];
    private $models         = [
                                'News' => 'news',
                                'Templates' => 'templates',
                                'Questions' => 'questionnaire',
                                'Lecturers' => 'guest_lecturer',
                                'LectureHall' => 'lecture_hall',
                                'Teachers' => 'lecturers',
                                'IndexPages' => 'index',
                                'ArchiveActivity' => 'archive_activity',
                                'HigherEducation' => 'higher_education',
                                'HigherEducationProfile' => 'higher_education_profile',
                                'AdditionalEducationSections' => 'additional_education_sections',
                                'AdditionalEducation' => 'additional_education',
                                'Faculty' => 'faculty',
                                'Cathedra' => 'cathedra',
                                'SmiAboutUs' => 'smi_about_us',
                                'AdditionalEducationalLecturers' => 'additional_educational_lecturers',
                                'HonoraryProfessors' => 'honorary_professors',
                                'DepartmentModel' => 'department',
                                'SubDepartmentModel' => 'sub_department'
                              ];


    /**
     * @param Request $request
     * @param String $route
     */
    public function index(Request $request)
    {
        if (!$request->route) return Response('', 404);

        $route = $request->route;

        if ($route === "/") $route = "/index";

        $route = $this->parseRoute($route);

        if (array_search($route[0], $this->repositoryList) !== FALSE)
        {

            //Init repository
            $repository = new $this->baseRepository;

            //Finding model in list of model
            $modelName = array_search($route[0], $this->models);

            //If model does not exists then returning 404 code
            if ($modelName === FALSE) return Response('', 404);

            if (count($route) > 1)
            {
		$tmp = $route;
		unset($tmp[0]);
                $tmp = implode("", $tmp);

                if (!preg_match("/^\d{1,}+$/", $tmp))
                {
                    return Response('', 404);
                }

                $result = $repository->findById($request, (int) $tmp, $modelName);

            } else {

                $result = $repository->getAll($request, $modelName);

            }

        } else {

            //Searching page route in Pages table
            $page = Pages::where('route', '/'.$route[0])->firstOrFail();

            //Getting dataset page on page id
            $dataset = PageDataset::where('page_id', $page->id)->firstOrFail('dataset');

            if (empty($dataset)) return Response('', 404);

            $result = json_decode($dataset->dataset, true);

            if ($route[0] == "student_life") {

                for ($i = 0; $i < count($result['sections']['cards']['data']); $i++) {

                    $result['sections']['cards']['data'][$i]['id'] = $i;

                }

                $month = [
                    'январь' => '01',
                    'января' => '01',
                    'февраль' => '02',
                    'февраля' => '02',
                    'март' => '03',
                    'марта' => '03',
                    'апрель' => '04',
                    'апреля' => '04',
                    'май' => '05',
                    'мая' => '05',
                    'июнь' => '06',
                    'июня' => '06',
                    'июль' => '07',
                    'июля' => '07',
                    'август' => '08',
                    'августа' => '08',
                    'сентябрь' => '09',
                    'сентября' => '09',
                    'октябрь' => 10,
                    'октября' => 10,
                    'ноябрь' => 11,
                    'ноября' => 11,
                    'декабрь' => 12,
                    'декабря' => 12
                ];

                $count_elements = count($result['sections']['cards']['data']);
                $iterations = $count_elements - 1;

                for ($i=0; $i < $count_elements; $i++) {
                    $changes = false;
                    for ($j=0; $j < $iterations; $j++) {

                        $date = array_diff(explode(' ', $result['sections']['cards']['data'][$j]['period']), ['']);

                        if (count($date) > 1) {
                            foreach($date as &$item) {
                                if (!is_int($item)) {
                                    if (array_key_exists(mb_strtolower($item), $month)) {
                                        $item = $month[mb_strtolower($item)];
                                    }
                                }
                            }
                        }

                        $add = false;
                        if (count($date) == 2) {
                            $add = true;
                        }

                        $result['sections']['cards']['data'][$j]['period'] = implode('.', $date);

                        if ($add) {
                            $result['sections']['cards']['data'][$j]['period'] = "01." . $result['sections']['cards']['data'][$j]['period'];
                        }

                        $date = array_diff(explode(' ', $result['sections']['cards']['data'][($j + 1)]['period']), ['']);

                        if (count($date) > 1) {
                            foreach($date as &$item) {
                                if (!is_int($item)) {
                                    if (array_key_exists(mb_strtolower($item), $month)) {
                                        $item = $month[mb_strtolower($item)];
                                    }
                                }
                            }
                        }

                        $add = false;
                        if (count($date) == 2) {
                            $add = true;
                        }

                        $result['sections']['cards']['data'][($j + 1)]['period'] = implode('.', $date);

                        if ($add) {
                            $result['sections']['cards']['data'][($j + 1)]['period'] = "01." . $result['sections']['cards']['data'][($j + 1)]['period'];
                        }

                        if (strtotime($result['sections']['cards']['data'][$j]['period']) < strtotime($result['sections']['cards']['data'][($j + 1)]['period'])) {
                            $changes = true;
                            list($result['sections']['cards']['data'][$j], $result['sections']['cards']['data'][($j + 1)]) = array($result['sections']['cards']['data'][($j + 1)], $result['sections']['cards']['data'][$j]);

                        }
                    }
                    $iterations--;
                    if (!$changes) {
                        break;
                    }
                }

                //$result['sections']['cards']['data'] = $this->SortByKeyValue($result['sections']['cards']['data'], 'period');

            }

            if ($route[0] == "main_achievements_awards") {
                for ($i = 0; $i < count($result['sections']['cards']['data']); $i++) {
                    $result['sections']['cards']['data'][$i]['id'] = $i;
                }

                if (isset($request->year)) {
                    $data = [];
                    $years = array_values(array_diff(explode(";", $request->year), ['']));
                    for ($i = 0; $i < count($result['sections']['cards']['data']); $i++) {
                        foreach ($years as $year) {
                            if ((int)date('Y', strtotime($result['sections']['cards']['data'][$i]['date_published'])) == (int)$year) {
                                $data[] = $result['sections']['cards']['data'][$i];
                            }
                        }
                    }
                    $result['sections']['cards']['data'] = $data;
                }

                $count_elements = count($result['sections']['cards']['data']);
                $iterations = $count_elements - 1;

                for ($i=0; $i < $count_elements; $i++) {
                    $changes = false;
                    for ($j=0; $j < $iterations; $j++) {
                        if (strtotime($result['sections']['cards']['data'][$j]['date_published']) < strtotime($result['sections']['cards']['data'][($j + 1)]['date_published'])) {
                            $changes = true;
                            list($result['sections']['cards']['data'][$j], $result['sections']['cards']['data'][($j + 1)]) = array($result['sections']['cards']['data'][($j + 1)], $result['sections']['cards']['data'][$j]);
                        }
                    }
                    $iterations--;
                    if (!$changes) {
                        break;
                    }
                }
            }

            if ($route[0] == "") {

            }

        }

        if (empty($result)) return Response('', 404);

        return Response()->json($result,200);
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {

        if (!$request->route) return Response('', 404);

        $route = $request->route;

        if ($route === "/") $route = "/index";

        $route = $this->parseRoute($route);

        if (array_search($route[0], $this->repositoryList) !== FALSE)
        {

            //Init repository
            $repository = new $this->baseRepository;

            //Finding model in list of model
            $modelName = array_search($route[0], $this->models);

            //If model does not exists then returning 404 code
            if ($modelName === FALSE) return Response('', 404);

            $result = $repository->create($request, $modelName);

        } else {

            try {

                if (!isset($request->section)) return Response('', 406);

                if (!isset($request->data)) return Response('', 400);

                $pages = new Pages();

                $page = $pages
                        ->where('route', '/'.$route[0])
                        ->firstOrFail();

                $pagesDataset = new PageDataset();
                $dataset = $pagesDataset
                            ->where('page_id', $page->id)
                            ->firstOrFail('dataset');

                try {

                    $dataset = json_decode($dataset->dataset, true);

                    if (is_string($request->data)) $request->data = json_decode($request->data, true);

                    $data = $request->data;

                    /*foreach ($data as $key => &$value)
                    {
                        if ((mb_stristr($key, "_eng") !== FALSE || mb_stristr($key, "Eng") !== FALSE) && !is_null($value))
                        {

                            $yandexTranslate = new YandexTranslate($value);
                            $value = $yandexTranslate->getResponse();

                        }
                    }*/

                    $dataset['sections'][$request->section]['data'][] = $data;

                    $pagesDataset
                        ->where('page_id', $page->id)
                        ->update(['dataset' => json_encode($dataset)]);

                    $result = $dataset;

                } catch (\Exception $e) {

                    return Response('', 400);

                }

            } catch (\Exception $e) {
                return Response('', 404);
            }

        }

        if (empty($result)) return Response('', 404);

        return Response()->json($result, 200);

    }

    /**
     * @param Request $request
     */
    public function update(Request $request)
    {

        if (!$request->route) return Response('', 404);

        $route = $request->route;

        if ($route === "/") $route = "/index";

        $route = $this->parseRoute($route);

        if (array_search($route[0], $this->repositoryList) !== FALSE)
        {

            //Init repository
            $repository = new $this->baseRepository;

            //Finding model in list of model
            $modelName = array_search($route[0], $this->models);

            //If model does not exists then returning 404 code
            if ($modelName === FALSE) return Response('', 404);

            $result = $repository->update($request, $modelName);

        } else {

            try {

                if (!isset($request->section)) return Response('', 406);

                if (!isset($request->data)) return Response('', 400);

                $pages = new Pages();

                $page = $pages
                    ->where('route', '/'.$route[0])
                    ->firstOrFail();

                $pagesDataset = new PageDataset();
                $dataset = $pagesDataset
                    ->where('page_id', $page->id)
                    ->firstOrFail('dataset');

                try {

                    $dataset = json_decode($dataset->dataset, true);

                    if (is_string($request->data)) $request->data = json_decode($request->data, true);

                    $data = $request->data;

                    /*foreach ($data as $key => &$value)
                    {
                        if ((mb_stristr($key, "_eng") !== FALSE || mb_stristr($key, "Eng") !== FALSE) && !is_null($value))
                        {

                            $yandexTranslate = new YandexTranslate($value);
                            $value = $yandexTranslate->getResponse();

                        }
                    }*/

                    $dataset['sections'][$request->section]['data'][$request->index] = $data;

                    $pagesDataset
                        ->where('page_id', $page->id)
                        ->update(['dataset' => json_encode($dataset)]);

                    $result = $dataset;

                } catch (\Exception $e) {

                    return Response('', 400);

                }

            } catch (\Exception $e) {
                return Response('', 404);
            }

        }

        if (empty($result)) return Response('', 400);

        return Response()->json($result, 200);

    }

    /**
     * @param Request $request
     */
    public function delete(Request $request)
    {

        if (!$request->route) return Response('', 404);

        $route = $request->route;

        if ($route === "/") $route = "/index";

        $route = $this->parseRoute($route);

        if (array_search($route[0], $this->repositoryList) !== FALSE)
        {

            //Init repository
            $repository = new $this->baseRepository;

            //Finding model in list of model
            $modelName = array_search($route[0], $this->models);

            //If model does not exists then returning 404 code
            if ($modelName === FALSE) return Response('', 404);

            if ($status = $repository->delete($request, $modelName)) {
                if (is_bool($status)) return Response('', 200);
                $result = $status;
            } else {
                return Response('', 400);
            }

        } else {

            try {

                if (!isset($request->section)) return Response('', 406);

                $pages = new Pages();

                $page = $pages
                    ->where('route', '/'.$route[0])
                    ->firstOrFail();

                $pagesDataset = new PageDataset();
                $dataset = $pagesDataset
                    ->where('page_id', $page->id)
                    ->firstOrFail('dataset');

                try {

                    $dataset = json_decode($dataset->dataset, true);

                    if (is_string($request->data)) $request->data = json_decode($request->data, true);

                    unset($dataset['sections'][$request->section]['data'][$request->index]);

                    $dataset['sections'][$request->section]['data'] = array_values($dataset['sections'][$request->section]['data']);

                    $pagesDataset
                        ->where('page_id', $page->id)
                        ->update(['dataset' => json_encode($dataset)]);

                    $result = $dataset;

                } catch (\Exception $e) {

                    return Response('', 400);

                }

            } catch (\Exception $e) {
                return Response('', 404);
            }

        }

        if (empty($result)) return Response('', 400);

        return Response()->json($result, 200);

    }

    /**
     * @param String $route
     * @return array
     */
    private function parseRoute(String $route) {
        return array_values(array_diff(explode('/', $route), [""]));
    }

    public function search(Request $request)
    {

        if (!$request->text) return Response('', 400);

        $filter = null;

        if ($request->filter) $filter = $request->filter;

        return ContentRepository::search($request->text, $filter);

    }

    private function SortByKeyValue($data, $sortKey, $sort_flags=SORT_DESC)
    {
        if (empty($data) or empty($sortKey)) return $data;

        $ordered = array();
        foreach ($data as $key => $value)
            $ordered[$value[$sortKey]] = $value;

        ksort($ordered, $sort_flags);

        return array_values($ordered); // array_values() added for identical result with multisort*
    }

}
