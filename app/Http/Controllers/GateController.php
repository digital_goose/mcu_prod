<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class GateController extends Controller {

    public function index(string $slug = null) {
//return ['da'];
        $params = [
            'route' => $slug ?? '/'
        ];

        $defaults = array(
            CURLOPT_URL => "https://inpsycho.ru/api/get-content",
            CURLOPT_POST => true,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_POSTREDIR => 3,
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
	    CURLOPT_AUTOREFERER => true,
    	    CURLOPT_MAXREDIRS => 15,
            CURLOPT_SSL_VERIFYPEER => false
        );

        $ch = curl_init();

        curl_setopt_array($ch, $defaults);

        //        $content = json_decode(curl_exec($ch), true);
        $content = curl_exec($ch);

        if ($content === false) {
            var_dump(curl_error($ch));
            curl_close($ch);
        } else {
            $content = json_decode($content, true);

            $description = 'Московский институт психоанализа';

            if ($content && $content['sections']) {
                $description = '';

                /*foreach ($section['data'] as $data) {
                    foreach ($data as $key => $value) {
                        if (strripos($value, 'http') === false) {
                            $description .= $value;
                        }
                    }
                }*/

                foreach ($content['sections'] as $section) {
                    if (array_key_exists('0', $section['data'])) {
                        foreach ($section['data'] as $data) {
                            foreach ($data as $key => $value) {
                                if (strripos($value, 'http') === false && strpos(mb_strtolower($key), 'eng') === false) {
                                    $description .= $value;
                                }
                            }
                        }
                    } else {
                        foreach ($section['data'] as $key => $value) {
                            if (strripos($value, 'http') === false && strpos(mb_strtolower($key), 'eng') === false) {
                                $description .= $value;
                            }
                        }
                    }
                }

                $description = strip_tags($description);
                $description = mb_substr($description, 0, 255, 'UTF-8');
            }

            $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$meta = [];
            if ($http_status != 200) {
		return view('default', compact('content', 'meta'));
            } else {

            $meta = [
                [
                    'name' => 'title',
                    'content' => $content['title'] ?? 'Московский институт психоанализа'
                ],
                [
                    'property' => 'og:title',
                    'content' => $content['title'] ?? 'Московский институт психоанализа'
                ],
                [
                    'name' => 'description',
                    'content' => $description
                ],
                [
                    'property' => 'og:description',
                    'content' => $description
                ],
                [
                    'property' => 'og:url',
                    'content' => URL::current()
                ],
            ];
            }
            curl_close($ch);
            return view('default', compact('content', 'meta'));
        }
    }

    public function admin() {
        return view('admin');
    }

}
