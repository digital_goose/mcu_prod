<?php


namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Auth\AuthRepository;

class AuthController extends Controller
{
    public function login(Request $request) {

        if (!isset($request->user_email) || !isset($request->user_password)) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Bad request! Email or password does not exists!'
                ],
                400
            );
        }

        if (!preg_match('/[a-zA-Z\d_\-.]{1,}@[a-zA-Z\d_\-.]{1,}\.[a-zA-Z\d_\-.]{1,}/ui', $request->user_email)) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Bad request! Email have does not valid symbols!'
                ],
                400
            );
        }

        if (!preg_match('/[a-zA-Z\d@$#.,_!%^&*()\-=+]{8,}$/ui', $request->user_password)) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Bad request! Password have does not valid symbols or less than 8 characters!'
                ],
                400
            );
        }

        return AuthRepository::login($request->user_email, $request->user_password, $request->ip());
    }

    public function registration(Request $request) {
        if (!isset($request->user_email) || !isset($request->user_password)) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Bad request! Email or password does not exists!'
                ],
                400
            );
        }

        if (!preg_match('/[a-zA-Z\d_\-.]{1,}@[a-zA-Z\d_\-.]{1,}\.[a-zA-Z\d_\-.]{1,}/ui', $request->user_email)) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Bad request! Email have does not valid symbols!'
                ],
                400
            );
        }

        if (!preg_match('/[a-zA-Z\d@$#.,_!%^&*()\-=+]{8,}$/ui', $request->user_password)) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Bad request! Password have does not valid symbols or less than 8 characters!'
                ],
                400
            );
        }

        return AuthRepository::registration($request->user_email, $request->user_password, $request->ip());
    }
}
