<?php

namespace App\Http\Controllers;

use App\Models\PageDataset;
use App\Models\Pages;
use App\Repositories\BasicRepository;
use App\Repositories\Service\Pages\NewsRepository;
use Illuminate\Http\Request;

class ContentController extends Controller
{

    private $repositoryPath = 'App\Repositories\Service\Pages\NewsRepository';
    private $modelPath = 'App\Models\\';

    private $staticRepositories = [
        'templates',
        'news',
        'questionnaire',
        'lecturers',
        'lecture_hall'
    ];

    private $staticModels = [
        'News' => 'news',
        'Templates' => 'templates',
        'Questions' => 'questionnaire',
        'Lecturers' => 'lecturers',
        'LectureHall' => 'lecture_hall'
    ];

    public function index(Request $request)
    {
        if (!$request->route) {
            return Response('', 404);
        }

        $route = $request->route;

        if ($route === "/") $route = "/news";

        $route = $this->readRoute($route);

        $repositoryIsset = array_search($route[0], $this->staticRepositories);

        if (is_integer($repositoryIsset)) {
            $repository = new $this->repositoryPath;
            $model = array_search($route[0], $this->staticModels);

            if (count($route) === 1) {
                $result = $repository->getAll($request, $model);
            }

            if (count($route) > 1) {
                if (!preg_match("/^\d{1,}+$/", $route[1])) {
                    return Response('', 404);
                }

                $result = $repository->findById($request, (int) $route[1], $model);

                if (count($result) === 0) return Response('', 404);
            }
        } else {
            try {
                $page = Pages::where('route', '/'.$route[0])->firstOrFail();
                $dataset = PageDataset::where('page_id', $page->id)->get('dataset');

                if (empty($dataset)) {
                    return [];
                }

                $dataset = json_decode($dataset[0]->dataset, true);

                return $dataset;
            } catch (\Exception $e) {
                return Response('', 404);
            }
        }

        if (empty($result)) return Response('', 404);

        return Response()->json($result,200);
    }

    public function create(Request $request) {
        if (!$request->route) {
            return Response('', 404);
        }

        $route = $request->route;

        if ($route === "/") $route = "/index";

        $route = $this->readRoute($route);

        $repositoryIsset = array_search($route[0], $this->staticRepositories);

        if (is_integer($repositoryIsset)) {
            $repository = new $this->repositoryPath;
            $model = array_search($route[0], $this->staticModels);

            $result = $repository->store($request, "$this->modelPath$model");
            return [$result];
            if (count($result) === 0) return Response('', 404);
        } else {
            try {
                $page = Pages::where('route', '/'.$route[0])->firstOrFail();
                $dataset = PageDataset::where('page_id', $page->id)->get('dataset');

                if (!isset($request->section)) return Response('', 406);

                if (!isset($request->data)) return [];

                if (empty($dataset)) {
                    return [];
                }

                $dataset = json_decode($dataset[0]->dataset, true);

                if (is_string($request->data)) $request->data = json_decode($request->data, true);

                if (!isset($dataset['sections'][$request->section]['data'])) return Response('', 404);

                $dataset['sections'][$request->section]['data'][] = $request->data;

                PageDataset::where('page_id', $page->id)->update(['dataset' => json_encode($dataset)]);

                return $dataset;
            } catch (\Exception $e) {
                return Response('', 404);
            }
        }

        return Response('',201);
    }

    public function update(Request $request) {
        if (!$request->route) {
            return Response('', 404);
        }

        $route = $request->route;

        if ($route === "/") $route = "/index";

        $route = $this->readRoute($route);

        $repositoryIsset = array_search($route[0], $this->staticRepositories);

        if (is_integer($repositoryIsset)) {
            $repository = new $this->repositoryPath;

            $model = array_search($route[0], $this->staticModels);

            $result = $repository->update($request, "$this->modelPath$model");

            if (!$result) return Response('', 404);

            return [$result];
            if (count($result) === 0) return Response('', 404);
        } else {
            try {
                $page = Pages::where('route', '/'.$route[0])->firstOrFail();
                $dataset = PageDataset::where('page_id', $page->id)->get('dataset');

                if (!isset($request->section)) return Response('', 406);

                if (!isset($request->data)) return [];

                if (empty($dataset)) {
                    return [];
                }

                $dataset = json_decode($dataset[0]->dataset, true);

                if (is_string($request->data)) $request->data = json_decode($request->data, true);

                if (!isset($dataset['sections'][$request->section]['data'])) return Response('', 404);
                if (!isset($dataset['sections'][$request->section]['data'][$request->index])) return Response('', 404);

                $dataset['sections'][$request->section]['data'][$request->index] = $request->data;

                PageDataset::where('page_id', $page->id)->update(['dataset' => json_encode($dataset)]);

                return $dataset;
            } catch (\Exception $e) {
                return Response('', 404);
            }
        }

        return Response('',201);
    }

    public function delete(Request $request) {
        if (!$request->route) {
            return Response('', 404);
        }

        $route = $request->route;

        if ($route === "/") $route = "/index";

        $route = $this->readRoute($route);

        $repositoryIsset = array_search($route[0], $this->staticRepositories);

        if (is_integer($repositoryIsset)) {
            $repository = new $this->repositoryPath;

            $model = array_search($route[0], $this->staticModels);

            $result = $repository->trash($request, "$this->modelPath$model");

            if (!$result) return Response('', 404);

            return Response('', 200);
        } else {
            try {
                $page = Pages::where('route', '/'.$route[0])->firstOrFail();
                $dataset = PageDataset::where('page_id', $page->id)->get('dataset');

                if (!isset($request->section)) return Response('', 406);

                if (empty($dataset)) {
                    return [];
                }

                $dataset = json_decode($dataset[0]->dataset, true);

                if (is_string($request->data)) $request->data = json_decode($request->data, true);

                if (!isset($dataset['sections'][$request->section]['data'])) return Response('', 404);
                if (!isset($dataset['sections'][$request->section]['data'][$request->index])) return Response('', 404);

                unset($dataset['sections'][$request->section]['data'][$request->index]);

                $dataset['sections'][$request->section]['data'] = array_values($dataset['sections'][$request->section]['data']);

                PageDataset::where('page_id', $page->id)->update(['dataset' => json_encode($dataset)]);

                return $dataset;
            } catch (\Exception $e) {
                return Response('', 404);
            }
        }

        return Response('',201);
    }

    public function createPage(Request $request) {
        if (!isset($request->title) || !isset($request->title_eng) || !isset($request->template) || !isset($request->route)) return Response('', 400);
        $repository = new NewsRepository();
        return $repository->createPage($request);
    }

    public function deletePage(Request $request) {
        if (!isset($request->route)) return Response('', 400);
        $repository = new NewsRepository();
        return $repository->deletePage($request);
    }

    public function answerQuestionnaire(Request $request) {
        if (!isset($request->answers)) {
            return Response('0', 400);
        }

        $repository = new NewsRepository();
        return $repository->answerQuestionnaire($request);
    }

    private function readRoute(String $route) {
        return array_values(array_diff(explode('/', $route), [""]));
    }

    private function SortByKeyValue($data, $sortKey, $sort_flags=SORT_DESC)
    {
        if (empty($data) or empty($sortKey)) return $data;

        $ordered = array();
        foreach ($data as $key => $value)
            $ordered[$value[$sortKey]] = $value;

        ksort($ordered, $sort_flags);

        return array_values($ordered); // array_values() added for identical result with multisort*
    }

}
