<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

Class StubsController extends Controller 
{

	public function stub($url = null) {
		if (is_null($url)) {
			return Response()->json(
                array(
                    'status' => 'error',
                    'message' => 'This page not found'
                ),
                404
            );
		}

		switch ($url) {
			case 'test-page':
				return Response()->json(
	                array(
	                    'status' => 'success',
	                    'data' => [
	                    	'template' => 'template1',
	                    	'template_data' => [
	                    		'title' => "Тестовая страница",
	                    		'title_eng' => "Test page",
	                    		'logotype' => "https://instagram.com/static/images/web/mobile_nav_type_logo.png/735145cfe0a4.png",
	                    		'table' => [
	                    			[
	                    				[
	                    					'text' => 'Пупа лупа',
	                    					'text_eng' => 'Pupa lupa',
	                    					'link' => 'https://www.instagram.com/'
	                    				],
	                    				[
	                    					'text' => 'Пупа лупа 1',
	                    					'text_eng' => 'Pupa lupa 1',
	                    					'link' => 'https://www.instagram.com/'
	                    				],
	                    				[
	                    					'text' => 'Пупа лупа 2',
	                    					'text_eng' => 'Pupa lupa 2',
	                    					'link' => 'https://www.instagram.com/'
	                    				],
	                    				[
	                    					'text' => 'Пупа лупа 3',
	                    					'text_eng' => 'Pupa lupa 3',
	                    					'link' => 'https://www.instagram.com/'
	                    				],
	                    			],
	                    			[
	                    				[
	                    					'text' => 'Тупа пупа',
	                    					'text_eng' => 'Тupa рupa',
	                    					'link' => 'https://www.instagram.com/'
	                    				],
	                    				[
	                    					'text' => 'Тупа пупа1',
	                    					'text_eng' => 'Тupa рupa',
	                    					'link' => 'https://www.instagram.com/'
	                    				],
	                    			]
	                    		],
	                    		'long_text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum dapibus viverra. Suspendisse vel diam mattis dolor maximus finibus vitae sit amet libero. Donec urna risus, rhoncus ac lectus et, tincidunt semper ex. Nullam non pellentesque dui. Morbi suscipit augue diam. Maecenas et nibh lorem. Vivamus tincidunt, ante dignissim rhoncus auctor, dui enim convallis eros, vitae tincidunt lorem ex eu eros. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam consequat quam a mi sodales, in suscipit tortor fermentum. Vestibulum urna odio, rhoncus eu lobortis ut, pellentesque vel nibh. Donec pellentesque diam turpis, ut hendrerit elit lobortis in. Morbi euismod mi ac nunc aliquam maximus.',
	                    		'slider' => [
	                    			'img_1' => 'https://sun9-21.userapi.com/c850136/v850136968/d1b2b/TiPdxf2idwM.jpg?ava=1',
	                    			'img_2' => 'https://vjoy.cc/wp-content/uploads/2019/07/1-1.jpg',
	                    			'img_3' => 'https://fallout76.su/images/other/kartinki_iz_follaut_76_resizebyheight_cropcenter_900_500.jpg'
	                    		],
	                    		'slider_text' => [
	                    			[
	                    				'img' => 'https://sun9-21.userapi.com/c850136/v850136968/d1b2b/TiPdxf2idwM.jpg?ava=1',
	                    				'title' => 'Заголовок слайда',
	                    				'text' => 'Nullam non pellentesque dui. Morbi suscipit augue diam. Maecenas et nibh lorem. Vivamus tincidunt, ante dignissim rhoncus auctor, dui enim convallis eros, vitae tincidunt lorem ex eu eros. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. '
	                    			],
	                    			[
	                    				'img' => 'https://vjoy.cc/wp-content/uploads/2019/07/1-1.jpg',
	                    				'title' => 'Заголовок слайда',
	                    				'text' => 'Nullam non pellentesque dui. Morbi suscipit augue diam. Maecenas et nibh lorem. Vivamus tincidunt, ante dignissim rhoncus auctor, dui enim convallis eros, vitae tincidunt lorem ex eu eros. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. '
	                    			],
	                    			[
	                    				'img' => 'https://fallout76.su/images/other/kartinki_iz_follaut_76_resizebyheight_cropcenter_900_500.jpg',
	                    				'title' => 'Заголовок слайда',
	                    				'text' => 'Nullam non pellentesque dui. Morbi suscipit augue diam. Maecenas et nibh lorem. Vivamus tincidunt, ante dignissim rhoncus auctor, dui enim convallis eros, vitae tincidunt lorem ex eu eros. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. '
	                    			]
	                    		]
	                    	]
	                    ]
	                ),
	                200
	            );
				break;
			
			default:
				return Response()->json(
	                array(
	                    'status' => 'error',
	                    'message' => 'This page not found'
	                ),
	                404
	            );
				break;
		}

	}

}