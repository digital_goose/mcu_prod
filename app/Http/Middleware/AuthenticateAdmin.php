<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Users;
use App\Models\UsersAuthLog;

class AuthenticateAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userToken = $request->header('Authorization');

        if (UsersAuthLog::checkAuthorization($userToken) === 0) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => "401 Unauthorized!"
                ], 401);
        }

        $userId = UsersAuthLog::getUserId($userToken);
        $role = Users::getUserRole($userId[0]->user_id);

        if ($role != 1) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => "401 Unauthorized!"
                ], 401);
        }

        return $next($request);
    }
}
