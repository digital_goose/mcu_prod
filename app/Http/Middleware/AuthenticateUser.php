<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Users;
use App\Models\UsersAuthLog;

class AuthenticateUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userToken = $request->header('Authorization') ?? "";
        if (empty($userToken)) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => "401 Unauthorized!"
                ],
                401
            );
        }

        if (UsersAuthLog::checkAuthorization($userToken) == 0) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => "401 Unauthorized!"
                ],
                401
            );
        }

        return $next($request);
    }
}
