<?php


namespace App\Repositories\Service\Pages;

use App\Repositories\BasicRepository;
use App\Models\FAQ;
use App\Models\Users;
use App\Models\UsersAuthLog;

class FAQRepository
{

    public function getAll($request) {
        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? 20;
        $order = $request->order ?? 'desc';

        $total = FAQ::count();

        if (empty($total)) return ['total' => $total, 'data' => []];

        $data = FAQ::offset($offset)
            ->limit($limit)
            ->orderBy('id', $order)
            ->get(['id', 'title', 'title_eng', 'content', 'content_eng']);

        $fields = [];

        if (isset($_COOKIE['user_email']) && isset($_COOKIE['user_token'])) {
            $userId = Users::searchUser($_COOKIE['user_email']);

            if ($userId != 0) {
                if (UsersAuthLog::checkAuthorization($userId, $_COOKIE['user_token']) > 0) {
                    $role = Users::getUserRole($userId);
                    if ($role === 1) {
                        $fields = [
                            'fields' => [
                                [
                                    "title" => 'Заголовок',
                                    'name' => 'title',
                                    'type' => 'text'
                                ],
                                [
                                    "title" => 'Заголовок (англ.)',
                                    'name' => 'title_eng',
                                    'type' => 'text'
                                ],
                                [
                                    "title" => 'Контент',
                                    'name' => 'content',
                                    'type' => "textArea"
                                ]
                            ]
                        ];
                    }
                }
            }

        }

        $response =  [
            'news' => [
                'title' => "Новости",
                'template' => "News",
                'sections' => [
                    'news' => [
                        'title' => "Новости",
                        'data' => [$data],
                        'total' => $total
                    ]
                ]
            ]
        ];

        if (!empty($fields)) {
            $response['news']['sections']['news'] += $fields;
        }

        return $response;
    }

    public function findById(Int $id) {

        $data = News::where('id', $id)
            ->get(['id', 'title', 'title_eng', 'preview', 'content', 'content_eng', 'public_date', 'category']);

        if (count($data) === 0) return [];

        $fields = [];

        if (isset($_COOKIE['user_email']) && isset($_COOKIE['user_token'])) {
            $userId = Users::searchUser($_COOKIE['user_email']);

            if ($userId != 0) {
                if (UsersAuthLog::checkAuthorization($userId, $_COOKIE['user_token']) > 0) {
                    $role = Users::getUserRole($userId);
                    if ($role === 1) {
                        $fields = [
                            'fields' => [
                                [
                                    "title" => 'Заголовок',
                                    'name' => 'title',
                                    'type' => 'text'
                                ],
                                [
                                    "title" => 'Заголовок (англ.)',
                                    'name' => 'title_eng',
                                    'type' => 'text'
                                ],
                                [
                                    "title" => 'Превью',
                                    'name' => 'preview',
                                    'type' => 'text'
                                ],
                                [
                                    "title" => 'Контент',
                                    'name' => 'content',
                                    'type' => 'textarea'
                                ],
                                [
                                    "title" => 'Контент (англ.)',
                                    'name' => 'content_eng',
                                    'type' => 'textarea'
                                ],
                                [
                                    "title" => 'Дата публикации',
                                    'name' => 'public_date',
                                    'type' => 'date'
                                ],
                                [
                                    "title" => 'Категория',
                                    'name' => 'category',
                                    'type' => 'select',
                                    'options' => [
                                        [
                                            'name' => 'Жизнь института',
                                            'value' => 'Жизнь института'
                                        ],
                                        [
                                            'name' => 'СМИ',
                                            'value' => 'СМИ'
                                        ],
                                        [
                                            'name' => 'Обучение',
                                            'value' => 'Обучение'
                                        ],
                                        [
                                            'name' => 'Проекты',
                                            'value' => 'Проекты'
                                        ]
                                    ]
                                ]
                            ]
                        ];
                    }
                }
            }
        }

        $response = [
            'news' => [
                'title' => "Новости",
                'template' => "News",
                'sections' => [
                    'news' => [
                        'title' => "Новости",
                        'data' => [$data]
                    ]
                ]
            ]
        ];

        if (!empty($fields)) {
            $response['news']['sections']['news'] += $fields;
        }

        return $response;
    }
}
