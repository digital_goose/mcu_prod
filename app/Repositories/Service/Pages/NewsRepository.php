<?php


namespace App\Repositories\Service\Pages;

use App\Models\News;
use App\Models\Users;
use App\Models\Templates;
use App\Models\UsersAuthLog;
use Illuminate\Support\Facades\DB;

class NewsRepository
{

    private $modelPath = 'App\Models\\';

    public function getAll($request, $model) {
        $validation = [
            'News' => [
                'admin' => false,
                'template' => 'news',
                'section' => 'news',
                'variables' => ['category'],
                'default' => [
                    'category' => ['Жизнь института', 'СМИ', 'Обучение', 'Проекты']
                ],
                'methods' => [
                    'category' => [
                        'replace' => [";", "/", "\\"],
                        'explode' => ',',
                        'array_diff' => '',
                        'trim' => ''
                    ]
                ],
                'where' => "public_date <= '" . date("Y-m-d", strtotime(now())) . "'",
                'whereIn' => 'category',
                'get' => ['id', 'title', 'title_eng', 'content', 'content_eng', 'preview', 'public_date', 'category'],
                'default_fields' => [
                    'title' => "Новости",
                    'template' => "News",
                    'sections' => [
                        'news' => [
                            'title' => "Новости",
                            'fields' => [
                                'title' => [
                                    "title" => 'Заголовок',
                                    'type' => 'text'
                                ],
                                'title_eng' => [
                                    "title" => 'Заголовок (англ.)',
                                    'type' => 'text'
                                ],
                                'preview' => [
                                    "title" => 'Превью',
                                    'type' => 'text'
                                ],
                                'content' => [
                                    "title" => 'Контент',
                                    'type' => 'textarea'
                                ],
                                'content_eng' => [
                                    "title" => 'Контент (англ.)',
                                    'type' => 'textarea'
                                ],
                                'public_date' => [
                                    "title" => 'Дата публикации',
                                    'type' => 'date'
                                ],
                                'category' => [
                                    "title" => 'Категория',
                                    'type' => 'select',
                                    'options' => [
                                        [
                                            'name' => 'Жизнь института',
                                            'value' => 'Жизнь института'
                                        ],
                                        [
                                            'name' => 'СМИ',
                                            'value' => 'СМИ'
                                        ],
                                        [
                                            'name' => 'Обучение',
                                            'value' => 'Обучение'
                                        ],
                                        [
                                            'name' => 'Проекты',
                                            'value' => 'Проекты'
                                        ]
                                    ]
                                ]
                            ],
                            'data' => []
                        ]
                    ]
                ]
            ],
            'Templates' => [
                'admin' => true,
                'template' => '',
                'section' => 'templates',
                'variables' => [],
                'default' => [],
                'methods' => [],
                'where' => '',
                'whereIn' => '',
                'get' => ['id', 'name', 'title'],
                'default_fields' => [
                    'title' => "Шаблоны",
                    'template' => "Templates",
                    'sections' => [
                        'templates' => [
                            'title' => "Шаблоны",
                            'fields' => [
                                'name' => [
                                    "title" => 'Наименование (англ.)',
                                    'type' => 'text'
                                ],
                                'title' => [
                                    "title" => 'Наименование',
                                    'type' => 'text'
                                ]
                            ],
                            'data' => []
                        ]
                    ]
                ]
            ],
            'Question' => [
                'admin' => false,
                'template' => 'questionnaire',
                'section' => 'questions',
                'variables' => [],
                'default' => [],
                'methods' => [],
                'where' => "show = TRUE",
                'whereIn' => '',
                'get' => ['id', 'question', 'question_eng', 'description', 'description_eng', 'answer_1', 'answer_1_eng', 'answer_2', 'answer_2_eng', 'answer_3', 'answer_3_eng', 'answer_4', 'answer_4_eng'],
                'default_fields' => [
                    'title' => "Анкета",
                    'title_eng' => "Questionnaire",
                    'template' => "",
                    'sections' => [
                        'description' => [
                            'title' => 'Описание',
                            'fields' => [
                                'description' => [
                                    'title' => 'Описание',
                                    'type' => "text"
                                ],
                                'description_eng' => [
                                    'title' => "Описание (англ.)",
                                    'type' => "text"
                                ]
                            ],
                            'data' => []
                        ],
                        'questions' => [
                            'title' => "Вопросы",
                            'fields' => [
                                'question' => [
                                    "title" => 'Вопрос',
                                    'type' => 'text'
                                ],
                                'question_eng' => [
                                    "title" => 'Вопрос (англ.)',
                                    'type' => 'text'
                                ],
                                'description' => [
                                    "title" => 'Описание',
                                    'type' => 'textarea'
                                ],
                                'description_eng' => [
                                    "title" => 'Описание (англ.)',
                                    'type' => 'textarea'
                                ],
                                'answer_1' => [
                                    "title" => 'Ответ 1',
                                    'type' => 'text'
                                ],
                                'answer_1_eng' => [
                                    "title" => 'Ответ 1 (англ.)',
                                    'type' => 'text'
                                ],
                                'answer_2' => [
                                    "title" => 'Ответ 2',
                                    'type' => 'text'
                                ],
                                'answer_2_eng' => [
                                    "title" => 'Ответ 2 (англ.)',
                                    'type' => 'text'
                                ],
                                'answer_3' => [
                                    "title" => 'Ответ 3',
                                    'type' => 'text'
                                ],
                                'answer_3_eng' => [
                                    "title" => 'Ответ 3 (англ.)',
                                    'type' => 'text'
                                ],
                                'answer_4' => [
                                    "title" => 'Ответ 4',
                                    'type' => 'text'
                                ],
                                'answer_4_eng' => [
                                    "title" => 'Ответ 4 (англ.)',
                                    'type' => 'text'
                                ]
                            ],
                            'data' => []
                        ]
                    ]
                ]
            ]
        ];

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? null;
        $order = $request->order ?? 'desc';

        if (!isset($validation[$model])) return [];

        $fieldsData = [];

        foreach ($validation[$model]['variables'] as $variable) {
            if (!isset($request->$variable) && !isset($validation[$model]['default'][$variable])) {
                return [];
            }

            $data = $request->$variable ?? $validation[$model]['default'][$variable];

            $methods = $validation[$model]['methods'][$variable];

            foreach ($methods as $methodName => $method) {
                switch ($methodName) {
                    case 'replace':
                            if (is_string($data)) {
                                $data = str_replace($method, "", $data);
                            }
                        break;
                    case 'explode':
                            if (is_string($data)) {
                                $data = explode($method, $data);
                            }
                        break;
                    case 'array_diff':
                            if (is_array($data)) {
                                $data = array_diff($data, [$method]);
                            }
                        break;
                    case 'trim':
                        if (is_array($data)) {
                            $data = array_map("trim", $data);
                        } elseif (is_string($data)) {
                            $data = trim($data);
                        }
                        break;
                }
            }

            $fieldsData += [
                $variable => $data
            ];
        }

        $this->modelPath .= $model;

        if ($validation[$model]['whereIn'] != "" && $validation[$model]['where'] != "") {
            $total = $this->modelPath::whereRaw($validation[$model]['where'])
                ->whereIn($validation[$model]['whereIn'], $fieldsData[$validation[$model]['whereIn']])
                ->count();
        } elseif ($validation[$model]['whereIn'] != "" && $validation[$model]['where'] === "") {
            $total = $this->modelPath::whereIn($validation[$model]['whereIn'], $fieldsData[$validation[$model]['whereIn']])
                ->count();
        } elseif ($validation[$model]['whereIn'] === "" && $validation[$model]['where'] != "") {
            $total = $this->modelPath::whereRaw($validation[$model]['where'])
                ->count();
        } else {
            $total = $this->modelPath::count();
        }

        $userToken = $request->header('Authorization') ?? "";

        $isAdmin = false;

        if (!empty($userToken)) {
            if (UsersAuthLog::checkAuthorization($userToken) > 0) {
                $userId = UsersAuthLog::getUserId($userToken);
                $role = Users::getUserRole($userId[0]->user_id);
                if ($role === 1) {
                    $validation[$model]['where'] = "";
                    $isAdmin = true;
                }
            }
        }

        if ($validation[$model]['admin'] && !$isAdmin) {
            return [];
        }

        $fields = $validation[$model]['default_fields'];

        if (!$isAdmin) {
            foreach ($fields['sections'] as &$section) {
                unset($section['fields']);
            }
        }

        if (!isset($fields['sections'][$validation[$model]['section']])) return [];

        if ($validation[$model]['whereIn'] != "" && $validation[$model]['where'] != "") {
            $data = $this->modelPath::whereRaw($validation[$model]['where'])
                ->whereIn($validation[$model]['whereIn'], $fieldsData[$validation[$model]['whereIn']])
                ->offset($offset)
                ->limit($limit)
                ->orderBy('id', $order)
                ->get($validation[$model]['get']);
        } elseif ($validation[$model]['whereIn'] != "" && $validation[$model]['where'] === "") {
            $data = $this->modelPath::whereIn($validation[$model]['whereIn'], $fieldsData[$validation[$model]['whereIn']])
                ->offset($offset)
                ->limit($limit)
                ->orderBy('id', $order)
                ->get($validation[$model]['get']);
        } elseif ($validation[$model]['whereIn'] === "" && $validation[$model]['where'] != "") {
            $data = $this->modelPath::whereRaw($validation[$model]['where'])
                ->offset($offset)
                ->limit($limit)
                ->orderBy('id', $order)
                ->get($validation[$model]['get']);
        } else {
            $data = $this->modelPath::offset($offset)
                ->limit($limit)
                ->orderBy('id', $order)
                ->get($validation[$model]['get']);
        }

        $fields['sections'][$validation[$model]['section']]['data'] = $data;
        $fields['sections'][$validation[$model]['section']] += ['total' => $total];

        return $fields;
    }

    public function findById($request, Int $id, $model) {
        $modelSettings = [
            'News' => [
                'admin' => false,
                'template' => 'news',
                'section' => 'news',
                'get' => ['id', 'title', 'title_eng', 'preview', 'content', 'content_eng', 'public_date', 'category'],
                'default_fields' => [
                    'title' => "Новости",
                    'template' => "News",
                    'sections' => [
                        'news' => [
                            'title' => "Новости",
                            'fields' => [
                                'title' => [
                                    "title" => 'Заголовок',
                                    'type' => 'text'
                                ],
                                'title_eng' => [
                                    "title" => 'Заголовок (англ.)',
                                    'type' => 'text'
                                ],
                                'preview' => [
                                    "title" => 'Превью',
                                    'type' => 'text'
                                ],
                                'content' => [
                                    "title" => 'Контент',
                                    'type' => 'textarea'
                                ],
                                'content_eng' => [
                                    "title" => 'Контент (англ.)',
                                    'type' => 'textarea'
                                ],
                                'public_date' => [
                                    "title" => 'Дата публикации',
                                    'type' => 'date'
                                ],
                                'category' => [
                                    "title" => 'Категория',
                                    'type' => 'select',
                                    'options' => [
                                        [
                                            'name' => 'Жизнь института',
                                            'value' => 'Жизнь института'
                                        ],
                                        [
                                            'name' => 'СМИ',
                                            'value' => 'СМИ'
                                        ],
                                        [
                                            'name' => 'Обучение',
                                            'value' => 'Обучение'
                                        ],
                                        [
                                            'name' => 'Проекты',
                                            'value' => 'Проекты'
                                        ]
                                    ]
                                ]
                            ],
                            'data' => []
                        ]
                    ]
                ]
            ],
            'Templates' => [
                'admin' => true,
                'template' => '',
                'section' => 'templates',
                'get' => ['id', 'name', 'title', 'fields'],
                'methods' => [
                    'json_decode' => 'fields'
                ],
                'default_fields' => [
                    'title' => "Шаблоны",
                    'template' => "Templates",
                    'sections' => [
                        'templates' => [
                            'title' => "Шаблоны",
                            'fields' => [
                                'name' => [
                                    "title" => 'Наименование (англ.)',
                                    'type' => 'text'
                                ],
                                'title' => [
                                    "title" => 'Наименование',
                                    'type' => 'text'
                                ]
                            ],
                            'data' => []
                        ]
                    ]
                ]
            ],
            'Question' => [
                'admin' => false,
                'template' => 'questionnaire',
                'section' => 'questions',
                'get' => ['id', 'question', 'question_eng', 'description', 'description_eng', 'answer_1', 'answer_1_eng', 'answer_2', 'answer_2_eng', 'answer_3', 'answer_3_eng', 'answer_4', 'answer_4_eng'],
                'default_fields' => [
                    'title' => "Анкета",
                    'title_eng' => "Questionnaire",
                    'template' => "",
                    'sections' => [
                        'description' => [
                            'title' => 'Описание',
                            'fields' => [
                                'description' => [
                                    'title' => 'Описание',
                                    'type' => "text"
                                ],
                                'description_eng' => [
                                    'title' => "Описание (англ.)",
                                    'type' => "text"
                                ]
                            ],
                            'data' => []
                        ],
                        'questions' => [
                            'title' => "Вопросы",
                            'fields' => [
                                'question' => [
                                    "title" => 'Вопрос',
                                    'type' => 'text'
                                ],
                                'question_eng' => [
                                    "title" => 'Вопрос (англ.)',
                                    'type' => 'text'
                                ],
                                'description' => [
                                    "title" => 'Описание',
                                    'type' => 'textarea'
                                ],
                                'description_eng' => [
                                    "title" => 'Описание (англ.)',
                                    'type' => 'textarea'
                                ],
                                'answer_1' => [
                                    "title" => 'Ответ 1',
                                    'type' => 'text'
                                ],
                                'answer_1_eng' => [
                                    "title" => 'Ответ 1 (англ.)',
                                    'type' => 'text'
                                ],
                                'answer_2' => [
                                    "title" => 'Ответ 1',
                                    'type' => 'text'
                                ],
                                'answer_2_eng' => [
                                    "title" => 'Ответ 1 (англ.)',
                                    'type' => 'text'
                                ],
                                'answer_3' => [
                                    "title" => 'Ответ 1',
                                    'type' => 'text'
                                ],
                                'answer_3_eng' => [
                                    "title" => 'Ответ 1 (англ.)',
                                    'type' => 'text'
                                ],
                                'answer_4' => [
                                    "title" => 'Ответ 1',
                                    'type' => 'text'
                                ],
                                'answer_4_eng' => [
                                    "title" => 'Ответ 1 (англ.)',
                                    'type' => 'text'
                                ]
                            ],
                            'data' => []
                        ]
                    ]
                ]
            ]
        ];

        if (!isset($modelSettings[$model])) return [];

        $this->modelPath .= $model;

        $data = $this->modelPath::where('id', $id)
            ->get($modelSettings[$model]['get']);

        if (count($data) === 0) return [];

        $userToken = $request->header('Authorization') ?? "";

        $isAdmin = false;

        if (!empty($userToken)) {
            if (UsersAuthLog::checkAuthorization($userToken) > 0) {
                $userId = UsersAuthLog::getUserId($userToken);
                $role = Users::getUserRole($userId[0]->user_id);
                if ($role === 1) {
                    $validation[$model]['where'] = "";
                    $isAdmin = true;
                }
            }
        }

        if ($modelSettings[$model]['admin'] && !$isAdmin) {
            return [];
        }

        if (isset($modelSettings[$model]['methods'])) {
            foreach ($modelSettings[$model]['methods'] as $methodName => $column) {
                foreach ($data as &$dataItem) {
                    switch ($methodName) {
                        case 'json_decode':
                                $dataItem->$column = json_decode($dataItem->$column, true);
                            break;
                    }
                }
            }
        }

        $fields = $modelSettings[$model]['default_fields'];

        if (!$isAdmin) {
            foreach ($fields['sections'] as &$section) {
                unset($section['fields']);
            }
        }

        if (!isset($fields['sections'][$modelSettings[$model]['section']])) return [];

        $fields['sections'][$modelSettings[$model]['section']]['data'] = $data;

        return $fields;
    }

    public function store($request, $model) {
        $model = new $model ?? false;

        if (!$model) return false;

        $tableName = $model->getTable();
        $needFillable = $model->getFillable();

        $database = env("DB_DATABASE");

        $query = <<<SQL1
        select
            column_name,
            data_type
        from
            information_schema.columns
        where
            table_catalog = '$database' and
            table_schema = 'public' and
            table_name = '$tableName';
SQL1;

        $data = DB::select(DB::raw($query));

        $columns = array_column($data, 'column_name');

        $fill = [];

        if (!isset($request->data)) return [];

        if (is_string($request->data))
            $request->data = json_decode($request->data, true);

        for ($index = 0; $index < count($needFillable); $index += 1) {
            if ($key = array_search($needFillable[$index], $columns)) {
                $value = $needFillable[$index];
                if (!$reqValue = $request->data[$value])
                    return [];

                /*if (!$this->validator($reqValue, $data[$key]->data_type))
                    return [];*/

                $fill += [$needFillable[$index] => $reqValue];
            }
        }

        $model->fill($fill);
        $model->save();

        return $model;
    }

    public function update($request, $model) {
        $model = new $model ?? false;

        if (!$model) return false;

        if (!isset($request->index)) return false;

        $tableName = $model->getTable();
        $needFillable = $model->getFillable();

        $database = env("DB_DATABASE");

        $query = <<<SQL1
        select
            column_name,
            data_type
        from
            information_schema.columns
        where
            table_catalog = '$database' and
            table_schema = 'public' and
            table_name = '$tableName';
SQL1;

        $data = DB::select(DB::raw($query));

        $columns = array_column($data, 'column_name');

        $fill = [];

        if (!isset($request->data)) return [];

        if (is_string($request->data))
            $request->data = json_decode($request->data, true);

        for ($index = 0; $index < count($needFillable); $index += 1) {
            if ($key = array_search($needFillable[$index], $columns)) {
                $value = $needFillable[$index];
                if (!$reqValue = $request->data[$value])
                    return [];

                /*if (!$this->validator($reqValue, $data[$key]->data_type))
                    return [];*/

                $fill += [$needFillable[$index] => $reqValue];
            }
        }

        if (!$model->where('id', $request->index)->update($fill)) {
            return false;
        }

        return $fill;
    }

    public function trash($request, $model) {
        $model = new $model ?? false;

        if (!$model) return false;

        if (!isset($request->index)) return false;

        return $model->where('id', $request->index)->delete();
    }

    public function createPage($request) {
        if (Templates::where('id', '=', (int)$request->template)->count() === 0) {
            return Response('', 404);
        }

        $fields = Templates::where('id', '=', (int)$request->template)->get('fields');

        $fields = $fields[0]->fields;

        $fields = json_decode($fields, true);

        $fields['title'] = $request->title;

        if (isset($fields['titleEng'])) {
            $fields['titleEng'] = $request->title_eng;
        } else {
            $fields['title_eng'] = $request->title_eng;
        }

        $model = $this->modelPath . "Pages";

        $pages = new $model;

        $route = "/" . implode(array_diff(explode('/', $request->route), [""]));

        if ($pages::where('route', "=", $route)->count() > 0) return Response('This page isset', 400);

        $id = DB::table('pages')->orderBy('id', 'DESC')->limit(1)->get('id');
        $lastId = ($id[0]->id ?? 0) + 1;

        $pages->fill([
            'id' => $lastId,
            'route' => $route,
            'title' => $request->title,
            'title_eng' => $request->title_eng,
            'template_id' => $request->template,
            'user_id' => 1
        ]);
        $pages->save();

        $model = $this->modelPath . "PageDataset";

        $pageDataset = new $model;

        $id = DB::table('page_dataset')->orderBy('id', 'DESC')->limit(1)->get('id');
        $lastId = ($id[0]->id ?? 0) + 1;

        $pageDataset->fill([
            'id' => $lastId,
            'page_id' => $pages->id,
            'dataset' => json_encode($fields)
        ]);
        $pageDataset->save();

        $dataset = json_decode($pageDataset->dataset, true);

        $response = [
            'title' => $request->title,
            'title_eng' => $request->title_eng,
            'route' => $route,
            'data' => $dataset
        ];

        return $response;
    }

    public function deletePage($request) {
        $model = $this->modelPath . "Pages";
        $pages = new $model;

        $page = $pages->where('route', '=', trim($request->route))->firstOrFail();

        $pageDatasetModel = $this->modelPath . "PageDataset";
        $pageDatasetModel = new $pageDatasetModel;
        $pageDatasetModel->where('page_id', '=', $page->id)->delete();

        $pages->where('id', '=', $page->id)->delete();
    }

    public function answerQuestionnaire($request) {
        $model = $this->modelPath . "Questions";
        $questions = new $model;

        $model = $this->modelPath . "QuestionAnswer";
        $answer = new $model;

        $answersList = json_decode($request->answers, TRUE);

        if (empty($answersList)) return Response('', 400);

        foreach ($answersList as $answerItem) {
            if (!isset($answerItem['question_id']) || !isset($answerItem['answer_id']) || !isset($answerItem['fingerprint']))
                return Response('', 400);
        }

        foreach ($answersList as $answerItem) {
            $questionID = $answerItem['question_id'];
            $fingerprint = $answerItem['fingerprint'];
            if ($answer->whereRaw("question_id = '$questionID' AND fingerprint = '$fingerprint'")->count() > 0)
                return Response()->json([
                    'message' => 'This answers isset.'
                ], 400);

            if ($questions->where('id', '=', $answerItem['question_id'])->count() === 0) {
                return Response('', 400);
            }

            if ($answerItem['answer_id'] > 3 || $answerItem['answer_id'] < 0) {
                return Response('', 400);
            }

            if (strlen($answerItem['fingerprint']) > 255) return Response('', 400);
        }

        $id = DB::table('question_answers')->orderBy('id', 'DESC')->limit(1)->get('id');
        $lastId = ($id[0]->id ?? 0) + 1;

        foreach ($answersList as $answerItem) {
            $answer->fill([
                'id' => $lastId,
                'question_id' => $answerItem['question_id'],
                'answer_id' => $answerItem['answer_id'],
                'fingerprint' => $answerItem['fingerprint']
            ]);

            $answer->save();

            $lastId++;
        }

    }

    private function validator($data, $type) {
        switch ($type) {
            case "character varying":
                    if (mb_strlen($data) > 255) return false;
                    return true;
                break;
            case "text":
                    return true;
                break;
            case "date":
                    return true;
                break;
            default:
                return true;
        }
    }

}
