<?php


namespace App\Repositories\Service;


use App\Models\Users;
use App\Models\UsersAuthLog;
use App\Models\StaticStubs;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Libs\YandexTranslate;
use Illuminate\Support\Str;

class ContentRepository
{

    private $baseModelPath = "App\Models\\";

    /**
     * @param $request
     * @param $model
     * @return array|mixed
     */
    public function getAll($request, String $model)
    {
        try {
        $models = [
            'News' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'News',
                    'section' => 'news'
                ],
                'Query' => [
                    'where' => "public_date <= '" . date("Y-m-d", strtotime(now())) . "'",
                    'whereIn' => 'category',
                    'get' => ['id', 'title', 'title_eng', 'content', 'content_eng', 'preview', 'public_date', 'category', 'slider', 'link']
                ],
                'Parameters' => [
                    'variables' => ['category'],
                    'default' => [
                        'category' => ['Жизнь института', 'СМИ', 'Обучение', 'Проекты', 'Ректор']
                    ],
                    'methods' => [
                        'category' => [
                            'replace' => [";", "/", "\\"],
                            'explode' => ',',
                            'array_diff' => '',
                            'trim' => null
                        ]
                    ],
                ]
            ],
            'Templates' => [
                'Settings' => [
                    'admin' => true,
                    'template' => 'Templates',
                    'section' => 'templates'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => ['id', 'name', 'title']
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'Questions' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'Questions',
                    'section' => 'questions'
                ],
                'Query' => [
                    'where' => "show = TRUE",
                    'whereIn' => null,
                    'get' => ['id', 'question', 'question_eng', 'description', 'description_eng', 'answer_1', 'answer_1_eng', 'answer_2', 'answer_2_eng', 'answer_3', 'answer_3_eng', 'answer_4', 'answer_4_eng']
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'Lecturers' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'GuestLecturer',
                    'section' => 'lecturers'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => ['id', 'photo', 'full_name', 'full_name_eng', 'about_lecturer', 'about_lecturer_eng']
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'LectureHall' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'LectureHall',
                    'section' => 'cards'
                ],
                'Query' => [
                    'where' =>
                        ((!$request->date_start || !$request->date_stop) ? ("date_end::timestamp >= '" . date('Y-m-d H:i:s', strtotime(now())) . "'") : ("(date_start::timestamp BETWEEN '$request->date_start'::timestamp AND '$request->date_stop'::timestamp OR date_end::timestamp BETWEEN '$request->date_start'::timestamp AND '$request->date_stop'::timestamp)"))
                        . ((!$request->kind || mb_strtolower($request->kind) == "все") ? ("") : (" AND (" . $this->methodsHandler($this->methodsHandler($this->methodsHandler($this->methodsHandler($request->kind, 'explode', ','), 'array_diff', ""), "ArraySetValue", "(kind LIKE '%_need_value_%')"), 'implode', ' OR ') . ")"))
                        . (((!$request->format)) ? ("") : (" AND format = '" . $request->format . "'"))
                        . (((!isset($request->price_1))) ? ("") : " AND (price_1 = '0' OR price_1 IS NULL)"),
                    'whereIn' => 'format',
                    'get' => "*"
                ],
                'Parameters' => [
                    'variables' => ['format'],
                    'default' => [
                        'format' => ['Очно', 'Онлайн']
                    ],
                    'methods' => [
                        'format' => [
                            'replace' => [";", "/", "\\"],
                            'explode' => ',',
                            'array_diff' => '',
                            'trim' => null
                        ]
                    ],
                ]
            ],
            'Teachers' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'Lecturers',
                    'section' => 'lecturer'
                ],
                'Query' => [
                    'where' =>
                        (
                            (!$request->department) ?
                                (
                                    (!$request->search) ? null :
                                        "(\"fullName\"::text ILIKE '%" .
                                        $request->search . "%')"/* "%' OR \"fullNameEng\"::text ILIKE '%" .
                                        $request->search . "%' OR \"department\"::text ILIKE '%" .
                                        $request->search . "%' OR \"departmentEng\"::text ILIKE '%" .
                                        $request->search . "%' OR \"faculty\"::text ILIKE '%" .
                                        $request->search . "%' OR \"facultyEng\"::text ILIKE '%" .
                                        $request->search . "%' OR \"photo\"::text ILIKE '%" .
                                        $request->search . "%' OR \"position\"::text ILIKE '%" .
                                        $request->search . "%' OR \"positionEng\"::text ILIKE '%" .
                                        $request->search . "%' OR \"rank\"::text ILIKE '%" .
                                        $request->search . "%' OR \"rankEng\"::text ILIKE '%" .
                                        $request->search . "%' OR \"workExperience\"::text ILIKE '%" .
                                        $request->search . "%' OR \"workExperienceEng\"::text ILIKE '%" .
                                        $request->search . "%' OR \"disciplines\"::text ILIKE '%" .
                                        $request->search . "%' OR \"disciplinesEng\"::text ILIKE '%" .
                                        $request->search . "%' OR \"email\"::text ILIKE '%" .
                                        $request->search . "%' OR \"phone\"::text ILIKE '%" .
                                        $request->search . "%' OR \"scientificInterests\"::text ILIKE '%" .
                                        $request->search . "%' OR \"scientificInterestsEng\"::text ILIKE '%" .
                                        $request->search . "%' OR \"professionalEducation\"::text ILIKE '%" .
                                        $request->search . "%' OR \"professionalEducationEng\"::text ILIKE '%" .
                                        $request->search . "%' OR \"scienceDirections\"::text ILIKE '%" .
                                        $request->search . "%' OR \"scienceDirectionsEng\"::text ILIKE '%" .
                                        $request->search . "%' OR \"rewards\"::text ILIKE '%" .
                                        $request->search . "%' OR \"rewardsEng\"::text ILIKE '%" .
                                        $request->search . "%' OR \"publications\"::text ILIKE '%" .
                                        $request->search . "%' OR \"publicationsEng\"::text ILIKE '%" .
                                        $request->search . "%')"*/
                                )
                            :
                                (
                                    (isset($request->search)) ?
                                        (
                                            "(\"fullName\"::text ILIKE '%" .
                                            $request->search . "%')" /* "%' OR \"fullNameEng\"::text ILIKE '%" .
                                            $request->search . "%' OR \"department\"::text ILIKE '%" .
                                            $request->search . "%' OR \"departmentEng\"::text ILIKE '%" .
                                            $request->search . "%' OR \"faculty\"::text ILIKE '%" .
                                            $request->search . "%' OR \"facultyEng\"::text ILIKE '%" .
                                            $request->search . "%' OR \"photo\"::text ILIKE '%" .
                                            $request->search . "%' OR \"position\"::text ILIKE '%" .
                                            $request->search . "%' OR \"positionEng\"::text ILIKE '%" .
                                            $request->search . "%' OR \"rank\"::text ILIKE '%" .
                                            $request->search . "%' OR \"rankEng\"::text ILIKE '%" .
                                            $request->search . "%' OR \"workExperience\"::text ILIKE '%" .
                                            $request->search . "%' OR \"workExperienceEng\"::text ILIKE '%" .
                                            $request->search . "%' OR \"disciplines\"::text ILIKE '%" .
                                            $request->search . "%' OR \"disciplinesEng\"::text ILIKE '%" .
                                            $request->search . "%' OR \"email\"::text ILIKE '%" .
                                            $request->search . "%' OR \"phone\"::text ILIKE '%" .
                                            $request->search . "%' OR \"scientificInterests\"::text ILIKE '%" .
                                            $request->search . "%' OR \"scientificInterestsEng\"::text ILIKE '%" .
                                            $request->search . "%' OR \"professionalEducation\"::text ILIKE '%" .
                                            $request->search . "%' OR \"professionalEducationEng\"::text ILIKE '%" .
                                            $request->search . "%' OR \"scienceDirections\"::text ILIKE '%" .
                                            $request->search . "%' OR \"scienceDirectionsEng\"::text ILIKE '%" .
                                            $request->search . "%' OR \"rewards\"::text ILIKE '%" .
                                            $request->search . "%' OR \"rewardsEng\"::text ILIKE '%" .
                                            $request->search . "%' OR \"publications\"::text ILIKE '%" .
                                            $request->search . "%' OR \"publicationsEng\"::text ILIKE '%" .
                                            $request->search . "%') AND (" .
                                            ($this
                                                ->methodsHandler($this
                                                    ->methodsHandler($this
                                                        ->methodsHandler($this
                                                            ->methodsHandler($request->department, 'explode', ','), 'array_diff', ""), "ArraySetValue", "(department LIKE '%_need_value_%')"), 'implode', ' OR '))*/)/* . ")"*/
                                        :
                                            ($this
                                                ->methodsHandler($this
                                                    ->methodsHandler($this
                                                        ->methodsHandler($this
                                                            ->methodsHandler($request->department, 'explode', ','), 'array_diff', ""), "ArraySetValue", "(department LIKE '%_need_value_%')"), 'implode', ' OR ') .
                                                ((isset($request->faculty)) ? (" OR " . $this
                                                ->methodsHandler($this
                                                    ->methodsHandler($this
                                                        ->methodsHandler($this
                                                            ->methodsHandler($request->faculty, 'explode', ','), 'array_diff', ""), "ArraySetValue", "(faculty LIKE '%_need_value_%')"), 'implode', ' OR ')) : "")
                                            )
                                )
                        ),
                    'whereIn' => null,
                    'get' => "*"
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'IndexPages' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'IndexPages',
                    'section' => ''
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => null
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'ArchiveActivity' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'ArchiveActivity',
                    'section' => 'cards'
                ],
                'Query' => [
                    'where' => "date_end::timestamp < '" . date('Y-m-d H:i:s', strtotime(now())) . "'"
                        . ((!$request->kind || mb_strtolower($request->kind) == "все") ? ("") : (" AND (" . $this->methodsHandler($this->methodsHandler($this->methodsHandler($this->methodsHandler($request->kind, 'explode', ','), 'array_diff', ""), "ArraySetValue", "(kind LIKE '%_need_value_%')"), 'implode', ' OR ') . ")"))
                        . (((!$request->format)) ? ("") : (" AND format = '" . $request->format . "'"))
                        . (((!isset($request->price_1))) ? ("") : " AND (price_1 = '0' OR price_1 IS NULL)"),
                    'whereIn' => null,
                    'get' => "*"
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'HigherEducation' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'HigherEducation',
                    'section' => 'specialties'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'HigherEducationProfile' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'HigherEducationProfile',
                    'section' => 'profile'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'AdditionalEducation' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'AdditionalEducation',
                    'section' => 'dpo'
                ],
                'Query' => [
                    'where' =>
                        ($this->methodsHandler($this->methodsHandler($this->methodsHandler(
                        ((isset($request->is_remote)) ? (" AND is_remote = '". (int)$request->is_remote . "'") : null) .
                        ((isset($request->is_recruiting)) ? "AND is_recruiting = '" . (int)$request->is_recruiting . "'" : null) .
                        ((!$request->search) ? null :
                            " AND (\"section_id\"::text ILIKE '%" .
                            $request->search . "%' OR \"title\"::text ILIKE '%" .
                            $request->search . "%' OR \"title_eng\"::text ILIKE '%" .
                            $request->search . "%' OR \"full_time_price\"::text ILIKE '%" .
                            $request->search . "%' OR \"part_time_price\"::text ILIKE '%" .
                            $request->search . "%' OR \"term\"::text ILIKE '%" .
                            $request->search . "%' OR \"certificate\"::text ILIKE '%" .
                            $request->search . "%' OR \"certificate_eng\"::text ILIKE '%" .
                            $request->search . "%' OR \"is_remote\"::text ILIKE '%" .
                            $request->search . "%' OR \"is_recruiting\"::text ILIKE '%" .
                            $request->search . "%' OR \"appeal_link\"::text ILIKE '%" .
                            $request->search . "%' OR \"about_program_text\"::text ILIKE '%" .
                            $request->search . "%' OR \"about_program_text_eng\"::text ILIKE '%" .
                            $request->search . "%' OR \"structure_text\"::text ILIKE '%" .
                            $request->search . "%' OR \"structure_text_eng\"::text ILIKE '%" .
                            $request->search . "%' OR \"text_admission\"::text ILIKE '%" .
                            $request->search . "%' OR \"text_admission_eng\"::text ILIKE '%" .
                            $request->search . "%' OR \"text_price\"::text ILIKE '%" .
                            $request->search . "%' OR \"text_price_eng\"::text ILIKE '%" .
                            $request->search . "%' OR \"text_video\"::text ILIKE '%" .
                            $request->search . "%' OR \"text_video_eng\"::text ILIKE '%" .
                            $request->search . "%' OR \"text_reviews\"::text ILIKE '%" .
                            $request->search . "%' OR \"text_reviews_eng\"::text ILIKE '%" .
                            $request->search . "%')"
                        ) .
                        ((isset($request->section_id)) ? " AND (" . $this
                                ->methodsHandler($this
                                    ->methodsHandler($this
                                        ->methodsHandler($this
                                            ->methodsHandler($request->section_id, 'explode', ','), 'array_diff', ""), "ArraySetValue", "(section_id LIKE '%_need_value_,%')"), 'implode', ' OR ') . ")" : null) .
                        ((isset($request->full_time_price) && $request->full_time_price) ? (" AND full_time_price IS NOT NULL") : null) .
                        ((isset($request->part_time_price) && $request->part_time_price) ? (" AND part_time_price IS NOT NULL") : null) .
                        ((isset($request->certificate) && $request->certificate) ? (" AND certificate IS NOT NULL") : null) .
                        ((isset($request->term)) ?
                            " AND (" . $this
                                ->methodsHandler($this
                                    ->methodsHandler($this
                                        ->methodsHandler($this
                                            ->methodsHandler($request->term, 'explode', ','), 'array_diff', ""), "ArraySetValue", "(term LIKE '%_need_value_%')"), 'implode', ' OR ') . ")" : null), 'explode', ' AND '), 'array_diff', ""), 'implode', ' AND ') != "") ? ($this->methodsHandler($this->methodsHandler($this->methodsHandler(
                            (((isset($request->is_remote)) ? (" AND is_remote = '". (int)$request->is_remote . "'") : null) .
                            ((isset($request->is_recruiting)) ? "AND is_recruiting = '" . (int)$request->is_recruiting . "'" : null) .
                            ((!$request->search) ? null :
                                " AND (\"section_id\"::text ILIKE '%" .
                                $request->search . "%' OR \"title\"::text ILIKE '%" .
                                $request->search . "%' OR \"title_eng\"::text ILIKE '%" .
                                $request->search . "%' OR \"full_time_price\"::text ILIKE '%" .
                                $request->search . "%' OR \"part_time_price\"::text ILIKE '%" .
                                $request->search . "%' OR \"term\"::text ILIKE '%" .
                                $request->search . "%' OR \"certificate\"::text ILIKE '%" .
                                $request->search . "%' OR \"certificate_eng\"::text ILIKE '%" .
                                $request->search . "%' OR \"is_remote\"::text ILIKE '%" .
                                $request->search . "%' OR \"is_recruiting\"::text ILIKE '%" .
                                $request->search . "%' OR \"appeal_link\"::text ILIKE '%" .
                                $request->search . "%' OR \"about_program_text\"::text ILIKE '%" .
                                $request->search . "%' OR \"about_program_text_eng\"::text ILIKE '%" .
                                $request->search . "%' OR \"structure_text\"::text ILIKE '%" .
                                $request->search . "%' OR \"structure_text_eng\"::text ILIKE '%" .
                                $request->search . "%' OR \"text_admission\"::text ILIKE '%" .
                                $request->search . "%' OR \"text_admission_eng\"::text ILIKE '%" .
                                $request->search . "%' OR \"text_price\"::text ILIKE '%" .
                                $request->search . "%' OR \"text_price_eng\"::text ILIKE '%" .
                                $request->search . "%' OR \"text_video\"::text ILIKE '%" .
                                $request->search . "%' OR \"text_video_eng\"::text ILIKE '%" .
                                $request->search . "%' OR \"text_reviews\"::text ILIKE '%" .
                                $request->search . "%' OR \"text_reviews_eng\"::text ILIKE '%" .
                                $request->search . "%')"
                            ) .
                            ((isset($request->section_id)) ? " AND (" . $this
                                    ->methodsHandler($this
                                        ->methodsHandler($this
                                            ->methodsHandler($this
                                                ->methodsHandler($request->section_id, 'explode', ','), 'array_diff', ""), "ArraySetValue", "(section_id LIKE '%_need_value_%')"), 'implode', ' OR ') . ")" : null) .
                            ((isset($request->full_time_price) && $request->full_time_price) ? (" AND full_time_price IS NOT NULL") : null) .
                            ((isset($request->part_time_price) && $request->part_time_price) ? (" AND part_time_price IS NOT NULL") : null) .
                            ((isset($request->certificate) && $request->certificate) ? (" AND certificate IS NOT NULL") : null) .
                            ((isset($request->term)) ?
                                " AND (" . $this
                                    ->methodsHandler($this
                                        ->methodsHandler($this
                                            ->methodsHandler($this
                                                ->methodsHandler($request->term, 'explode', ','), 'array_diff', ""), "ArraySetValue", "(term LIKE '%_need_value_%')"), 'implode', ' OR ') . ")" : null)), 'explode', ' AND '), 'array_diff', ""), 'implode', ' AND ')) : null,
                    'whereIn' => null,
                    'get' => [
                        'id',
                        'section_id',
                        'title',
                        'title_eng',
                        'background_image',
                        'full_time_price',
                        'part_time_price',
                        'remote_from_price',
                        'term',
                        'certificate',
                        'certificate_eng',
                        'is_remote',
                        'is_recruiting',
                        'appeal_link',
                        'is_visible',
                        'certificate_image'
                    ]
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'AdditionalEducationSections' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'AdditionalEducationSections',
                    'section' => 'dpo_section'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'Faculty' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'Faculty',
                    'section' => 'faculty'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'Cathedra' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'Cathedra',
                    'section' => 'cathedra'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'SmiAboutUs' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'SmiAboutUs',
                    'section' => 'cards'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'AdditionalEducationalLecturers' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'AdditionalEducationalLecturers',
                    'section' => 'lecturers'
                ],
                'Query' => [
                    'where' => ((isset($request->program_dpo)) ? "" . $this
                            ->methodsHandler($this
                                ->methodsHandler($this
                                    ->methodsHandler($this
                                        ->methodsHandler($request->program_dpo, 'explode', ';'), 'array_diff', ""), "ArraySetValue", "(direction_id ~ '(^|[;])_need_value_([;]|$)')"), 'implode', ' OR ') . "" : null),
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'HonoraryProfessors' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'HonoraryProfessors',
                    'section' => 'lecturers'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'DepartmentModel' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'Department',
                    'section' => 'department'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'SubDepartmentModel' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'SubDepartment',
                    'section' => 'sub_department'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
        ];
        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? null;
        $order = $request->order ?? 'desc';

        if (!is_null($models[$model]['Query']['where'])) {
            if (mb_substr($models[$model]['Query']['where'], 0, 4) == "AND ") {
                $models[$model]['Query']['where'] = mb_substr($models[$model]['Query']['where'], 4, mb_strlen($models[$model]['Query']['where']));
            }
            if (mb_substr($models[$model]['Query']['where'], 0, 5) == " AND ") {
                $models[$model]['Query']['where'] = mb_substr($models[$model]['Query']['where'], 5, mb_strlen($models[$model]['Query']['where']));
            }
        }

        if ($model == "Teachers") {
            $toffset = $offset;
            $offset = 0;
            $tlimit = $limit;
            $limit = null;
        }

        if (!isset($models[$model])) return [];

        $variablesValue = [];

        foreach ($models[$model]['Parameters']['variables'] as $variable)
        {
            try {

                $value = $request->$variable ?? $models[$model]['Parameters']['default'][$variable];

                $methods = $models[$model]['Parameters']['methods'][$variable];

                foreach ($methods as $method => $param) {
                    $value = $this->methodsHandler($value, $method, $param);
                }

                $variablesValue += [
                    $variable => $value
                ];

            } catch (\Exception $e) {

                return [];

            }
        }

        $isAdmin = false;

        $token = $request->header('Authorization') ?? null;

        if (!is_null($token)) {
            if (UsersAuthLog::checkAuthorization($token) > 0) {
                $userId = UsersAuthLog::getUserId($token);
                $role = Users::getUserRole($userId[0]->user_id);
                if ($role === 1) {
                    $models[$model]['Query']['where'] = null;
                    $models[$model]['Query']['where'] = null;
                    $isAdmin = true;
                }
            }
        }

        $stub = StaticStubs::where('template', '=', $models[$model]['Settings']['template'])->firstOrFail('stub');
        $stub = json_decode($stub->stub, true);

        if (!$isAdmin) {
            foreach ($stub['sections'] as &$section) {
                unset($section['fields']);
            }
        }

        if ($model == "IndexPages") {
            return $stub;
        }

        if ($model == "AdditionalEducation" && $isAdmin) {
            $models[$model]['Query']['get'] = "*";
        }

        if ($model == "ArchiveActivity") {
            $modelPath = $this->baseModelPath . "LectureHall";
        } else {
            $modelPath = $this->baseModelPath . $model;
        }

        $initModel = new $modelPath;

        if ($models[$model]['Query']['get'] === "*") {
            $models[$model]['Query']['get'] = $initModel->getFillable();
            array_unshift($models[$model]['Query']['get'], 'id');
        }

        if (!is_null($models[$model]['Query']['where']))
            $initModel = $initModel->whereRaw($models[$model]['Query']['where']);

        if (!is_null($models[$model]['Query']['whereIn']))
            $initModel = $initModel->whereIn($models[$model]['Query']['whereIn'], $variablesValue[$models[$model]['Query']['whereIn']]);

        $total = $initModel->count();

        if ($models[$model]['Settings']['admin'] && !$isAdmin) {
            return [];
        }

            if (!is_null($models[$model]['Query']['where']))
                $initModel = $initModel->whereRaw($models[$model]['Query']['where']);

            if (!is_null($models[$model]['Query']['whereIn']))
                $initModel = $initModel->whereIn($models[$model]['Query']['whereIn'], $variablesValue[$models[$model]['Query']['whereIn']]);

            if ($model == 'News') {
                $values = $initModel
                    ->offset($offset)
                    ->limit($limit)
                    ->orderBy('public_date', $order)
                    ->get($models[$model]['Query']['get']);
            } elseif ($model == 'SmiAboutUs' && !$isAdmin) {
                $values = $initModel
                    ->orderBy('date', $order)
                    ->get($models[$model]['Query']['get']);

                $response = [];

                if (is_null($limit)) {
                    $limit = count($values);
                }

                foreach ($values as $index => $value) {
                    if (count($response) < $limit) {
                        if ($index > $offset) {
                            $response[] = $value;
                        }
                    } else {
                        break;
                    }
                }

                $values = $response;

            } elseif ($model == 'Teachers' && !$isAdmin) {
                $values = $initModel
                    ->orderBy('fullName', 'ASC')
                    ->get($models[$model]['Query']['get']);
            } elseif ($model == 'AdditionalEducation' && !$isAdmin) {
                $values = $initModel
                    ->orderBy('order', 'ASC')
                    ->get($models[$model]['Query']['get']);
            } elseif ($model == 'AdditionalEducationSections' && !$isAdmin) {
                $values = $initModel
                    ->orderBy('order', 'ASC')
                    ->get($models[$model]['Query']['get']);
            } elseif ($model == 'LectureHall' && !$isAdmin) {
                $values = $initModel
                    ->orderBy('date_end', 'ASC')
                    ->get($models[$model]['Query']['get']);
            } else {
                $values = $initModel
                    ->offset($offset)
                    ->limit($limit)
                    ->orderBy('id', $order)
                    ->get($models[$model]['Query']['get']);
            }
            if ($model == "Teachers" && !$isAdmin) {
                if (!$request->search && !$request->faculty) {
                    $values = $this->sortTeachers($values, $tlimit, $toffset);
                }
                if (!$request->search && $request->faculty) {
                    $values = $this->sortTeachersDetail($values, $tlimit, $toffset);
                }
            }

            $stub['sections'][$models[$model]['Settings']['section']]['data'] = $values;
            $stub['sections'][$models[$model]['Settings']['section']] += ['total' => $total];

            return $stub;

        } catch (\Exception $e) {

            return [$e->getMessage()];

        }

    }

    /**
     * @param $request
     * @param Int $id
     * @param String $model
     * @return array|mixed
     */
    public function findById($request, Int $id, $model)
    {
        $models = [
            'News' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'DetailNews',
                    'section' => 'news'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => ['id', 'title', 'title_eng', 'preview', 'content', 'content_eng', 'public_date', 'category', 'slider', 'link']
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'Templates' => [
                'Settings' => [
                    'admin' => true,
                    'template' => 'Templates',
                    'section' => 'templates'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => ['id', 'name', 'title', 'fields']
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [
                        'json_decode' => 'fields'
                    ]
                ]
            ],
            'Question' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'Questions',
                    'section' => 'questions'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => ['id', 'question', 'question_eng', 'description', 'description_eng', 'answer_1', 'answer_1_eng', 'answer_2', 'answer_2_eng', 'answer_3', 'answer_3_eng', 'answer_4', 'answer_4_eng']
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'LectureHall' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'LectureHallDetail',
                    'section' => 'cards'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => ['*']
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'Teachers' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'LecturersDetail',
                    'section' => 'lecturer'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => "*"
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'HigherEducation' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'HigherEducationDetail',
                    'section' => 'specialties'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'HigherEducationProfile' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'HigherEducationProfileDetail',
                    'section' => 'profile'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'AdditionalEducation' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'AdditionalEducationDetail',
                    'section' => 'dpo'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'AdditionalEducationSections' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'AdditionalEducationSectionsDetail',
                    'section' => 'dpo_section'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'Faculty' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'FacultyDetail',
                    'section' => 'faculty'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'Cathedra' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'CathedraDetail',
                    'section' => 'cathedra'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'AdditionalEducationalLecturers' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'AdditionalEducationalLecturersDetail',
                    'section' => 'lecturers'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'HonoraryProfessors' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'HonoraryProfessorsDetail',
                    'section' => 'lecturers'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'DepartmentModel' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'DepartmentDetail',
                    'section' => 'department'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
            'SubDepartmentModel' => [
                'Settings' => [
                    'admin' => false,
                    'template' => 'SubDepartmentDetail',
                    'section' => 'sub_department'
                ],
                'Query' => [
                    'where' => null,
                    'whereIn' => null,
                    'get' => '*'
                ],
                'Parameters' => [
                    'variables' => [],
                    'default' => [],
                    'methods' => [],
                ]
            ],
        ];

        if (!isset($models[$model])) return [];

        $modelPath = $this->baseModelPath . $model;

        $initModel = new $modelPath;

        $token = $request->header('Authorization') ?? null;

        $isAdmin = false;

        if (!is_null($token)) {
            if (UsersAuthLog::checkAuthorization($token) > 0) {
                $userId = UsersAuthLog::getUserId($token);
                $role = Users::getUserRole($userId[0]->user_id);
                if ($role === 1) {
                    $models[$model]['Query']['where'] = null;
                    $models[$model]['Query']['where'] = null;
                    $isAdmin = true;
                }
            }
        }

        if ($models[$model]['Query']['get'] === "*") {
            $models[$model]['Query']['get'] = $initModel->getFillable();
            array_unshift($models[$model]['Query']['get'], 'id');
        }

        $values = $initModel
            ->where('id', $id)
            ->firstOrFail($models[$model]['Query']['get']);

        if ($models[$model]['Settings']['admin'] && !$isAdmin) {
            return [];
        }

        foreach ($models[$model]['Parameters']['methods'] as $method => $param) {
            foreach ($values as &$item) {
                $item->$param = $this->methodsHandler($item->$param, $method, null);
            }
        }

        $stub = StaticStubs::where('template', '=', $models[$model]['Settings']['template'])->firstOrFail('stub');
        $stub = json_decode($stub->stub, true);

        if (!$isAdmin) {
            foreach ($stub['sections'] as &$section) {
                unset($section['fields']);
            }
        }

        if ($model == 'Teachers') {
            $stub['title'] = $values->fullName;
            $stub['titleEng'] = $values->fullNameEng;
            $stub['sections']['lecturer']['title'] = $values->fullName;
        }

        try {
            $stub['sections'][$models[$model]['Settings']['section']]['data'] = $values;
            if ($model == 'News') {
               $stub['title'] = $values->title;
               $stub['titleEng'] = $values->titleEng;
               $stub['sections']['news']['title'] = $values->title;
           }
            return $stub;
        } catch (\Exception $e) {
            return [];
        }

    }

    /**
     * @param $request
     * @param String $model
     * @return array|mixed
     */
    public function create($request, String $model)
    {

        try {

            if ($request->stub) {
                $staticTemplates = new StaticStubs();

                $stub = $staticTemplates
                        ->where('template', '=', $model)
                        ->firstOrFail('stub');

                $stub = json_decode($stub->stub, true);

                if (is_string($request->data)) {
                    $data = json_decode($request->data, true);
                } else {
                    $data = $request->data;
                }

                /*foreach ($data as $key => &$value)
                {
                    if ((mb_stristr($key, "_eng") !== FALSE || mb_stristr($key, "Eng") !== FALSE) && !is_null($value))
                    {

                        $yandexTranslate = new YandexTranslate($value);
                        $value = $yandexTranslate->getResponse();

                    }
                }*/

                $stub['sections'][$request->section]['data'][] = $data;

                $staticTemplates
                    ->where('template', '=', $model)
                    ->update(['stub' => json_encode($stub)]);

                return $stub;

            }

            $modelPath = $this->baseModelPath . $model;

            $initModel = new $modelPath;

            //Getting table name geted model
            $tableName = $initModel->getTable();

            //Getting need columns for insert new data
            $needFillable = $initModel->getFillable();

            //Getting database name
            $database = env("DB_DATABASE");

            //This query getting columns types in need table
            $query = <<<SQL1
        select
            column_name,
            data_type
        from
            information_schema.columns
        where
            table_catalog = '$database' and
            table_schema = 'public' and
            table_name = '$tableName';
SQL1;

            $types = DB::select(DB::raw($query));

            $columns = array_column($types, 'column_name');

            $id = $initModel->orderBy('id', 'DESC')->limit(1)->get('id');
            $lastId = $id[0]->id ?? 0;
            $lastId += 1;

            $fill = [];

            if (!$request->data) return [];

            if (is_string($request->data))
                $request->data = json_decode($request->data, true);

            for ($index = 0; $index < count($needFillable); $index += 1)
            {
                if ($key = array_search($needFillable[$index], $columns) !== FALSE)
                {
                    $value = $needFillable[$index];

                    $needValue = $request->data[$value];

                    if ((mb_stristr($key, "_eng") !== FALSE || mb_stristr($key, "Eng") !== FALSE) && !is_null($value))
                    {

                        $yandexTranslate = new YandexTranslate($request->data[$value]);
                        $needValue = $yandexTranslate->getResponse();

                    }

                    $fill += [$needFillable[$index] => $needValue];
                }
            }

            $fill += ['id' => $lastId];

            $initModel->insert($fill);

            return $initModel;

        } catch (\Exception $e) {

            return [$e->getMessage()];

        }

    }

    /**
     * @param $request
     * @param String $model
     * @return array|mixed
     */
    public function update($request, String $model)
    {

        try {

            if ($request->stub) {

                $staticTemplates = new StaticStubs();

                $stub = $staticTemplates
                    ->where('template', '=', $model)
                    ->firstOrFail('stub');

                $stub = json_decode($stub->stub, true);

                if (is_string($request->data)) {
                    $data = json_decode($request->data, true);
                } else {
                    $data = $request->data;
                }

                /*foreach ($data as $key => &$value) {
                    if (mb_stristr($key, "_eng") !== FALSE || mb_stristr($key, "Eng") !== FALSE)
                    {

                        $yandexTranslate = new YandexTranslate($value);
                        $value = $yandexTranslate->getResponse();

                    }
                }*/

                $stub['sections'][$request->section]['data'][$request->index] = $data;

                $staticTemplates
                    ->where('template', '=', $model)
                    ->update(['stub' => json_encode($stub)]);

                return $stub;

            }

            $modelPath = $this->baseModelPath . $model;

            $initModel = new $modelPath;

            //Getting table name geted model
            $tableName = $initModel->getTable();

            //Getting need columns for insert new data
            $needFillable = $initModel->getFillable();

            //Getting database name
            $database = env("DB_DATABASE");

            //This query getting columns types in need table
            $query = <<<SQL1
        select
            column_name,
            data_type
        from
            information_schema.columns
        where
            table_catalog = '$database' and
            table_schema = 'public' and
            table_name = '$tableName';
SQL1;

            $types = DB::select(DB::raw($query));

            $columns = array_column($types, 'column_name');

            $fill = [];

            if (!$request->data) return [];

            if (is_string($request->data))
                $request->data = json_decode($request->data, true);

            for ($index = 0; $index < count($needFillable); $index += 1)
            {
                if ($key = array_search($needFillable[$index], $columns) !== FALSE)
                {
                    $value = $needFillable[$index];

                    $needValue = $request->data[$value];

                    if ((mb_stristr($key, "_eng") !== FALSE || mb_stristr($key, "Eng") !== FALSE) && !is_null($value))
                    {

                        $yandexTranslate = new YandexTranslate($request->data[$value]);
                        $needValue = $yandexTranslate->getResponse();

                    }

                    $fill += [$needFillable[$index] => $needValue];
                }
            }

            $initModel->where('id', '=', $request->index)->update($fill);

            return $initModel;

        } catch (\Exception $e) {

            return [];

        }

    }

    /**
     * @param $request
     * @param String $model
     * @return array|mixed
     */
    public function delete($request, String $model)
    {

        try {

            if ($request->stub) {

                $staticTemplates = new StaticStubs();

                $stub = $staticTemplates
                    ->where('template', '=', $model)
                    ->firstOrFail('stub');

                $stub = json_decode($stub->stub, true);

                unset($stub['sections'][$request->section]['data'][$request->index]);

                $stub['sections'][$request->section]['data'] = array_values($stub['sections'][$request->section]['data']);

                $staticTemplates
                    ->where('template', '=', $model)
                    ->update(['stub' => json_encode($stub)]);

                return $stub;

            }

            $modelPath = $this->baseModelPath . $model;

            $initModel = new $modelPath;

            return $initModel->where('id', $request->index)->delete();

        } catch (\Exception $e) {

            return [];

        }

    }

    /**
     * @param String $text
     * @return array|array[]
     */
    public static function search(String $text, $filter = null)
    {
        $baseUrl = "App\Models\\";
        $models = [
            "News" => [
                "/news/{id}",
                "news"
            ],
            "Lecturers" => [
               "/guest_lecturer",
                "lecturers"
            ],
            "LectureHall" => [
                "/lecture_hall/{id}",
                "lecture_hall"
            ],
            "Teachers" => [
                "/lecturers/{id}",
                "teachers"
            ],
        ];

        if (!is_null($filter)) {
            if (mb_strtolower($filter) == "teachers") {
                $filter = "Lecturers";
                $models = [
                    "Teachers" => [
                        "/lecturers/{id}",
                        "teachers"
                    ],
                ];
            } else {
                $filter = null;
            }
        }

        $response = [];

        $search = [
            "'<script[^>]*?>.*?</script>'sui",  // Вырезает javaScript
            "'<[\/\!]*?[^<>]*?>'sui",           // Вырезает HTML-теги
            "'([\r\n])[\s]+'",                 // Вырезает пробельные символы
            "'&(quot|#34);'ui",                 // Заменяет HTML-сущности
            "'&(amp|#38);'ui",
            "'&(lt|#60);'ui",
            "'&(gt|#62);'ui",
            "'&(nbsp|#160);'ui",
            "'&(iexcl|#161);'ui",
            "'&(cent|#162);'ui",
            "'&(pound|#163);'ui",
            "'&(copy|#169);'ui",
            "'&#(\d+);'ui"];

        $replace = [
            "",
            "",
            "\\1",
            "\"",
            "&",
            "<",
            ">",
            " ",
            chr(161),
            chr(162),
            chr(163),
            chr(169),
            "chr(\\1)"
        ];

        foreach ($models as $model => $url)
        {
            $modelPath = $baseUrl . $model;
            $initModel = new $modelPath;

            $get = $searchable = $initModel->searchable;

            foreach ($searchable as &$column) {
                $column = '"' . $column . '"' . "::text";
            }

            foreach ($get as &$column) {
                $column = "\"$column\"";
            }

            if (is_null($filter)) {
                foreach ($get as &$column) {
                    $column = "substring(lower($column::text) from lower('$text')) as $column";
                }
            }

            array_unshift($get, 'id');
            $get = implode(",", $get);

                $searchable = implode(" ILIKE '%$text%' OR ", $searchable);
            $searchable .= " ILIKE '%$text%'";

            $query = <<<SQL1
            SELECT $get FROM $url[1] WHERE $searchable
SQL1;

            $searched = \DB::select(\DB::raw($query));

            if (is_null($filter)) {
                foreach ($searched as &$searchedItem) {
                    $searchedItem = array_diff((array)$searchedItem, [null]);
                }
            } else {
                foreach ($searched as &$searchedItem) {
                    $searchedItem = (array)$searchedItem;
                }
            }

            $rData = [];

            foreach ($searched as &$item) {
                $get = [];

                foreach ($item as $column => $value) {
                    if ($column != "id") {
                        $get[] = "$column";
                    } else $id = $value;
                }

                try {
                    $data = $initModel->where('id', '=', $id)->firstOrFail($get);

                    $data = json_decode($data, true);
                    $temp = null;
                    foreach ($data as &$dataValue) {
                        $dataValue = preg_replace($search, $replace, $dataValue);
                    }

                    $temp = $data;

                    $rData[] = [
                        'route' => (mb_stristr($url[0], "{id}") !== FALSE) ? str_replace("{id}", $item['id'], $url[0]) : $url[0],
                        'data' => $temp
                    ];
                } catch (\Exception $e) {}
            }

            if (!empty($searched)) {
                $response = $response + [$model => $rData];
            }
        }

        if (!is_null($filter)) {
            $stub = StaticStubs::where('template', '=', $filter)->firstOrFail('stub');
            $stub = json_decode($stub->stub, true);

            foreach ($stub['sections'] as &$section) {
                unset($section['fields']);
            }

            $stub['sections']['lecturer']['data'] = $response;

            return $stub;
        }

        $pagesModel = $baseUrl . "Pages";
        $pagesModel = new $pagesModel;

        $query = <<<SQL1
        SELECT page_id, dataset FROM page_dataset ORDER BY page_id ASC
SQL1;

        $pages = \DB::select(\DB::raw($query));

        $pagesContent = [];

        foreach ($pages as $page) {
                $dataset = json_decode($page->dataset, true);
                $temp = [];
                foreach ($dataset['sections'] as $section) {
                    foreach ($section['data'] as $item) {
                        foreach ($item as $column => $value) {
                            if (mb_stristr($value, $text) !== FALSE) {
                                $temp[] = preg_replace($search, $replace, htmlspecialchars_decode(str_replace(["\r\n", "\r", "\n"], ' ', $value), ENT_NOQUOTES));
                            }
                        }
                    }
                }
                if (!empty($temp)) {
                    $pageUrl = $pagesModel->where('id', '=', $page->page_id)->firstOrFail('route');
                    $pageUrl = $pageUrl->route;
                    $pagesContent[] = [
                        'route' => $pageUrl,
                        'data' => $temp
                    ];
                }
            }

        if (!empty($pagesContent)) {
            $response = $response + ['Pages' => $pagesContent];
        }

        return $response;

    }

    /**
     * @param $value
     * @param String $method
     * @param null $param
     * @return array|false|mixed|string|string[]
     */
    private function methodsHandler($value, String $method, $param = null)
    {

        switch ($method) {
            case 'replace':
                if (is_string($value)) {
                    return str_replace($param, "", $value);
                }
                break;
            case 'explode':
                if (is_string($value)) {
                    return explode($param, $value);
                }
                break;
            case 'array_diff':
                if (is_array($value)) {
                    return array_values(array_diff($value, [$param]));
                }
                break;
            case 'trim':
                if (is_array($value)) {
                    return array_map("trim", $value);
                } elseif (is_string($value)) {
                    return trim($value);
                }
                break;
            case 'json_decode':
                if (is_string($value)) {
                    return json_decode($value, true);
                }
                break;
            case 'implode':
                    return implode($param, $value);
                break;
            case 'ArraySetValue':
                    if (is_array($value)) {
                        foreach ($value as &$item) {
                            $item = str_replace('_need_value_', $item, $param);
                        }
                        return $value;
                    } else return $value;
                break;
        }

        return $value;

    }

    private function sortTeachers($teachersList, $limit, $offset) {
        $sortPosition = [
//            "ректор",
            "декан",
            "первый проректор",
            "проректор",
            "заместитель декана",
            "заведующий отделом аспирантуры и магистратуры",
            "завкафедрой",
            "заведующий кафедрой",
            "профессор",
            "заведующий лабораторией",
            "ведущий научный сотрудник",
            "младший научный сотрудник",
            "старший преподаватель",
            "доцент",
            "преподаватель",
            "ассистент"
        ];

        try {

            for ($i = 0; $i < count($teachersList); $i++) {
                $teachersList[$i] = json_decode($teachersList[$i]);

                $departments = $teachersList[$i]->position;

                $departments = array_diff(explode(',', $departments), [""]);

                $roleId = count($sortPosition);

                foreach ($departments as $department) {
                    $tempRole = array_search(mb_strtolower($department), $sortPosition, true);
                    if ($tempRole !== FALSE && $roleId > $tempRole) {
                        $roleId = $tempRole;
                    }
                    else {
                        foreach ($sortPosition as $index => $position) {
                            if (preg_match("/^$position/", mb_strtolower($department)) && $roleId > $index) {
                                $roleId = $index;
                            }
                        }
                    }
                }

                $teachersList[$i]->role_id = $roleId;
            }

        $count_elements = count($teachersList);
        $iterations = $count_elements - 1;

            for ($i = 0; $i < $count_elements; $i++) {
                $changes = false;
                for ($j = 0; $j < $iterations; $j++) {
                    if ($teachersList[$j]->role_id > $teachersList[($j + 1)]->role_id) {
                        $changes = true;
                        list($teachersList[$j], $teachersList[($j + 1)]) = array($teachersList[($j + 1)], $teachersList[$j]);
                    }
                }
                $iterations--;
                if (!$changes) {
                    break;
                }
            }

            $response = [];

            if (isset($teachersList[$offset])) {

                if (is_null($limit) || !isset($teachersList[(int)$offset + (int)$limit])) {
                    for ($i = (int)$offset; $i < count($teachersList); $i++) {
                        $response[] = $teachersList[$i];
                    }
                } else {
                    for ($i = $offset; $i < ((int)$offset + (int)$limit); $i++) {
                        $response[] = $teachersList[$i];
                    }
                }
            }

            return $response;
        } catch (\Exception $e) {
            return [$e->getMessage()];
        }
    }

    private function sortTeachersDetail($teachersList, $limit, $offset) {
        $sortPosition = [
//            "ректор",
            "декан",
            "завкафедрой",
            "заведующий кафедрой",
            "первый проректор",
            "проректор",
            "заместитель декана",
            "заведующий отделом аспирантуры и магистратуры",
            "профессор",
            "заведующий лабораторией",
            "ведущий научный сотрудник",
            "младший научный сотрудник",
            "старший преподаватель",
            "доцент",
            "преподаватель",
            "ассистент"
        ];

        try {

            for ($i = 0; $i < count($teachersList); $i++) {
                $teachersList[$i] = json_decode($teachersList[$i]);

                $departments = $teachersList[$i]->position;

                $departments = array_diff(explode(',', $departments), [""]);

                $roleId = count($sortPosition);

                foreach ($departments as $department) {
                    $tempRole = array_search(mb_strtolower($department), $sortPosition, true);
                    if ($tempRole !== FALSE && $roleId > $tempRole) {
                        $roleId = $tempRole;
                    }
                    else {
                        foreach ($sortPosition as $index => $position) {
                            if (preg_match("/^$position/", mb_strtolower($department)) && $roleId > $index) {
                                $roleId = $index;
                            }
                        }
                    }
                }

                $teachersList[$i]->role_id = $roleId;
            }

            $count_elements = count($teachersList);
            $iterations = $count_elements - 1;

            for ($i = 0; $i < $count_elements; $i++) {
                $changes = false;
                for ($j = 0; $j < $iterations; $j++) {
                    if ($teachersList[$j]->role_id > $teachersList[($j + 1)]->role_id) {
                        $changes = true;
                        list($teachersList[$j], $teachersList[($j + 1)]) = array($teachersList[($j + 1)], $teachersList[$j]);
                    }
                }
                $iterations--;
                if (!$changes) {
                    break;
                }
            }

            $response = [];

            if (isset($teachersList[$offset])) {

                if (is_null($limit) || !isset($teachersList[(int)$offset + (int)$limit])) {
                    for ($i = (int)$offset; $i < count($teachersList); $i++) {
                        $response[] = $teachersList[$i];
                    }
                } else {
                    for ($i = $offset; $i < ((int)$offset + (int)$limit); $i++) {
                        $response[] = $teachersList[$i];
                    }
                }
            }

            return $response;
        } catch (\Exception $e) {
            return [$e->getMessage()];
        }
    }

}
