<?php


namespace App\Repositories\Service\Admin;

use Illuminate\Support\Facades\DB;
use App\Models\Users;
use App\Models\UserRole;

class UserRepository
{

    static function changeUserRole($user_id, $role_id) {

        if ( !Users::findUser($user_id) ) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'This user not found!'
                ],
                404
            );
        }

        if ( !UserRole::findRole($user_id) ) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'This user not found!'
                ],
                404
            );
        }

        $isUpdate = DB::table('users')
                        ->where('id', $user_id)
                        ->update([
                            'role_id' => $role_id
                        ]);

        if (!$isUpdate) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Undefined error!'
                ],
                500
            );
        }

        return response()->json(
            [
                'status' => 'success',
                'message' => 'Role update!'
            ],
            200
        );
    }

    static function removeUser($user_id) {

        if ( !Users::findUser($user_id) ) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'This user not found!'
                ],
                404
            );
        }

        DB::table('users_auth_log')
            ->where('user_id', $user_id)
            ->delete();

        DB::table('teacher_information')
            ->where('user_id', $user_id)
            ->delete();

        DB::table('users')
            ->where('id', $user_id)
            ->delete();

        return response()->json(
            [
                'status' => 'success',
                'message' => 'User remove!'
            ],
            200
        );

    }

}
