<?php


namespace App\Repositories\Service\Admin;

use Illuminate\Support\Facades\DB;
use App\Models\UserRole;

class ListRepository
{

    static function usersList() {

        $roleId = UserRole::getRoleId('User');

        $roleId = $roleId[0]->id;

        return DB::table('users')
            ->where('role_id', $roleId)
            ->join('user_role', 'role_id', '=', 'user_role.id')
            ->get(['users.id', 'users.email', 'user_role.title']);

    }

    static function teachersList() {

        $roleId = UserRole::getRoleId('Teacher');

        $roleId = $roleId[0]->id;

        return DB::table('users')
            ->where('role_id', $roleId)
            ->join('user_role', 'role_id', '=', 'user_role.id')
            ->get(['users.id', 'users.email', 'user_role.title']);

    }

    static function pagesList() {
        $contentPageId = DB::table('templates')->where('name', '=', 'ContentPage')->get('id');

        $contentPageId = $contentPageId[0]->id;

        $dynamic = DB::table('pages')
            ->whereRaw("template_id != 43 AND template_id != 44 AND template_id != $contentPageId")
            ->orderBy('pages.id', 'ASC')
            ->get(['pages.id', 'pages.route', 'pages.title', 'pages.title_eng']);

        $other = DB::table('pages')
            ->whereRaw("template_id = 43 OR template_id = 44 AND template_id != $contentPageId")
            ->orderBy('pages.id', 'ASC')
            ->get(['pages.id', 'pages.route', 'pages.title', 'pages.title_eng']);

        return [$dynamic, $other];

    }

    static function roleList() {

        return DB::table('user_role')
            ->get(['user_role.id', 'user_role.title']);

    }

}
