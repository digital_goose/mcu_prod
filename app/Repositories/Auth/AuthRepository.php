<?php


namespace App\Repositories\Auth;

use App\Models\UserRole;
use App\Models\Users;
use App\Models\UsersAuthLog;

class AuthRepository
{

    public static function login($email, $password, $ip) {
        $email = htmlspecialchars(trim($email));
        $password = htmlspecialchars(trim($password));

        if (Users::searchUser($email) == 0) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => "This user not found!"
                ],
                404
            );
        }

        $authenticateData = Users::authenticateUser($email, $password);

        if ($authenticateData == 404) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => "Incorrect login or password!"
                ],
                404
            );
        }

        if (count($authenticateData) != 3) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => "Unknown error!"
                ],
                500
            );
        }

        $userId = $authenticateData[0];
        $token = $authenticateData[1];

        if (UsersAuthLog::userAuthorization($userId, $token, $ip)) {
            return response()->json(
                [
                    'status' => 'success',
                    'data' => [
                        'user_email' => $email,
                        'user_token' => $token,
                        'role' => $authenticateData[2]
                    ]
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Unknown authorization error'
                ],
                409
            );
        }
    }

    public static function registration($email, $password, $ip) {
        $email = htmlspecialchars(trim($email));
        $password = htmlspecialchars(trim($password));

        if (Users::searchUser($email) != 0) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => "This user exists!"
                ],
                403
            );
        }

        $roleId = UserRole::getRoleId('User');

        if (!isset($roleId[0]->id)) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => "Unknown error!"
                ],
                500
            );
        }

        $roleId = $roleId[0]->id;

        if (!Users::registration($email, $password, $roleId)) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => "Unknown error!"
                ],
                500
            );
        }

        return self::login($email, $password, $ip);
    }

}
