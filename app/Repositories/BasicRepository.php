<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Contracts\CRUD;

class BasicRepository implements CRUD
{

    protected $model = null;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function getAll($limit = 1000, $offset = 0, $order = 'desc')
    {
        $limit = $limit ?? 1000;
        $offset = $offset ?? 0;

        if (!in_array($order, ['asc', 'desc'])) {
            $order = 'desc';
        }

        return $this->model
            ->offset($offset)
            ->limit($limit)
            ->orderBy('id', $order)
            ->get();
    }

    public function findById(int $id)
    {
        return $this->model
            ->findOrFail($id);
    }

    public function store(array $request)
    {
        $model = $this->model;
        $model->fill($request);
        $model->save();

        return $model;
    }

    public function update(int $id, array $request)
    {
        $model = $this->findById($id);
        $model->fill($request);
        $model->save();

        return $model;
    }

    public function destroy(int $id)
    {
        return $this->model->destroy($id) ? true : false;
    }
}
